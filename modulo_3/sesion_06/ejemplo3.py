import asyncio
import concurrent.futures
from rich import print
from functools import partial
import random
import string
from dataclasses import dataclass
from multiprocessing import Manager
import pandas as pd

@dataclass
class Operacion:
    name: str
    a: float|int 
    b: float|int
    result: float|int


async def suma(a,b):
    return a+b

async def multiplica(a,b):
    return a*b

async def task_one(limit:int, queue):
    for i,_ in enumerate(range(limit)):
        a = random.uniform(1,200)
        b = ord(random.choice(string.ascii_lowercase))
        result = await suma(a,b)
        op = Operacion("suma",a,b,result)
        queue.put(op)

def run_process_one(limit:int, queue):
    asyncio.run(task_one(limit, queue))


async def task_two(limit:int, queue):
    for i,_ in enumerate(range(limit)):
        a = random.uniform(1,200)
        b = ord(random.choice(string.ascii_lowercase))
        result = await multiplica(a,b)
        op = Operacion("multiplicacion",a,b,result)
        queue.put(op)

def run_process_two(limit:int, queue):
    asyncio.run(task_two(limit, queue))


def read_queue(queue):
    N = queue.qsize()
    results = []
    for _ in range(N):
        elem = queue.get()
        results.append(elem)
    df = pd.DataFrame(results)
    df.to_csv("resultados_concurrent_fut.csv")


async def main():
    loop = asyncio.get_running_loop()
    limit = 100
    with concurrent.futures.ProcessPoolExecutor() as pool:
        with Manager() as mg:
            queue = mg.Queue()
            funcion = partial(run_process_one, limit, queue)
            task1 = loop.run_in_executor(
                pool, funcion)

            funcion = partial(run_process_two, limit, queue)
            task2 = loop.run_in_executor(
                pool, funcion)

            tasks = [task1, task2]

            await asyncio.gather(*tasks)
            read_queue(queue)


if __name__=="__main__":
    asyncio.run(main())
