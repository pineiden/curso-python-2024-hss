from pathlib import Path


def show(path:Path):
    if path.exists():
        with path.open() as f:
            for line in f.readlines():
                print(line, end="")

    else:
        print(f"Archivo {path} no existe")
