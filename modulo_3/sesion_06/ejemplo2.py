import random
import string
import typer
from dataclasses import dataclass
from datetime import datetime
import json
import pandas as pd
from multiprocessing import Pool


@dataclass
class Report:
    amount: int
    start_time: datetime
    end_time: datetime
    duration: float

app = typer.Typer()
LIMIT = 100

def operacion(a,b):
    return a+b


def run(limit:int):
    start_time = datetime.now()
    # en vez de esto usar generadores
    for i,_ in enumerate(range(limit)):
        a = random.uniform(1,200)
        b = ord(random.choice(string.ascii_lowercase))
        resultado = operacion(a,b)
        print(f"{i} :: {a} + {b} = {resultado}")
    end_time = datetime.now()
    difference = (end_time-start_time).total_seconds()
    report = Report(limit, start_time, end_time, difference)
    return report

@app.command()
def main(start:int, end:int, step:int=1):
    start_time = datetime.now()
    reports = []
    with Pool(3) as p:
        limites = [v for v in range(start, end, step)]
        print("Limites", limites)
        report = p.map(run, limites)
        reports.append(report)
    df = pd.DataFrame(reports)
    df.to_csv(f"report_lineal_{start}_{end}_{step}.csv")
    end_time = datetime.now()
    difference = (end_time-start_time).total_seconds()
    print("Total duración operacion lineal", difference)

if __name__=="__main__":
    app()
