def validar_y_deserializar_mensaje(serializado: bytes) -> str:
    SOH = b'\x01'  # Símbolo SOH
    EOT = b'\x04'  # Símbolo EOT

    # Verificar que el mensaje comienza y termina correctamente
    if not (serializado.startswith(SOH) and serializado.endswith(EOT)):
        raise ValueError("Mensaje no tiene delimitadores SOH o EOT correctos")

    # Remover SOH y EOT
    contenido = serializado[1:-1]

    # Extraer largo (primeros 2 bytes) y calcular su valor
    largo_bytes = contenido[:2]
    largo = int.from_bytes(largo_bytes, byteorder="big")

    # Extraer texto y checksum
    texto_y_checksum = contenido[2:]
    texto = texto_y_checksum[:-1]
    checksum_recibido = texto_y_checksum[-1]

    # Validar el largo del texto
    if len(texto) != largo:
        raise ValueError(f"Largo del mensaje no coincide: esperado {largo}, recibido {len(texto)}")

    # Calcular checksum y validar
    checksum_calculado = sum(texto) % 256
    if checksum_calculado != checksum_recibido:
        raise ValueError("Checksum no válido")

    # Decodificar y devolver el texto
    return texto.decode("utf-8")

# Ejemplo de uso
serializado = b'\x01\x00\x1eEste es un mensaje serializado\xed\x04'
try:
    texto = validar_y_deserializar_mensaje(serializado)
    print("Mensaje decodificado:", texto)
except ValueError as e:
    print("Error:", e)
