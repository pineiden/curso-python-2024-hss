def create_msg(data: bytes) -> bytes:
    SOH = b'\x01'  # Símbolo SOH
    EOT = b'\x04'  # Símbolo EOT

    # Longitud del mensaje (sin incluir SOH, largo y EOT)
    largo = len(data)
    largo_bytes = largo.to_bytes(2, byteorder="big")  # Convertir el largo a 2 bytes (big-endian)

    # Calcular checksum (suma de todos los bytes, módulo 256)
    checksum = sum(data) % 256
    checksum_byte = checksum.to_bytes(1, byteorder="big")

    # Crear mensaje final
    mensaje = SOH + largo_bytes + data + checksum_byte + EOT
    return mensaje


texto = "Este es un mensaje serializado"
texto_serializado = texto.encode("utf-8")  # Convertir el texto a bytes
resultado = create_msg(texto_serializado)

# Mostrar resultado
print(resultado)  # Mensaje con SOH, largo, texto, checksum y EOT
