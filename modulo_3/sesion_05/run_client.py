import socket

SOCKET_PATH = "/tmp/mensajes.sock"

def main():
    # Crear un socket del tipo UNIX
    client_socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

    try:
        # Conectar al socket del servidor
        client_socket.connect(SOCKET_PATH)
        print(f"Conectado al servidor en {SOCKET_PATH}")

        while True:
            # Leer mensaje del usuario
            message = input("Escribe un mensaje (o 'salir' para terminar): ")
            if message.lower() == "salir":
                print("Cerrando cliente.")
                break

            # Enviar el mensaje al servidor
            client_socket.sendall(message.encode('utf-8'))

            # Recibir respuesta del servidor
            response = client_socket.recv(1024)
            print(f"Respuesta del servidor: {response.decode('utf-8')}")

    except Exception as e:
        print(e)


if __name__=="__main__":
    main()
