from comunicacionobjetos.serializacion import Serializer, Serializable
from dataclasses import dataclass


@dataclass
class Persona(Serializable):
    nombre: str
    apellido: str
    telefono: str
    email: str
    edad: int


@dataclass
class Puente(Serializable):
    nombre: str
    ubicacion: str
    latitud: float
    longitud: float


def test_persona_sd():
    persona = Persona(
        "Juan", 
        "Soto",
        "+56982148578",
        "jsoto@hackerspace.cl", 
        23)
    pbytes  = Serializer.serializar(persona)
    persona_recv = Serializer.deserializar(pbytes)
    assert persona==persona_recv, "Error en serializador"



def test_puente_sd():
    puente = Puente(
        "Cal y Canto", 
        "Santiago",
        -33.43,
        -70.65        
    )
    pbytes  = Serializer.serializar(puente)
    puente_recv = Serializer.deserializar(pbytes)
    assert puente==puente_recv, "Error en serializador"
