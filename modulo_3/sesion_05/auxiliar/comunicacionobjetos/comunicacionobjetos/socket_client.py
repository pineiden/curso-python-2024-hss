import socket
from .mensaje import Mensaje
from .serializacion import Serializer
from datetime import datetime, timezone
# Ruta del socket del servidor

def create_client(user:str, host:str, port:int):
    # Crear un socket del tipo UNIX
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Conectar al socket del servidor
        client_socket.connect_ex((host, port))
        print(f"Conectado al servidor en {host}:{port}")

        while True:
            # Leer mensaje del usuario
            message = input("Escribe un mensaje (o 'salir' para terminar): ")
            ahora = datetime.now(timezone.utc)
            instance = Mensaje(message, user, ahora)
            if message.lower() == "salir":
                print("Cerrando cliente.")
                break

            # convertir a bytes
            mbytes = Serializer.serializar(instance)
            # Enviar el mensaje al servidor
            client_socket.sendall(mbytes)

            # Recibir respuesta del servidor
            response = client_socket.recv(1024)
            mensaje_recibido =Serializer.deserializar(response)
            print("Recibido",mensaje_recibido)
            print(f"Respuesta del servidor: {mensaje_recibido}")

    except Exception as e:
        raise e

