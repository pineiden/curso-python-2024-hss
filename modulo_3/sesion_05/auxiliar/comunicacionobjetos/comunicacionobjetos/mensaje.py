from .serializacion import Serializable
from dataclasses import dataclass
from datetime import datetime


@dataclass
class Mensaje(Serializable):
    texto: str
    user: str
    timestamp: datetime
    

    def to_upper(self):
        self.texto = self.texto.upper()
        self.user = self.user.upper()
