from comunicacionobjetos.socket_server import define_server
import typer

app = typer.Typer()

@app.command()
def main(host:str, port:int):
    try:
        print(f"Activando server {host}:{port}")
        define_server(host, port)
    except Exception as e:
        raise e



if __name__=="__main__":
    app()
