from comunicacionobjetos.socket_client import create_client
import typer

app = typer.Typer()

@app.command()
def main(user:str, host:str, port:int):
    try:
        print(f"Activando server {host}:{port}")
        create_client(user, host, port)
    except Exception as e:
        raise e



if __name__=="__main__":
    app()
