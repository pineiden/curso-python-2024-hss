import pickle
import unicodedata
from dataclasses import dataclass
from abc import ABCMeta

class Serializable(metaclass=ABCMeta):
    pass


# es como un namespace
class Serializer:
    SOH = b'\x01'  # Símbolo SOH
    EOT = b'\x04'  # Símbolo EOT

    @staticmethod
    def serializar(instance: Serializable) -> bytes:
        assert isinstance(instance, Serializable), "No es serializable"
        # Serializar con pickle
        data = pickle.dumps(instance)

        # Calcular largo
        largo = len(data)
        largo_bytes = largo.to_bytes(2, byteorder="big")

        # Calcular checksum
        checksum = sum(data) % 256
        checksum_byte = checksum.to_bytes(1, byteorder="big")

        # Construir mensaje serializado
        mensaje = Serializer.SOH + largo_bytes + data + checksum_byte + Serializer.EOT
        return mensaje

    @staticmethod
    def deserializar(mensaje_serializado: bytes) -> Serializable:
        # Verificar delimitadores SOH y EOT
        if not (mensaje_serializado.startswith(Serializer.SOH) and mensaje_serializado.endswith(Serializer.EOT)):
            raise ValueError("Mensaje no tiene delimitadores SOH o EOT correctos")

        # Remover SOH y EOT
        contenido = mensaje_serializado[1:-1]

        # Extraer largo
        largo_bytes = contenido[:2]
        largo = int.from_bytes(largo_bytes, byteorder="big")

        # Extraer datos y checksum
        data_y_checksum = contenido[2:]
        data = data_y_checksum[:-1]
        checksum_recibido = data_y_checksum[-1]

        # Validar largo
        if len(data) != largo:
            raise ValueError(f"Largo del mensaje no coincide: esperado {largo}, recibido {len(data)}")

        # Validar checksum
        checksum_calculado = sum(data) % 256
        if checksum_calculado != checksum_recibido:
            raise ValueError("Checksum no válido")

        # Deserializar objeto con pickle
        instance = pickle.loads(data)
        return instance

