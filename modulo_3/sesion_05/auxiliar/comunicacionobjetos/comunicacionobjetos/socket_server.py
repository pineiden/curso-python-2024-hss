import socketserver
from .mensaje import Mensaje
from .serializacion import Serializer

class MiTCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        print("Received from {}:".format(self.client_address[0]))
        print(self.data)
        mensaje_recibido =Serializer.deserializar(self.data)
        print("Recibido",mensaje_recibido)

        mensaje_recibido.to_upper()
        mensaje_enviar =Serializer.serializar(mensaje_recibido)
        print("Enviar a cliente",mensaje_recibido)
        # just send back the same data, but upper-cased
        self.request.sendall(mensaje_enviar)



def define_server(host:str, port: int):
    assert 1000<port<65000, "Puerto debe ser positivo y menor a 65000"

    # Create the server, binding to localhost on port 9999
    with socketserver.TCPServer((host, port), MiTCPHandler) as server:
        # Activate the server; this will keep running until you
        # interrupt the program with Ctrl-C
        server.serve_forever()
