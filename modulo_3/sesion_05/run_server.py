import socket
from socket import AF_INET, AF_UNIX, SOCK_STREAM
from pathlib import Path

SOCKET_PATH = Path("/tmp/mensajes.sock")

if SOCKET_PATH.exists():
    SOCKET_PATH.unlink()

# crear linux socket
iface_unix = socket.socket(family=AF_UNIX, type=SOCK_STREAM)
# iface_tcp = socket.socket(family=AF_INET, type=SOCK_STREAM)

# asociar interface con path
try:
    iface_unix.bind(str(SOCKET_PATH))
    iface_unix.listen(5)
    while True:
        # Esperar por una conexión
        client_socket, client_address = iface_unix.accept()
        print("Cliente conectado.")

        # Manejar los datos recibidos del cliente
        try:
            while True:
                data = client_socket.recv(1024)  # Leer hasta 1024 bytes
                if not data:
                    break
                print(f"Recibido: {data.decode('utf-8')}")
                # Enviar una respuesta al cliente
                client_socket.sendall(b"Mensaje recibido")
        except Exception as e:
            print(f"Error al manejar el cliente: {e}")
        finally:
            # Cerrar la conexión con el cliente
            client_socket.close()
            print("Cliente desconectado.")
except Exception as e:
    print(f"Error en el servidor: {e}")
finally:
    # Asegurarse de cerrar el socket del servidor al salir
    iface_unix.close()
    if SOCKET_PATH.exists():
        SOCKET_PATH.unlink()
    print("Servidor cerrado.")
