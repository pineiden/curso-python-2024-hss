import asyncio
import sys

async def holacoro(dormir:float):
    for i in range(3):
        # esperar en segundos
        await asyncio.sleep(dormir)
        print(f"Hola {i}")


async def chaocoro(dormir:float):
    for i in range(3):
        # esperar en segundos
        await asyncio.sleep(dormir)
        print(f"Chao {i}")



if __name__=="__main__":
    print(sys.argv)
    dormir = float(sys.argv[1])
    print("Dormir", dormir)
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    tarea_futura_1 = holacoro(dormir)
    tarea_futura_2 = chaocoro(dormir)
    tareas = [
        asyncio.ensure_future(tarea_futura_1),
        asyncio.ensure_future(tarea_futura_2)
    ]

    loop.run_until_complete(
        asyncio.gather(*tareas)
    )
