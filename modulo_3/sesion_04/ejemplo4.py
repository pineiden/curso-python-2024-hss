import asyncio
import sys
from functools import partial
from datetime import datetime, timezone

async def holacoro(dormir:float):
    for i in range(3):
        # esperar en segundos
        await asyncio.sleep(dormir)
        now = datetime.now(timezone.utc)
        print(f"Hola {i} -> {now}")


def renew(loop, dormir,*args):
    task = loop.create_task(holacoro(dormir))
    renew_fn = partial(renew, loop, dormir, *args)
    # renew_fn()
    task.add_done_callback(renew_fn)


if __name__=="__main__":
    print(sys.argv)
    dormir = float(sys.argv[1])
    print("Dormir", dormir)
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)


    tarea_futura = asyncio.Task(holacoro(dormir))
    renew_fn = partial(renew, loop, dormir)
    tarea_futura.add_done_callback(renew_fn)

    try:
        loop.run_forever()
    except KeyboardInterrrupt:
        print("Loop stoped")
