from fastapi import FastAPI
from typing import Union
from pathlib import Path
import csv
from datetime import datetime, timezone

from .schemas import Herramienta

app = FastAPI()

estudiantes = {
    "cristobal":29,
    "simón":32,
    "francisca":31
}

archivo = Path(__file__).parent.resolve() / "animales.csv"
herramientas = Path(__file__).parent.resolve() / "herramientas.csv"

@app.get("/")
async def read_root():
    return {"mensaje":"Hola curso python"}


@app.get("/estudiante/{nombre}")
async def read_item(nombre:str):
    nombre = nombre.lower()
    return {"edad":estudiantes.get(nombre)}


@app.post("/animal/{nombre}/{pais}")
async def read_animal(nombre:str, pais:str):
    item = {
        "nombre":nombre,
        "pais":pais
    }
    flag = archivo.exists()
    with archivo.open("a") as f:
            
        writer = csv.DictWriter(f, fieldnames=["nombre","pais"])
        if not flag:
            writer.writeheader()
        writer.writerow(item)

    return {"registro":item, "msg":"ok"}




@app.post("/herramienta")
async def read_herramienta(
        herramienta:Herramienta):
    item = herramienta.dict()
    now = datetime.now(timezone.utc)
    item["timestamp"] = now.isoformat()
    flag = herramientas.exists()
    with herramientas.open("a") as f:
            
        writer = csv.DictWriter(
            f, 
            fieldnames=[
                "nombre",
                "descripcion", 
                "peso", 
                "timestamp"])
        if not flag:
            writer.writeheader()
        writer.writerow(item)

    return {"registro":item, "msg":"ok"}
