import asyncio
import sys

async def holacoro(dormir:float):
    for i in range(3):
        # esperar en segundos
        await asyncio.sleep(dormir)
        print(f"Hola {i}")


async def chaocoro(dormir:float):
    for i in range(3):
        # esperar en segundos
        await asyncio.sleep(dormir)
        print(f"Chao {i}")



if __name__=="__main__":
    print(sys.argv)
    dormir = float(sys.argv[1])
    print("Dormir", dormir)
    loop = asyncio.new_event_loop()
    executor = concurrent.futures.ProcessPoolExecutor()
    dhola = 1
    dchao = 2

    future1 = loop.run_in_executor(executor,
                                   funtools.partial(holacoro, dhola))
