sudoku_txt="""
530070000\n
600195000\n
098000060\n
800060003\n
400803001\n
700020006\n
060000280\n
000419005\n
000080079
"""

from pprint import pprint
from typing import Optional, Tuple, List, Dict
from functools import reduce
from copy import deepcopy


Sudoku=List[List[str]]
Size=Tuple[Optional[int], Optional[int]]
Position=Tuple[Optional[int], Optional[int]]

sudoku:Sudoku = [list(line) for line in sudoku_txt.split("\n") if line]

pprint("====Sudoku====")
pprint(sudoku)


def size(puzzle:Sudoku)->Size:
    if puzzle:
        n=len(puzzle)
        if puzzle[0]:
            m=len(puzzle[0])
            return n,m
    return None, None

print(size(sudoku))

def find_zero(board:Sudoku)-> Position:
    for i,row in enumerate(board):
        for j, value in enumerate(row):
            if value == "0":
                return (i,j)
    return (None, None)

def position_range(i:int)->Position:
    if 0 <= i <= 2:
        return [0,3]
    elif 3 <= i <= 5:
        return [3,6]
    elif 6 <= i <= 8:
        return [6,9]

def sector(board:Sudoku, position:Position) -> Sudoku:
    slice_row = slice(*position_range(position[0]))
    slice_column = slice(*position_range(position[1]))
    row_selection = board[slice_row]
    selection = [row[slice_column] for row in row_selection]
    return selection

def get_sectors(board:Sudoku, position:Position)->Dict[str,object]:
    i = position[0]
    j = position[1]
    row = board[i]
    column = [row[j] for row in board]
    sector_ = sector(board, position)
    return {"row":row, "column":column, "selection":sector_}


def conjuntos(sectors):
    row_set = set(sectors.get('row',{}))
    row_set.discard("0")
    column_set = set(sectors.get('column',{}))
    column_set.discard("0")
    sector_set = set(reduce(lambda a,b:a+b, sectors.get("selection",[])))
    sector_set.discard("0")
    total = {str(n) for n in range(1,10)}
    posibles = total - (sector_set | column_set | row_set)
    return {"row":row_set, "column":column_set, "sector":sector_set, "posibles":posibles}

def is_valid(value:int, numeros: set)->bool:
    return str(value) in numeros

def set_value(board:Sudoku, position:Position, value:int):
    board[position[0]][position[1]]=str(value)


def solve(puzzle:Sudoku)->Sudoku:
    new_puzzle = deepcopy(puzzle)
    i,j = find_zero(new_puzzle)
    if i is None and j is None:
        return new_puzzle
    else:
        sectors = get_sectors(new_puzzle, [i,j])
        numeros = sorted([int(e) for e in
    conjuntos(sectors).get('posibles',set())])
        for n in numeros:
            set_value(new_puzzle, [i,j], n)
            solution = solve(new_puzzle)
            if solution:
                return solution
        else:
            return []

solucion:Sudoku = solve(sudoku)
pprint("====Solución Sudoku=====")
pprint(solucion)
