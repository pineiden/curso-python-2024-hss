from arbol_binario.arbol import Nodo


def buscar(arbol:Nodo, memoria:float):
    # condicion de detención
    if arbol.derecha and arbol.izquierda:       
        memoria = memoria if memoria<arbol.valor else arbol.valor
        memoria = buscar(
            arbol.derecha, memoria)        
        memoria = buscar(
            arbol.izquierda, memoria)
        return memoria
    elif arbol.derecha is None and arbol.izquierda:
        return buscar(
            arbol.izquierda, memoria)
    elif arbol.derecha and arbol.izquierda is None:
        return buscar(
            arbol.derecha, memoria)
    else:
        return memoria if arbol.valor>memoria else arbol.valor

"""
Tarea:: buscar los nodos con números primos

- fn de números primos?
- N -> lista nro primos
- buscar cada nodo que tenga valor primo (que este en la lista)
- mostrar la lista final
"""



def main():
    arbol = Nodo(10)
    arbol.insert_izquierda(5)
    arbol.insert_derecha(-15)
    nodo_d = arbol.derecha
    nodo_d.insert_izquierda(1)
    nodo_d.insert_derecha(3)
    nodo_izq_d = nodo_d.izquierda
    nodo_izq_d.insert_derecha(7)
    print(arbol)
    resultado = buscar(arbol, 100)
    
    print(resultado)


if __name__=="__main__":
    main()
