from typing import Optional, TypeVar
from dataclasses import dataclass

Nodo = TypeVar('Nodo')

@dataclass
class Nodo:
    valor: float
    izquierda: Optional[Nodo] = None
    derecha: Optional[Nodo] = None


    def insert_izquierda(self, valor:float):
        self.izquierda = Nodo(valor)

    def insert_derecha(self, valor:float):
        self.derecha = Nodo(valor)



