def factorial(n:int)->int:
    try:
        if n>=0:
            if n==0:
                return 1
            else:
                return n * factorial(n-1)
        else:
            raise Exception("Negative value")
    except Exception as e:
        print(f"Valor ingresado no válido {n}")
        raise e


if __name__=="__main__":
    valor = input("Dame un número: ")
    if valor.isdigit():
        valor = int(valor)
        print(factorial(valor))
    else:
        print("El valor no es dígito")



