import time
from pathlib import Path
import json

def fibonacci(n:int)->int:
    if n<= 1:
        return n
    else:
        return fibonacci(n-1) + fibonacci(n-2)

memoria: dict[int, int] = {}

def fibonacci_mem(n:int)->int:
    if n in memoria:
        return memoria[n]
    if n<= 1:
        memoria[n] = n
    else:
        memoria[n] = fibonacci_mem(n-1) + fibonacci_mem(n-2)
    return memoria[n]

if __name__=="__main__":
    valor = input("Dame un número: ")

    if valor.isdigit():
        valor = int(valor)
        dataset = []
        start_g = time.time()

        for i in range(2,valor):
            print("Index", i)
            start = time.time()
            valor_a=fibonacci(i)
            end = time.time()
            basico = end-start

            start = time.time()
            valor_b=fibonacci_mem(i)
            end = time.time()
            memo = end-start
            item = {
                "valor":valor_a,
                "basico":basico, 
                "memo":memo, 
                "index":i}
            print(item)
            dataset.append(item)
        end_g = time.time()
        
        # serializamos
        data = {
            "total":end_g-start_g,
            "dataset":dataset
        }
        path = Path("fibonacci_comp.json")
        txt = json.dumps(data)
        path.write_text(txt)
        

    else:
        print("El valor no es dígito")



 
