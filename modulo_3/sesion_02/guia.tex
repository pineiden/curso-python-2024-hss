% Created 2024-10-10 jue 19:56
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\author{David Pineda}
\date{\today}
\title{Expresiones Regulares}
\hypersetup{
 pdfauthor={David Pineda},
 pdftitle={Expresiones Regulares},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.1 (Org mode 9.7-pre)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents

Es usual que en diversos problemas a los que nos enfrentamos
necesitemos buscar coincidencias en un texto o cadena de símbolos, así
como también reemplazar en base a ciertas condiciones.

Es un problema de computación base, que se puede modelar mediante la
definición de un lenguaje regular, en el libro \textbf{Introducción a la
teoría de la computación} de Sipser, es posible tener un acercamiento
teórico a este tipo de problemas.

Para enfrentar problemas con \emph{secuencias} de símbolos, en que el
computador tenga limitada su memoriam una solución muy buena es el uso
de  \textbf{máquinas de estado}. Estas soluciones se pueden aplicar tanto a
problemas sencillos como un semáforo y complejos como el control de
producción de una fábrica.
\section{Automáta finito.}
\label{sec:orgb99b719}

Las expresiones regulares se pueden modelar visualmente como un
autómata finito en una máquina de estados.

Como ejemplo, el caso de control de una puerta automática. Se abre
cuando siente que se aproxima una persona, al alejarse se cierra.

Esta puerta tiene dos sensores instalados a cada lado de la puerta
para detectar personas.

\begin{itemize}
\item Puerta tiene dos estados: \{abierta, cerrada\}
\end{itemize}

Condiciones:

\begin{description}
\item[{Frente}] persona se aproxima por el frente
\item[{Trasera}] persona viene por detras
\item[{Ambas}] si vienen por ambos lados
\item[{Ninguna}] si no vienen personas.
\end{description}

\begin{center}
\includegraphics[width=.9\linewidth]{img/base_diag.png}
\end{center}

Con esto, la tabla de estados se representa por:

\begin{center}
\begin{tabular}{lllll}
Estado & Ninguno & Delantera & Trasera & Ambas\\[0pt]
\hline
CERRADO & CERRADO & ABIERTO & CERRADO & CERRADO\\[0pt]
ABIERTO & CERRADO & ABIERTO & ABIERTO & ABIERTO\\[0pt]
\end{tabular}
\end{center}

Ahora, de manera más abstracta y general, se puede representar como el
autómata finito M\textsubscript{1}.

\begin{center}
\includegraphics[width=.9\linewidth]{generic.png}
\end{center}

Este código representa un autómata con los estados q1q1​, q2q2​, y q3q3​, donde:

\begin{itemize}
\item \(q_1​\) tiene una transición de bucle con 0 y pasa a q2​ con 1.
\item \(q_2​\) tiene una transición de bucle con 1 y pasa a q3​ con 0.
\item \(q_3\)​ tiene una transición de regreso a q2​ con 0 o 1, y tiene un bucle con 0.
\end{itemize}

El estado \(q_2\)​ es el estado de aceptación (indicado por el doble
círculo en el diagrama).

Al momento de computar o procesar una cadena  \textbf{1101}, la salida será
aceptado o rechazado. El autómata recibirá la cadena de símbolos desde
la izquierda a la derecha (1, 1, 0, 1) haciendo cambiar el estado del
dispositivo. 

\begin{enumerate}
\item Iniciar en el estado q1.
\item Leer 1, seguir la transición de q1 a q2.
\item Leer 1, seguir la transición de q2 a q2.
\item Leer 0, seguir la transición de q2 a q3.
\item Leer 1, seguir la transición de q3 a q2.
\item Aceptar porque M1 está en un estado de aceptación q2 al final de la entrada.
\end{enumerate}
\section{Encontrar secuencias de letras}
\label{sec:orga50e1c5}

En realidad el problema más común al que aplican las \textbf{expresiones
regulares} (regex en adelante), es sobre caracteres o textos.

Las \textbf{regex} se pueden definir y se pueden utilizar en diferentes
lenguajes de programación, son una herramienta más para gestionar la
complejidad de diferentes símbolos encadenados.

Si no dispusieramos de las \textbf{regex} para encontrar, por ejemplo la
coincidencia para la palabra "mamá" deberíamos hacer algo así:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
buscar = "mamá"
lista = [
    "taza", "padre", "madre", "perro", "mamá", "abuela"
]

for palabra in lista:
    primera = palabra[0]
    segunda = palabra[1]
    tercera = palabra[2]
    cuarta = palabra[3]
    if primera=="m" and segunda=="a" and tercera=="m" and cuarta=="á":
        print("Palabra encontrada")
\end{minted}

Esto puede ser relativamente sencillo y reemplazable por:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
buscar = "mamá"
lista = [
    "taza", "padre", "madre", "perro", "mamá", "abuela"
]

for palabra in lista:
    if palabra==buscar:
        print("Palabra encontrada")
\end{minted}

Pero, ¿qué pasa si buscamos un conjunto de palabras y variantes como
\{papá, mamá\}?


\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
buscar = {"mamá", "papá"}
lista = [
    "taza", "padre", "madre", "perro", "mamá", "abuela"
]

for palabra in lista:
    if palabra in buscar:
        print("Palabra encontrada")
\end{minted}

Hasta este punto la síntaxis que provee el lenguaje \textbf{python} permite
hacer búsquedas en textos de manera compacta, sin embargo habrán casos
en que sí será necesario definir una \textbf{regex}.

De moment solo con python será posible encontrar:

\begin{itemize}
\item concidencia directa
\item texto comienza con una cadena
\item texto finaliza con cadena
\item cadena existe en el texto.
\end{itemize}

Estas son los casos más típicos que se podrían encontrar, pero
necesitaremos a veces definir algo más general y potente, para eso
usaremos \textbf{regex}.
\section{Guía didáctica para enseñar expresiones regulares}
\label{sec:org3494f40}


\subsection{1. Introducción: ¿Qué son las expresiones regulares?}
\label{sec:orgc4b9fed}

\begin{itemize}
\item \textbf{\textbf{Objetivo:}} Entender la definición y propósito de las regex.
\item \textbf{\textbf{Explicación:}} 
Las expresiones regulares son patrones que se utilizan para buscar o manipular texto. Pueden parecer complicadas al principio, pero son muy poderosas cuando las entiendes.
\item \textbf{\textbf{Ejemplo simple:}} 
\begin{itemize}
\item `hola` buscará la palabra "hola" en un texto.
\end{itemize}
\item \textbf{\textbf{Actividad:}} 
\begin{itemize}
\item Pedir a los estudiantes que busquen su nombre en un párrafo de texto utilizando un simple patrón.
\end{itemize}
\end{itemize}
\subsection{2. Conceptos básicos: Caracteres y metacaracteres}
\label{sec:orga50f26e}

\begin{itemize}
\item \textbf{\textbf{Objetivo:}} Aprender los componentes básicos de las regex.
\item \textbf{\textbf{Caracteres literales:}} 
Son los caracteres que se buscan tal cual.
\begin{itemize}
\item Ejemplo: El patrón `casa` buscará exactamente "casa".
\end{itemize}
\item \textbf{\textbf{Metacaracteres:}} 
Son caracteres con significados especiales.
\begin{itemize}
\item Ejemplo: `.` (punto) representa cualquier carácter.
\end{itemize}
\item \textbf{\textbf{Actividad:}} 
\begin{itemize}
\item Proporcionar una lista de palabras y pedir que usen el punto para buscar palabras con un número específico de letras.
\end{itemize}
\end{itemize}
\subsection{3. Clases de caracteres y rangos}
\label{sec:org0c96b14}

\begin{itemize}
\item \textbf{\textbf{Objetivo:}} Usar clases de caracteres para crear patrones más flexibles.
\item \textbf{\textbf{Explicación:}}
\begin{itemize}
\item `[abc]` busca cualquiera de los caracteres 'a', 'b', o 'c'.
\item `[a-z]` busca cualquier letra minúscula.
\item `\d` busca cualquier dígito (equivalente a `[0-9]`).
\end{itemize}
\item \textbf{\textbf{Ejemplo:}} 
\begin{itemize}
\item Buscar cualquier vocal en un texto: `[aeiou]`.
\end{itemize}
\item \textbf{\textbf{Actividad:}} 
\begin{itemize}
\item Pedir a los estudiantes que busquen todas las letras mayúsculas en un texto utilizando rangos `[A-Z]`.
\end{itemize}
\end{itemize}
\subsection{4. Cuantificadores}
\label{sec:orgf76db4e}

\begin{itemize}
\item \textbf{\textbf{Objetivo:}} Controlar cuántas veces debe aparecer un carácter o grupo en una coincidencia.
\item \textbf{\textbf{Explicación de los cuantificadores:}}
\begin{itemize}
\item `*` (cero o más veces)
\item `+` (una o más veces)
\item `\{n\}` (exactamente n veces)
\item `\{n,m\}` (entre n y m veces)
\end{itemize}
\item \textbf{\textbf{Ejemplo:}} 
\begin{itemize}
\item Buscar una secuencia de una o más vocales: `[aeiou]+`.
\end{itemize}
\item \textbf{\textbf{Actividad:}} 
\begin{itemize}
\item Proporcionar un texto con números y pedir que encuentren secuencias de dígitos de al menos 3 dígitos: `\d{3,}`.
\end{itemize}
\end{itemize}
\subsection{5. Anclas: \^{} y \$}
\label{sec:orgb4f85a2}

\begin{itemize}
\item \textbf{\textbf{Objetivo:}} Controlar la posición de la coincidencia dentro del texto.
\item \textbf{\textbf{Explicación:}}
\begin{itemize}
\item `\^{}` coincide con el inicio de una línea.
\item `\$` coincide con el final de una línea.
\end{itemize}
\item \textbf{\textbf{Ejemplo:}} 
\begin{itemize}
\item Buscar una palabra que esté al principio de una línea: `\textsuperscript{hola}`.
\end{itemize}
\item \textbf{\textbf{Actividad:}} 
\begin{itemize}
\item Dar un bloque de texto y pedir a los estudiantes que encuentren líneas que terminen en un signo de interrogación: `$\backslash$?\$`.
\end{itemize}
\end{itemize}
\subsection{6. Grupos y referencias}
\label{sec:org7455340}

\begin{itemize}
\item \textbf{\textbf{Objetivo:}} Aprender a agrupar partes de una expresión y referenciarlas.
\item \textbf{\textbf{Explicación:}}
\begin{itemize}
\item Los paréntesis `()` agrupan parte de una expresión.
\item `$\backslash$1` se refiere al primer grupo capturado.
\end{itemize}
\item \textbf{\textbf{Ejemplo:}} 
\begin{itemize}
\item Buscar palabras repetidas en una oración: `(\b\w+\b) $\backslash$1`.
\end{itemize}
\item \textbf{\textbf{Actividad:}} 
\begin{itemize}
\item Proporcionar un texto con palabras repetidas y pedir a los estudiantes que las identifiquen.
\end{itemize}
\end{itemize}
\subsection{7. Opcionalidad y Alternancia}
\label{sec:orge75dcd0}

\begin{itemize}
\item \textbf{\textbf{Objetivo:}} Crear patrones más flexibles con opcionalidad y alternancia.
\item \textbf{\textbf{Explicación:}}
\begin{itemize}
\item `?` hace que un carácter sea opcional.
\item `|` permite la alternancia (o).
\end{itemize}
\item \textbf{\textbf{Ejemplo:}} 
\begin{itemize}
\item Buscar "color" o "colour": `colou?r`.
\end{itemize}
\item \textbf{\textbf{Actividad:}} 
\begin{itemize}
\item Crear una expresión que permita encontrar diferentes variantes de una palabra (por ejemplo, "comportamiento" y "comportamientos").
\end{itemize}
\end{itemize}
\subsection{8. Aplicaciones prácticas}
\label{sec:org8e35dbf}

\begin{itemize}
\item \textbf{\textbf{Objetivo:}} Mostrar cómo se usan las regex en el mundo real.
\item \textbf{\textbf{Ejemplos:}}
\begin{itemize}
\item Validar un correo electrónico: `\w+@\w+$\backslash$.\w{2,3}`
\item Buscar URLs en un texto.
\end{itemize}
\item \textbf{\textbf{Actividad final:}} 
\begin{itemize}
\item Pedir a los estudiantes que creen expresiones regulares para validar números de teléfono, direcciones de correo electrónico y fechas en formato `dd/mm/yyyy`.
\end{itemize}
\end{itemize}
\subsection{9. Recursos adicionales:}
\label{sec:org71d99cc}

\begin{itemize}
\item \textbf{\textbf{Herramientas online para probar regex:}}
\begin{itemize}
\item \href{https://regex101.com}{Regex101} y \href{https://regexr.com}{RegExr}.
\end{itemize}
\item \textbf{\textbf{Tutoriales interactivos:}}
\begin{itemize}
\item \href{https://regexone.com}{RegexOne} es excelente para practicar de manera progresiva.
\end{itemize}
\end{itemize}
\subsection{10. Consejo didáctico:}
\label{sec:orgdaad9a4}

Para hacerlo más dinámico, usa juegos o retos en los que los estudiantes necesiten resolver problemas utilizando regex, haciendo que el aprendizaje sea más lúdico y aplicado.
\subsection{Enlaces}
\label{sec:orgfb57ac1}

\url{https://adictosaltrabajo.com/2015/01/29/regexsam/}
\section{Referencias académicas y libros sobre Expresiones Regulares}
\label{sec:org19278d3}

\subsection{1. Libros sobre Expresiones Regulares}
\label{sec:orgc94ec94}
\begin{itemize}
\item \textbf{\textbf{"Mastering Regular Expressions"}} por Jeffrey E. F. Friedl
\begin{itemize}
\item Este es uno de los libros más completos sobre expresiones regulares. Cubre desde los fundamentos hasta los detalles más avanzados.
\item \textbf{\textbf{Nivel:}} Intermedio a avanzado.
\item \textbf{\textbf{Enlace:}} Disponible en Amazon y otras librerías.
\end{itemize}

\item \textbf{\textbf{"Regular Expressions Cookbook"}} por Jan Goyvaerts y Steven Levithan
\begin{itemize}
\item Ofrece una variedad de patrones de expresiones regulares que pueden ser utilizados en diferentes lenguajes de programación.
\item \textbf{\textbf{Nivel:}} Principiante a intermedio.
\item \textbf{\textbf{Enlace:}} Disponible en Amazon y otras librerías.
\end{itemize}

\item \textbf{\textbf{"Introducing Regular Expressions"}} por Michael Fitzgerald
\begin{itemize}
\item Guía introductoria ideal para principiantes. Explica los conceptos básicos de manera accesible con ejemplos sencillos.
\item \textbf{\textbf{Nivel:}} Principiante.
\item \textbf{\textbf{Enlace:}} Disponible en O'Reilly y otras plataformas.
\end{itemize}
\end{itemize}
\subsection{2. Cursos en línea y tutoriales}
\label{sec:org1f2ba5f}
\begin{itemize}
\item \textbf{\textbf{Coursera: "Introduction to Regular Expressions"}}
\begin{itemize}
\item Cursos en Coursera que cubren regex dentro de la ciencia de datos y la programación.
\item \textbf{\textbf{Nivel:}} Principiante a intermedio.
\item \textbf{\textbf{Enlace:}} \url{https://www.coursera.org/}
\end{itemize}

\item \textbf{\textbf{RegexOne (Tutorial Interactivo)}}
\begin{itemize}
\item Recurso en línea interactivo que enseña regex de manera gradual con ejemplos prácticos ejecutables en el navegador.
\item \textbf{\textbf{Nivel:}} Principiante.
\item \textbf{\textbf{Enlace:}} \url{https://regexone.com}
\end{itemize}
\end{itemize}
\subsection{3. Referencias académicas}
\label{sec:org4e6287e}
\begin{itemize}
\item \textbf{\textbf{"The Theory of Parsing, Translation, and Compiling"}} por Alfred V. Aho y Jeffrey D. Ullman
\begin{itemize}
\item Libro teórico sobre análisis de lenguajes formales y compiladores. Incluye capítulos sobre regex y autómatas finitos.
\item \textbf{\textbf{Nivel:}} Avanzado.
\item \textbf{\textbf{Enlace:}} Disponible en bibliotecas académicas y editoriales especializadas.
\end{itemize}

\item \textbf{\textbf{"Introduction to the Theory of Computation"}} por Michael Sipser
\begin{itemize}
\item Libro sobre teoría de la computación, ofrece una sección útil sobre lenguajes regulares y autómatas finitos.
\item \textbf{\textbf{Nivel:}} Avanzado.
\item \textbf{\textbf{Enlace:}} Disponible en bibliotecas académicas.
\end{itemize}
\end{itemize}
\subsection{4. Publicaciones académicas sobre Expresiones Regulares}
\label{sec:org8ebd940}
\begin{itemize}
\item \textbf{\textbf{"Efficient Regular Expression Evaluation"}} por Russ Cox
\begin{itemize}
\item Artículo técnico sobre la eficiencia en la evaluación de regex, explicando cómo los motores modernos optimizan la ejecución.
\item \textbf{\textbf{Nivel:}} Intermedio a avanzado.
\item \textbf{\textbf{Enlace:}} \url{https://swtch.com/\~rsc/regexp/regexp1.html}
\end{itemize}

\item \textbf{\textbf{"Regular Expression Matching Can Be Simple And Fast"}} por Russ Cox
\begin{itemize}
\item Visión técnica detallada sobre la optimización del rendimiento de motores de expresiones regulares.
\item \textbf{\textbf{Nivel:}} Intermedio a avanzado.
\item \textbf{\textbf{Enlace:}} \url{https://swtch.com/\~rsc/regexp/regexp1.html}
\end{itemize}
\end{itemize}
\section{Ejemplos avanzados de expresiones regulares con resolución en Python (multilínea)}
\label{sec:org1d4ac9e}

\subsection{1. Ejemplos de búsqueda (multilínea):}
\label{sec:orgbeb8623}

\textbf{*} 1.1. Búsqueda simple de palabras clave en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)casa`
\item \textbf{\textbf{Descripción:}} Busca la palabra "casa" en un texto de varias líneas.
\item \textbf{\textbf{Ejemplo de uso:}}
\begin{itemize}
\item Texto:
"La casa es azul.\nEn la otra calle hay otra casa."
\end{itemize}
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     import re

     texto = """La casa es azul.
     En la otra calle hay otra casa."""
     
     resultados = re.findall(r'(?m)casa', texto)
     print(resultados)
     # Resultado: ['casa', 'casa']
\end{minted}
\end{itemize}

\textbf{*} 1.2. Búsqueda de cualquier carácter en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)c.sa`
\item \textbf{\textbf{Descripción:}} Busca "c" seguido de cualquier carácter, luego "sa" en varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """La casa es azul.
     Mi cosa favorita es leer."""
     
     resultados = re.findall(r'(?m)c.sa', texto)
     print(resultados)
     # Resultado: ['casa', 'cosa']
\end{minted}
\end{itemize}

\textbf{*} 1.3. Búsqueda de un rango de letras en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)[A-Za-z]`
\item \textbf{\textbf{Descripción:}} Busca cualquier letra mayúscula o minúscula en un texto de varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """123 abc
     DEF ghi."""
     
     resultados = re.findall(r'(?m)[A-Za-z]', texto)
     print(resultados)
     # Resultado: ['a', 'b', 'c', 'D', 'E', 'F', 'g', 'h', 'i']
\end{minted}
\end{itemize}

\textbf{*} 1.4. Búsqueda de secuencias de dígitos en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)\d+`
\item \textbf{\textbf{Descripción:}} Busca secuencias de uno o más dígitos en un texto de varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """Hay 3 perros y
     15 gatos."""
     
     resultados = re.findall(r'(?m)\d+', texto)
     print(resultados)
     # Resultado: ['3', '15']
\end{minted}
\end{itemize}

\textbf{*} 1.5. Búsqueda con anclas (inicio y fin de línea)
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)\textsuperscript{Hola}`
\item \textbf{\textbf{Descripción:}} Busca la palabra "Hola" al inicio de cualquier línea en un texto de varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """Hola mundo.
     Adiós mundo.
     Hola de nuevo."""
     
     resultados = re.findall(r'(?m)^Hola', texto)
     print(resultados)
     # Resultado: ['Hola', 'Hola']
\end{minted}
\end{itemize}
\subsection{2. Ejemplos de matching con grupos (multilínea):}
\label{sec:org6e2b20e}

\textbf{*} 2.1. Matching de palabras repetidas en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)(\b\w+\b)\s+$\backslash$1`
\item \textbf{\textbf{Descripción:}} Busca una palabra seguida de la misma palabra repetida en un texto de varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """Repite repite lo mismo.
     Y vuelve a repetir repetir en otra línea."""
     
     resultados = re.findall(r'(?m)(\b\w+\b)\s+\1', texto)
     print(resultados)
     # Resultado: ['Repite', 'repetir']
\end{minted}
\end{itemize}

\textbf{*} 2.2. Matching de correos electrónicos en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)(\w+@\w+$\backslash$.\w{2,3})`
\item \textbf{\textbf{Descripción:}} Busca correos electrónicos simples en un texto de varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """Mi correo es ejemplo@dominio.com.
     Otro correo es persona@mail.com."""
     
     resultados = re.findall(r'(?m)(\w+@\w+\.\w{2,3})', texto)
     print(resultados)
     # Resultado: ['ejemplo@dominio.com', 'persona@mail.com']
\end{minted}
\end{itemize}

\textbf{*} 2.3. Matching con alternancia (opcionalidad) en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)colou?r`
\item \textbf{\textbf{Descripción:}} Busca la palabra "color" o "colour" en un texto de varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """I like color.
     I also like colour."""
     
     resultados = re.findall(r'(?m)colou?r', texto)
     print(resultados)
     # Resultado: ['color', 'colour']
\end{minted}
\end{itemize}

\textbf{*} 2.4. Matching de fechas en formato dd/mm/yyyy en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)(\d{2})/(\d{2})/(\d{4})`
\item \textbf{\textbf{Descripción:}} Busca fechas en formato `dd/mm/yyyy` en varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """Hoy es 03/10/2024.
     Mañana será 04/10/2024."""
     
     resultados = re.findall(r'(?m)(\d{2})/(\d{2})/(\d{4})', texto)
     print(resultados)
     # Resultado: [('03', '10', '2024'), ('04', '10', '2024')]
\end{minted}
\end{itemize}

\textbf{*} 2.5. Matching de números de teléfono en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)($\backslash$+\d{1,3})? ?\(?(\d{3})\)?[ -]?(\d{3})[ -]?(\d{4})`
\item \textbf{\textbf{Descripción:}} Busca números de teléfono en varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """Llámenme al +1 (123) 456-7890.
     O al 123 456 7890."""
     
     resultados = re.findall(r'(?m)(\+\d{1,3})? ?\(?(\d{3})\)?[ -]?(\d{3})[ -]?(\d{4})', texto)
     print(resultados)
     # Resultado: [('+1', '123', '456', '7890'), ('', '123', '456', '7890')]
\end{minted}
\end{itemize}
\subsection{3. Ejemplos de reemplazo (multilínea):}
\label{sec:org24adb1d}

\textbf{*} 3.1. Reemplazo simple de palabras en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)gato`
\item \textbf{\textbf{Reemplazo:}} `perro`
\item \textbf{\textbf{Descripción:}} Reemplaza todas las ocurrencias de "gato" por "perro" en varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """El gato es ágil.
     Hay otro gato en la calle."""
     
     resultado = re.sub(r'(?m)gato', 'perro', texto)
     print(resultado)
     # Resultado: "El perro es ágil.\nHay otro perro en la calle."
\end{minted}
\end{itemize}

\textbf{*} 3.2. Reemplazo de múltiples espacios por uno solo en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)\s+`
\item \textbf{\textbf{Reemplazo:}} ` `
\item \textbf{\textbf{Descripción:}} Reemplaza múltiples espacios consecutivos por un solo espacio en varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """Muchos    espacios    aquí
     y    allá."""
     
     resultado = re.sub(r'(?m)\s+', ' ', texto)
     print(resultado)
     # Resultado: "Muchos espacios aquí y allá."
\end{minted}
\end{itemize}

\textbf{*} 3.3. Reemplazo de formato de fechas en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)(\d{2})/(\d{2})/(\d{4})`
\item \textbf{\textbf{Reemplazo:}} `\$3-\$2-\$1`
\item \textbf{\textbf{Descripción:}} Reemplaza fechas en formato `dd/mm/yyyy` por `yyyy-mm-dd` en varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """La fecha es 03/10/2024.
     Otra fecha es 04/10/2024."""
     
     resultado = re.sub(r'(?m)(\d{2})/(\d{2})/(\d{4})', r'\3-\2-\1', texto)
     print(resultado)
     # Resultado: "La fecha es 2024-10-03.\nOtra fecha es 2024-10-04."
\end{minted}
\end{itemize}

\textbf{*} 3.4. Reemplazo de correos electrónicos por "anonimizados" en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)(\w+)@(\w+$\backslash$.\w{2,3})`
\item \textbf{\textbf{Reemplazo:}} `anon@anon.com`
\item \textbf{\textbf{Descripción:}} Reemplaza los correos electrónicos con una dirección genérica en varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """Mi correo es ejemplo@dominio.com.
     Otro correo es persona@mail.com."""
     
     resultado = re.sub(r'(?m)(\w+)@(\w+\.\w{2,3})', 'anon@anon.com', texto)
     print(resultado)
     # Resultado: "Mi correo es anon@anon.com.\nOtro correo es anon@anon.com."
\end{minted}
\end{itemize}

\textbf{*} 3.5. Reemplazo de tags HTML en varias líneas
\begin{itemize}
\item \textbf{\textbf{Regex:}} `(?m)<.*?>`
\item \textbf{\textbf{Reemplazo:}} ``
\item \textbf{\textbf{Descripción:}} Elimina cualquier etiqueta HTML en un texto de varias líneas.
\item \textbf{\textbf{Código Python:}}
\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
     texto = """<p>Este es un <b>texto</b> con HTML.</p>
     <div>Otro contenido</div>"""
     
     resultado = re.sub(r'(?m)<.*?>', '', texto)
     print(resultado)
     # Resultado: "Este es un texto con HTML.\nOtro contenido"
\end{minted}
\end{itemize}
\end{document}
