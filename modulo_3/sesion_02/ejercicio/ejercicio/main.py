import re
import typer
from pathlib import Path

regex_str = r"^(test_)?proyecto_genoma_([12]\d{3}-[01]\d-[0123]\d)_[a-z]{3,}.csv$" 
regex = re.compile(regex_str)

app = typer.Typer()

@app.command()
def main(path:Path):
    filename = path.name
    if regex.match(str(filename)):
        print("Processing", path)
    else:
        print("No matching", path)


if __name__=="__main__":
    app()
