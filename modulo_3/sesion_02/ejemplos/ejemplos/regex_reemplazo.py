import re

numeros = [
    "357",
    "4393",
    "3.14",
    "19000077MM",
    "403-1",
    "24500-03",
    "2.7324",
    "MDS503",
    "234234234",
    ".23232"
]


"""
Regex para reemplazar
"""



def str_a_nro(valor):
    regex = re.compile("^(\d*\.?\d+)$")
    reemplazo = regex.sub(r"NRO_\1", valor)
    return reemplazo

for linea in numeros:
    v = str_a_nro(linea)
    if v.startswith("NRO"):
        print(v)
    else:
        print("___", v)
