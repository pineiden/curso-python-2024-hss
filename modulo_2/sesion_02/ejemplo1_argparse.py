import argparse
from dataclasses import dataclass
from pathlib import Path
from rich import print
from ejemplo_decorador import mi_decorador

@dataclass
class WC:
    lines:int
    chars:int


def wc(filename:str, primera_palabra:bool=False)->WC:
    counter_lines = 0
    counter_chars = 0
    lista_palabras = []
    with open(filename,"r") as f:
        for line in f.readlines():
            line = line.replace("\n","")
            counter_lines += 1
            counter_chars += len(line)
            if primera_palabra:
                palabra = line.split(" ")[0]
                if palabra!="":
                    lista_palabras.append(palabra)

    if primera_palabra:
        print(lista_palabras)
    return WC(counter_lines, counter_chars)
            

@mi_decorador
def run(*args,**kwargs):
    parser = argparse.ArgumentParser(
        prog="Programa de ejemplo",
        description="Este es un ejemplo de uso de argparse",
        epilog="se puede usar en la línea de comandos")

    parser.add_argument("filename", type=Path, help="ruta al archivo")
    parser.add_argument("-p", "--primera_palabra", default=True,action=argparse.BooleanOptionalAction)
    # con esta acción se activa el parser con los argumentos
    args = parser.parse_args()
    if args.filename.exists():
        print("Primera palabra", args.primera_palabra)
        result = wc(args.filename, args.primera_palabra)
        print(result)
    else:
        print(f"No existe {args.filename}")


if __name__=="__main__":
    run()
