import random
from datetime import datetime 
from typing import Dict,List,Any
from dataclasses import dataclass, asdict
from rich import print
import math
from pathlib import Path
import json


lado = lambda: random.uniform(0.1,100)
names = ("a","b","c")


@dataclass
class Triangulo:
    a: float 
    b: float 
    c: float
    id: int
    timestamp: datetime

    def __init__(self):
        self.a = lado()
        self.b = lado()
        self.c = random.uniform(abs(self.a-self.b), abs(self.a+self.b))

    def set_id(self, nro:int):
        self.id = nro
    
    def set_timestamp(self, ts:datetime):
        self.timestamp = ts
        
    @property
    def perimetro(self):
        return self.a+self.b+self.c

    @property
    def area(self):
        s = (self.a+self.b+self.c)
        return math.sqrt((s-self.a) +(s-self.b)+(s-self.c))

    @staticmethod
    def genera_triangulos(n:int)->'Triangulo':
        for i in range(n):
            trg = Triangulo()
            trg.set_id(i)
            trg.set_timestamp(datetime.now())
            yield trg

    def to_dict(self):
        base = asdict(self)
        base.update({"area":self.area, "perimetro":self.perimetro})
        base["timestamp"] = base["timestamp"].isoformat()
        return base




if __name__=="__main__":
    n  = 10
    triangulos_path = Path("triangulos.json")
    fieldnames = ["id","timestamp","a","b","c","area","perimetro"]
    dataset = []

    for trg in Triangulo.genera_triangulos(n):
        item = trg.to_dict()
        dataset.append(item)

    txt = json.dumps(dataset)
    triangulos_path.write_text(txt)

