from tkinter import * 
from tkinter import ttk

class Ventana:
    nombre:str
    root:Tk

    def __init__(self, nombre:str):
        self.nombre = nombre
        self.root = Tk()
        self.load()

    def load(self):
        # layout
        frm = ttk.Frame(self.root, padding=10)
        frm.grid()

        # ubicar widgets
        ttk.Label(
            frm, 
            text=self.nombre).grid(column=1,row=0)

        ttk.Button(
            frm, 
            text="Salir", 
            command=self.cerrar).grid(column=1, row=3)

    def cerrar(self):
        self.root.destroy()

    def run(self):
        self.root.mainloop()


if __name__=="__main__":
    ventana = Ventana("Prueba de Tk")
    ventana.run()
