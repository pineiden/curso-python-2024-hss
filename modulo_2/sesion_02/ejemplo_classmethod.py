from dataclasses import dataclass
from typing import Any
from rich import print

@dataclass
class Persona:
    nombre:str 
    apellido:str 

    @classmethod
    def from_dict(cls, item:dict[str,Any]):
        campos = {"nombre", "apellido"}
        dato = {k:v.capitalize() for k,v in item.items() if k in campos}
        return cls(**dato)



if __name__=="__main__":
    item = {
        "nombre":"juanito",
        "apellido":"zarapato",
        "mascota":"Tobi",
        "edad":34
    }
    persona = Persona.from_dict(item)
    print(persona)

