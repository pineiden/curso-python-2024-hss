from dataclasses import dataclass


@dataclass
class Persona:
    nombre:str
    apellido:str

    @property
    def identificacion(self):
        return f"{self.nombre.capitalize()} {self.apellido.capitalize()}"



if __name__=="__main__":
    persona = Persona("juan", "pérez")
    print(persona.identificacion)
