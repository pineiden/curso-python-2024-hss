import csv
from pathlib import Path
import argparse
from dataclasses import dataclass
from typing import Any
from rich import print

@dataclass
class Pais:
    nombre:str
    iso:str
    codigo_telefono:str

    @classmethod
    def from_dict(cls, item:dict[str, Any]):
        filtro = {
            "nombre":"nombre", 
            "iso2":"iso",
            "phone_code":"codigo_telefono"}
        
        dato = {filtro[k.strip()]:v for k,v in item.items() if k.strip() in filtro}
        return cls(**dato)

def read_csv(ruta:Path):
    with ruta.open() as f:
        reader = csv.DictReader(f)
        for item in reader:
            yield Pais.from_dict(item)


def run(*args,**kwargs):
    parser = argparse.ArgumentParser(
        prog="Programa de ejemplo",
        description="Este es un ejemplo de uso de argparse",
        epilog="se puede usar en la línea de comandos")

    parser.add_argument("filename", type=Path, help="ruta al archivo")
    # con esta acción se activa el parser con los argumentos
    args = parser.parse_args()
    for pais in read_csv(args.filename):
        print(pais)


if __name__=="__main__":
    run()
