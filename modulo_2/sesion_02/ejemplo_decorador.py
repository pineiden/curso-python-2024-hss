def mi_decorador(funcion,*args,**kwargs):
    def nueva_funcion(*args,**kwargs):
        print("================")
        c = funcion(*args, **kwargs)
        print("================")
        return c

    return nueva_funcion
