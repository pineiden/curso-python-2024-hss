from dataclasses import dataclass
import math

@dataclass
class Punto:
    x:float 
    y:float 


@dataclass
class Distancias:
    @staticmethod
    def euclidiana(p1:Punto,p2:Punto):
        dx2 = (p1.x - p2.x)**2
        dy2 = (p1.y - p2.y)**2
        return math.sqrt(dx2 + dy2)

    @staticmethod
    def geografica(p1:Punto,p2:Punto):
        pass


if __name__=="__main__":
    p1 = Punto(-10, 3)
    p2 = Punto(5, 23)
    distancias = Distancias()
    distancia = distancias.euclidiana(p1,p2)

    print(f"La distancia entre {p1} y {p2} es {distancia}")
