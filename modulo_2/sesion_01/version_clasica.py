from typing import Any
from datetime import datetime
from rich import print
import copy

class Persona:
    nombre:str
    apellido:str
    acciones:dict[str, Any] 

    def __init__(self, nombre:str, apellido:str):
        self.nombre = nombre
        self.apellido = apellido
        self.acciones = {}

    def __repr__(self):
        return f"Persona({self.nombre}, {self.apellido}, {self.acciones})"

    def __str__(self):
        return f"Persona({self.nombre}, {self.apellido}, {self.acciones})"

    def eliminar(self, objeto):
        del objeto

    def agregar_accion(self, accion):
        ahora = datetime.now()
        self.acciones[ahora] = accion



class Botella:
    volumen: int
    material: str = "vidrio"
    contenido: str = "agua"
    color: str = "transparente"

    def __init__(
            self, 
            volumen, 
            material="vidrio", 
            contenido="agua", 
            color="transparente"):

        self.volumen=volumen
        self.materiaL=material
        self.contenido=contenido
        self.color=color

    def vaciar(self):
        self.volumen = 0

    def sacar(self, cantidad:int=0):
        if cantidad<=self.volumen:
            self.volumen -= cantidad
        else:
            self.vaciar()

    def agregar(self, cantidad:int=0):
        self.volumen += cantidad

    def __repr__(self):
        return f"Botella({self.volumen})"

    def __str__(self):
        return f"Botella({self.volumen})"

from collections import namedtuple

Accion = namedtuple(
    "Accion", 
    ["nombre", "cantidad", "objeto", "entrada"])

def vaciar(
        persona:Persona, 
        botella:Botella, 
        cuanto:int=0):
    accion = Accion(
        "vaciar", 
        botella.volumen, 
        type(botella).__name__, 
        cuanto)
    botella.sacar(cuanto)
    persona.agregar_accion(accion)


if __name__=="__main__":
    botella = Botella(1000)
    persona = Persona("Juan","Perez")
    while (cuanto:=input("Cuanto descargar botella?"))!="END":
        cuanto = int(cuanto)
        print("Antes", botella)
        vaciar(persona, botella, cuanto)
        print("Despues", botella)
        print("Quien vacia", persona)

