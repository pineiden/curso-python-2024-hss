from typing import Any
from datetime import datetime
from rich import print
import copy
from collections import namedtuple
from dataclasses import dataclass, field, asdict

Accion = namedtuple(
    "Accion", 
    ["nombre", "cantidad", "objeto", "entrada"])

@dataclass
class Persona:
    nombre:str
    apellido:str
    acciones:dict[datetime,Accion] = field(default_factory=dict)
    

    def agregar_accion(self, accion:Accion):
        ahora = datetime.now()
        self.acciones[ahora] = accion

@dataclass
class Botella:
    volumen: int 
    material: str = "vidrio"
    contenido: str = "agua"
    color: str = "transparente"

    def sacar(self, cantidad:int=0):
        if cantidad<=self.volumen:
            self.volumen -= cantidad
        else:
            self.vaciar()

    def vaciar(self):
        self.volumen = 0

    def agregar(self, cantidad:int=0):
        self.volumen += cantidad




def vaciar(
        persona:Persona, 
        botella:Botella, 
        cantidad:int=0):
    #copia = copy.deepcopy(botella)
    accion = Accion("vaciar", botella.volumen, type(botella).__name__, cantidad)
    botella.sacar(cantidad)
    persona.agregar_accion(accion)


if __name__=="__main__":
    botella = Botella(1000)
    persona = Persona("Juan", "Pérez")
    
    while (cantidad:=input("Cuando desea vaciar la botella?"))!="END":
        try:
            cantidad = int(cantidad)
            print("Antes", botella)
            vaciar(persona, botella, cantidad)
            print("Después", botella)
            print("La persona es", persona)
        except Exception as e:
            print(f"Valor debe ser un entero {e}")

