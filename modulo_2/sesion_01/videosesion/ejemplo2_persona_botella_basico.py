from rich import print
from datetime import datetime
import copy

persona = {
    "nombre":"Juan Pérez",
    "acciones":{}
}

botella = {
    "contenido":"agua",
    "volumen":1000
}


def vaciar(
        persona, 
        botella, 
        cantidad:int):
    assert cantidad>=0, "Cantidad debe ser positiva"
    ahora = datetime.now()
    copia = copy.deepcopy(botella)
    accion = ("vaciar", copia, cantidad)
    #accion = ("vaciar", botella, cantidad)
    volumen_actual = botella["volumen"]
    if cantidad<=volumen_actual:
        botella["volumen"] -= cantidad
    else:
        botella["volumen"] = 0
    persona["acciones"][ahora] = accion

if __name__=="__main__":
    while (cantidad:=input("Cuando desea vaciar la botella?"))!="END":
        try:
            cantidad = int(cantidad)
            print("Antes", botella)
            vaciar(persona, botella, cantidad)
            print("Después", botella)
            print("La persona es", persona)
        except Exception as e:
            print(f"Valor debe ser un entero {e}")
