import sys
from rich import print

persona = {
    "nombre":"Juan Pérez",
    "acciones":[]
}

botella = {
    "contenido":"agua",
    "volumen":1000
}


def vaciar(
        persona, 
        botella, 
        cantidad:int):
    assert cantidad>=0, "Cantidad debe ser positiva"
    accion = ("vaciar", botella, cantidad)
    botella["volumen"] -= cantidad
    persona["acciones"].append(accion)

if __name__=="__main__":
    cantidad = int(sys.argv[1])
    print("Antes", botella)
    vaciar(persona, botella, cantidad)
    print("Después", botella)
    print("La persona es", persona)

