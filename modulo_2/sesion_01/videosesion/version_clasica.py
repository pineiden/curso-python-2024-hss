from typing import Any
from datetime import datetime
from rich import print
import copy
from collections import namedtuple

Accion = namedtuple(
    "Acción", 
    ["nombre", "cantidad", "objeto", "entrada"])


class Persona:
    nombre:str
    apellido:str
    acciones:dict[datetime,Accion]
    

    def __init__(self, nombre:str, apellido:str):
        self.nombre = nombre
        self.apellido = apellido 
        self.acciones = {}

    def agregar_accion(self, accion:Accion):
        ahora = datetime.now()
        self.acciones[ahora] = accion


    def __repr__(self):
        return f"Persona({self.nombre}, {self.apellido}, {self.acciones})"

    def __str__(self):
        return f"Persona({self.nombre}, {self.apellido}, {self.acciones})"

class Botella:
    volumen: int 
    material: str = "vidrio"
    contenido: str = "agua"
    color: str = "transparente"

    def __init__(
            self,
            volumen,
            material="vidrio",
            contenido="agua",
            color="transparente"):
        self.volumen = volumen
        self.material = material
        self.contenido = contenido
        self.color = color

    def sacar(self, cantidad:int=0):
        assert cantidad>0,f"Cantidad debe ser >0, se ingresó {cantidad}"
        if cantidad<=self.volumen:
            self.volumen -= cantidad
        else:
            self.vaciar()

    def vaciar(self):
        self.volumen = 0

    def __repr__(self):
        return f"Botella({self.volumen})"

    def __str__(self):
        return f"Botella({self.volumen})"




def vaciar(
        persona:Persona, 
        botella:Botella, 
        cantidad:int=0):
    #copia = copy.deepcopy(botella)
    accion = Accion("vaciar", botella.volumen, type(botella).__name__, cantidad)
    #accion = ("vaciar", copia, cantidad)
    botella.sacar(cantidad)
    persona.agregar_accion(accion)


if __name__=="__main__":
    botella = Botella(1000)
    persona = Persona("Juan", "Pérez")
    
    while (cantidad:=input("Cuando desea vaciar la botella?"))!="END":
        try:
            cantidad = int(cantidad)
            print("Antes", botella)
            vaciar(persona, botella, cantidad)
            print("Después", botella)
            print("La persona es", persona)
        except Exception as e:
            print(f"Valor debe ser un entero {e}")

