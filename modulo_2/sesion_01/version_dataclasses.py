from dataclasses import dataclass, field, asdict
from datetime import datetime
from rich import print
import copy

@dataclass
class Persona:
    nombre:str
    apellido:str
    acciones:list[str] = field(default_factory=dict)

    def eliminar(self, objeto):
        del objeto

    def agregar_accion(self, accion):
        ahora = datetime.now()
        self.acciones[ahora] = accion


@dataclass
class Botella:
    volumen: int
    material: str = "vidrio"
    contenido: str = "agua"
    color: str = "transparente"

    def vaciar(self):
        self.volumen = 0

    def sacar(self, cantidad:int=0):
        if cantidad<=self.volumen:
            self.volumen -= cantidad
        else:
            self.vaciar()

    def agregar(self, cantidad:int=0):
        self.volumen += cantidad

from collections import namedtuple

Accion = namedtuple(
    "Accion", 
    ["nombre", "cantidad", "objeto", "entrada"])


def vaciar(
        persona:Persona, 
        botella:Botella, 
        cuanto:int=0):
    accion = Accion(
        "vaciar", 
        botella.volumen, 
        type(botella).__name__, 
        cuanto)
    botella.sacar(cuanto)
    persona.agregar_accion(accion)


if __name__=="__main__":
    botella = Botella(1000)
    persona = Persona("Juan","Perez")
    while (cuanto:=input("Cuanto descargar botella?"))!="END":
        cuanto = int(cuanto)
        print("Antes", botella)
        vaciar(persona, botella, cuanto)
        print("Despues", botella)
        print("Quien vacia", persona)
