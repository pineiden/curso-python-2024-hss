import sys
from datetime import datetime
from rich import print

persona = {
    "nombre":"Danilo",
    "acciones":{}
}


botella = {
    "volumen":1000
}

def vaciar(persona, botella, cuanto:int=0):
    ahora = datetime.now()
    accion = ("vaciar", botella, cuanto)
    volumen_actual = botella["volumen"]
    if cuanto<=volumen_actual:
        botella["volumen"] -= cuanto
    else:
        botella["volumen"] = 0
    persona["acciones"][ahora] = accion

if __name__=="__main__":
    while (cuanto:=input("Cuanto descargar botella?"))!="END":
        cuanto = int(cuanto)
        print("Antes", botella)
        vaciar(persona, botella, cuanto)
        print("Despues", botella)
        print("Quien vacia", persona)

