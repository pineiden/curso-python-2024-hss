% Created 2024-06-06 jue 00:47
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\author{David Pineda}
\date{\today}
\title{Objetos y clases en programación.}
\hypersetup{
 pdfauthor={David Pineda},
 pdftitle={Objetos y clases en programación.},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.1 (Org mode 9.7-pre)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents

Hasta el momento hemos aprendido a usar los diferentes elementos y
estructuras de datos que vienen con el lenguaje \emph{python}. 

Somos capaces de distinguir que usar cuando hacemos una operación
matemática, contar, resolver algún problema de ordenamiento, seguir
una serie de condiciones e iterar.

Pero hay mucho más por conocer. Así como para muchos problemas basta
con usar listas, diccionarios o los elementos básicos como números y
booleanos, también es posible crear nuestras propias estructuras de
datos que tengan un comportamiento específico a lo que requiere
nuestro problema para ser resuelto.

Si bien la versatilidad de los diccionarios y listas nos permiten
resolver la mayoría de los problemas en combinación con funciones, la
creación  de \textbf{objetos} nos permitirá lograr una mayor expresividad.
\section{Declarar un objeto definiendo una clase.}
\label{sec:orgb11e32e}

Así como hasta el momento hemos definido información asociada mediante
estructuras 

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
persona = {
     "nombre":"Juan",
     "apellido":"Espinoza",
     "fecha-nacimiento":"01-05-1996"
}
\end{minted}

También podemos hacerlo declarando primero una estructura de datos
llamada \textbf{objeto} mediante la siguiente síntaxis.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
class Persona:
    nombre: str 
    apellido: str 
    fecha_nacimiento: datetime
\end{minted}

De manera similar a los diccionarios, el objeto \textbf{Persona} permite
crear \textbf{instancas} de \textbf{Persona}. Para lograr esto podemos seguir dos
caminos posibles (de momento)

Forma clásica, agregando método (función asociada) \textbf{\uline{\uline{init}}}

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
class Persona:
    nombre: str 
    apellido: str 
    fecha_nacimiento: datetime

    def __init__(self, nombre, apellido, fecha_nacimiento):
        self.nombre = nombre 
        self.apellido = apellido 
        self.fecha_nacimiento = fecha_nacimiento
\end{minted}

O bien, usando \textbf{dataclass} que nos permitirá ahorrar la declaración
del método, decorando con \textbf{@dataclass}.

\url{https://docs.python.org/3/library/dataclasses.html}

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
from dataclases import dataclass
from datetime import datetime 

@dataclass
class Persona:
    nombre: str 
    apellido: str 
    fecha_nacimiento: datetime
\end{minted}

Podemos verificar que sea una clase con  \textbf{inspect.isclass}. 
\section{Representación UML}
\label{sec:org670fe7e}

Previamente a crear el código es muy recomendable crear diagramas que
permitan tener claridad sobre los componentes y métodos asociados.

Usando la herramienta \textbf{plantuml}

\url{https://plantuml.com/es/class-diagram}


\begin{center}
\includegraphics[width=.9\linewidth]{umlpersona.png}
\end{center}
\section{Decoradores}
\label{sec:orgb93d08e}

Los \textbf{decoradores} son herramientas disponibles en el lenguaje,
permiten agregar atributos o propiedades extras al objeto que decoran,
consisten en funciones que encapsulan la función objetivo y se pueden
definir según se necesite.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
def mi_decorador(funcion, a,b,*args, **kwargs):
    def nueva_funcion(a,b,*args, **kwargs):
        print("==============")
        c = function(*args,**kwargs)
        print("==============")
        return c 
    return nueva_funcion
\end{minted}


Cómo se ve en el código, la función a decorar se entrega como
argumento directamente a la función decoradora, en este caso cuando la
función decorada se utiliza se imprime antes una serie de \textbf{==} y luego
de ser ejecutada otra serie de \textbf{==}. Es decir, antes o después de
accionar la función decorada será posible realizar algo más asociado
mediante al decoración.

Ahora, para utilizar el \textbf{decorador} es simple ya que basta con agregar
antes de la definición de la función \textbf{@mi\textsubscript{decorador}}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
@mi_decorador 
def funcion_decorada(*args):
    for i, elemento in args:
        print(i, elemento)
\end{minted}

De la misma manera, cuando decoramos una \textbf{clase} con \textbf{dataclass}
agregamos a la clase una serie de métodos de utilidad por defecto.

Más referencias en:

\begin{itemize}
\item \url{https://ellibrodepython.com/decoradores-python}
\item \url{https://codigofacilito.com/articulos/decoradores-python}
\end{itemize}
\section{Lectura de csv con DictReader}
\label{sec:org2db7cc8}

Para la lectura de datos ordenados en archivos \textbf{csv} hemos utilizado
el \emph{reader} simple. En esta ocasión usaremos el objeto \textbf{DictReader}
que nos entregará un diccionario por cada fila con datos.

Descargamos el archivo csv:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{bash}
wget https://gist.githubusercontent.com/brenes/1095110/raw/c8f208b03485ba28f97c500ab7271e8bce43b9c6/paises.csv
\end{minted}

Verificamos que esté descargado:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{bash}
ls *.csv
\end{minted}

Ahora lo leemos.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
import csv
from pathlib import Path

path = Path("paises.csv")

with path.open() as f:
    reader = csv.DictReader(f)
    for i, item in enumerate(reader):
        if i<=5:
            print(item)
\end{minted}

Una de las tareas importantes al hacer lectura de un archivo \textbf{csv} es
validar cada campo según un formato requerido y al validarlo también
transformarlo en el tipo de dato requerido.
\section{Escritura de csv con DictWriter}
\label{sec:org423de77}

Ahora vamos de vuelta. 

Generaremos una serie de valores de un tríangulo de manera aleatoria y
añadiremos la marca de tiempo.


\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
from datetime import datetime
import random
from typing import Dict, List, Any


lado = lambda: random.uniform(0.1, 100)

names = ("a","b","c","d","e")

def triangulo(*args, **kwargs)->Dict[str, float]:
	a = lado()
	b = lado()
	c = random.uniform(abs(a-b), a+b)
	lados = (a,b,c)
	return dict(zip(names, lados))


def genera_triagulos(n:int)->Dict[str,Any]:
    for i in range(n):
        trg = triangulo()
        trg.update({"id":i, "timestamp": datetime.utcnow()})
        yield trg

for item in genera_triangulo(10):
    print(item)
\end{minted}

Para usarlo en una función que guarde a un archivo csv podría ser de
la siguiente manera, definiendo primero el nombre del archivo y
abríendolo para escritura.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
from pathlib import Path 
import csv 

path = Path("triangulos.csv")
fieldnames = ["id", "timestamp", "a","b","c"]
with path.open("w") as f:
    writer = csv.DictWriter(f, delimiter=";", fieldnames=fieldnames)
    writer.writeheader()
    for item in genera_triangulos(20):
        writer.writerow(item)
\end{minted}

Veremos que el archivo se crea y contiene la información de los
triángulos como esperábamos.
\subsection{Convertir CSV a JSON}
\label{sec:org4b6dbb2}

A veces será conveniente transformar un archivo \textbf{csv} a un \textbf{json}. Que es un
formato que lo pueden leer fácilmente los lenguajes modernos. Considerando
siempre que el tamaño del \textbf{csv} no sea grande (gestión de la memoria).

Leeremos un archivo en formato \textbf{csv} y lo transformaremos a un \textbf{json}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
import csv
import json

ruta = "mes_temp.csv"
ruta_json = "mes_temp.json"

lista = []

with open(ruta,'r') as archivo:
    reader = csv.DictReader(archivo)
    for elem in reader:
        lista.append(elem)
        
print(lista)
\end{minted}

Ahora, transformamos la lista de diccionarios en un json string.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
with open(ruta_json, 'w', encoding='utf8') as archivo_json:
    # transformamos a un objeto json_data antes de guardar
    # nos asegura la codificacion
    json_data = json.dumps(lista, indent=2, ensure_ascii = False)
    # vemos el tipo de dato antes de guardar
    # es un string
    print(type(json_data))
    archivo_json.write(json_data)
\end{minted}
\section{Serialización de datos}
\label{sec:org10af18d}

\subsection{Una esquema para mejor manejo de datos}
\label{sec:orgb288387}

Para tener claro todo, nos servirá este esquema. Nos habla de cómo será
necesario transformar un dato desde su fuente hasta que sea un elemento
utilizable.

\begin{center}
\includegraphics[width=.9\linewidth]{./img/dato_util.png}
\end{center}
\subsection{Explicación de cada paso}
\label{sec:orga80481f}

\begin{description}
\item[{validación}] consiste en verificar que un valor puede ser transformado a un
tipo de dato adecuado. Revisar si un dato en campo de \textbf{email} es
un correo electrónico.

\item[{transformación}] en caso de que sea un dato válido, se transforma al tipo de
dato adecuado.

\item[{agrupación}] se agrupa con otros datos ya validados para construir un objeto
de utilidad para el trabajo requerido.
\end{description}
\subsection{Serialización}
\label{sec:orgeca25e4}

La serialización es la acción que permite conservar un objeto como una cadena de
bytes. No todos los objetos son serializables, por lo que hay que tener cuidado.

\begin{center}
\includegraphics[width=.9\linewidth]{./img/trx_dato.png}
\end{center}
\subsection{Transformación de bytes a string y viceversa}
\label{sec:orgf554e76}

El proceso de transformar información desde una cadena de \textbf{bytes} a un \textbf{objeto}
que sea posible manipular y viceversa se llama \textbf{serialización} (byte a objeto) y
\textbf{deserialización} (objeto a byte).

Lo que se de-serializa desde Python es la representación del objeto como bytes,
proceso que permite reconstruirlo en memoria principal. Para datos básicos la
representación será el objeto mismo. 

Internamente, cuando se lee un archivo cada \textbf{byte} se de-serializa en una cadena de
caracteres. 

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
def texto_codificar(texto):
    txt_bytes=texto.encode('utf-8')    
    print("Texto codificado", txt_bytes)
    print(type(txt_bytes))
    decodificado=txt_bytes.decode('utf-8')
    print("Texto de-codificado", decodificado)    
    # ver los valores decimales de cada símbolo
    [print(char) for char in txt_bytes]
    # error!
    try:
        [print(ord(char)) for char in txt_bytes]
    except Exception as e:
        print("Hubo error %s" %e)
    # ver los valores binarios de cada símbolo
    # lo que ve el computador en memoria
    [print(bin(char)) for char in txt_bytes]
\end{minted}

Ahora, probamos la función con dos textos. Uno que contenga todos sus símbolos
en el grupo \textbf{ASCII} y otro que tiene símbolos extra.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
texto="Tres tristes tigres"
texto_codificar(texto)
texto2="Este cabrón me quitó el terreno"
texto_codificar(texto2)
\end{minted}
\section{Ejercicio.}
\label{sec:orgcf2e30d}

Resolver Sudoku.

Cambiar este código usando clases y métodos.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos=]{python}
sudoku_txt="""
530070000\n
600195000\n
098000060\n
800060003\n
400803001\n
700020006\n
060000280\n
000419005\n
000080079
"""

from pprint import pprint
from typing import Optional, Tuple, List, Dict
from functools import reduce
from copy import deepcopy


Sudoku=List[List[str]]
Size=Tuple[Optional[int], Optional[int]]
Position=Tuple[Optional[int], Optional[int]]

sudoku:Sudoku = [list(line) for line in sudoku_txt.split("\n") if line]

pprint("====Sudoku====")
pprint(sudoku)


def size(puzzle:Sudoku)->Size:
    if puzzle:
        n=len(puzzle)
        if puzzle[0]:
            m=len(puzzle[0])
            return n,m
    return None, None

print(size(sudoku))

def find_zero(board:Sudoku)-> Position:
    for i,row in enumerate(board):
        for j, value in enumerate(row):
            if value == "0":
                return (i,j)
    return (None, None)

def position_range(i:int)->Position:
    if 0 <= i <= 2:
        return [0,3]
    elif 3 <= i <= 5:
        return [3,6]
    elif 6 <= i <= 8:
        return [6,9]

def sector(board:Sudoku, position:Position) -> Sudoku:
    slice_row = slice(*position_range(position[0]))
    slice_column = slice(*position_range(position[1]))
    row_selection = board[slice_row]
    selection = [row[slice_column] for row in row_selection]
    return selection

def get_sectors(board:Sudoku, position:Position)->Dict[str,object]:
    i = position[0]
    j = position[1]
    row = board[i]
    column = [row[j] for row in board]
    sector_ = sector(board, position)
    return {"row":row, "column":column, "selection":sector_}


def conjuntos(sectors):
    row_set = set(sectors.get('row',{}))
    row_set.discard("0")
    column_set = set(sectors.get('column',{}))
    column_set.discard("0")
    sector_set = set(reduce(lambda a,b:a+b, sectors.get("selection",[])))
    sector_set.discard("0")
    total = {str(n) for n in range(1,10)}
    posibles = total - (sector_set | column_set | row_set)
    return {"row":row_set, "column":column_set, "sector":sector_set, "posibles":posibles}

def is_valid(value:int, numeros: set)->bool:
    return str(value) in numeros

def set_value(board:Sudoku, position:Position, value:int):
    board[position[0]][position[1]]=str(value)


def solve(puzzle:Sudoku)->Sudoku:
    new_puzzle = deepcopy(puzzle)
    i,j = find_zero(new_puzzle)
    if i is None and j is None:
        return new_puzzle
    else:
        sectors = get_sectors(new_puzzle, [i,j])
        numeros = sorted([int(e) for e in
    conjuntos(sectors).get('posibles',set())])
        for n in numeros:
            set_value(new_puzzle, [i,j], n)
            solution = solve(new_puzzle)
            if solution:
                return solution
        else:
            return []

solucion:Sudoku = solve(sudoku)
pprint("====Solución Sudoku=====")
pprint(solucion)

\end{minted}
\end{document}
