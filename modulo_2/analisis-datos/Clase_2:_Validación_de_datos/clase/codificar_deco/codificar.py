import csv
from tablas import (temporadas, mascotas, 
                    binario_mascotas, bitstring_to_bytes)


def print_dict(diccionario:dict):
    for key, value in diccionario.items():
        print(key, ":", value)
        
def obtener_preferencias():
    nombre = input("Nombre: ")
    print("Selecciona temporada")
    print_dict(temporadas)
    temporada = int(input("Temporada "))    
    print("Selecciona mascotas (numeros, separa por ,)")
    print_dict(mascotas)
    mascotas_sel = input("Mascotas ")
    lista_mascotas = set(map(int, mascotas_sel.split(",")))
    mascotas_bin = binario_mascotas(lista_mascotas)
    seguir = input("Seguir? (no: parar)")
    return nombre, temporada, (lista_mascotas, mascotas_bin), seguir
    
def guardar_data(nombre, temporada, sel_mascotas, archivo, writer):
    lista_mascotas, mascotas_bin = sel_mascotas
    mascotas_lista = [mascotas.get(key) for key in lista_mascotas]    
    dato = [nombre, temporadas.get(temporada), ":".join(mascotas_lista)]
    dato_bytes = [nombre.encode(), temporada.to_bytes(2,'little'),  bitstring_to_bytes(mascotas_bin), "\n".encode()]        
    datos_binario =  b','.join(dato_bytes)
    # al csv
    writer.writerow(dato)
    # al binario
    archivo.write(datos_binario)

archivo_ruta = "preferencias.bin"
archivo_csv_ruta = "preferencias.csv"

with open(archivo_ruta,'wb') as archivo, open(archivo_csv_ruta, "w") as archivo_csv:
    writer = csv.writer(archivo_csv, delimiter=',')
    nombre, temporada, sel_mascotas, seguir = obtener_preferencias()
    guardar_data(nombre, temporada, sel_mascotas, archivo, writer)
    while seguir!="no":
        nombre, temporada, sel_mascotas, seguir = obtener_preferencias()
        guardar_data(nombre, temporada, sel_mascotas, archivo, writer)
