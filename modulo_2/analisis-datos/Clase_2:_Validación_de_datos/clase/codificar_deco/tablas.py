temporadas = {
    0:"Inviernista",
    1:"Veranista",
    2:"Primaverista",
    3:"Otoñista"
}

mascotas = {
    0:"Catlover",  
    1:"Doglover",     
    2:"Ovejalover",   
    3:"Serpientelover", 
    4:"Gallinalover", 
    5:"Equinolover"  
}

def genera_binario(diccionario, seleccion):
    valor = ''
    for key in sorted(diccionario.keys()):
        if key in seleccion:
            valor += "1"
        else:
            valor += "0"
    return valor
            
def binario_mascotas(seleccion):
    return genera_binario(mascotas, seleccion)
    
import math

def size_byte(val):
    largo = len(val)
    n=1
    while largo>=2**n:
        n+=1
    return int(2**n/8)
    
    
def bitstring_to_bytes(s):
    size = size_byte(s)
    return int(s, 2).to_bytes(size, byteorder='little')


def bytes_to_bitstring(val):
    return int.from_bytes(val, byteorder='little')


def decode_mascotas(mascotas_bin):
    return "{0:6b}".format(bytes_to_bitstring(mascotas_bin)).replace(" ","0")
    
def expresa_mascotas(bin_code):
    resultado = []
    for pos, val in enumerate(bin_code):
        if val=="1":
            resultado.append(mascotas.get(pos))
    return resultado
