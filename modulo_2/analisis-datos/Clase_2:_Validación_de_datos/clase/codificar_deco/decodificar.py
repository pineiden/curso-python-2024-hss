import csv
from tablas import (temporadas, mascotas, 
                    binario_mascotas, bitstring_to_bytes, 
                    bytes_to_bitstring, decode_mascotas, expresa_mascotas)


archivo_ruta = "preferencias.bin"
archivo_csv_ruta = "preferencias.csv"

with open(archivo_ruta,'rb') as archivo, open(archivo_csv_ruta, "r") as archivo_csv:
    print("Lectura del binario")
    for line in archivo.readlines():
        elements_bin = line.split(b',')
        print("Binario")
        print(elements_bin)
        elements = [elements_bin[0].decode('utf8'), 
            temporadas.get(bytes_to_bitstring(elements_bin[1])),
            expresa_mascotas(decode_mascotas(elements_bin[2])), 
            elements_bin[3].decode("utf8")]
        print("Decoded ...")
        print(elements[:-1])
    print("Lectura csv")
    reader =  csv.reader(archivo_csv, delimiter=',')
    for line in reader:
        print(line)
    
