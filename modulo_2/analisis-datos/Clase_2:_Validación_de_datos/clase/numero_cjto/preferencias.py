import csv

def valida_binario(valor):
    conjunto = {'0', '1'}
    lista  = list(map(lambda e: e in conjunto, valor))
    return all(lista) and len(valor)==3

def temporada(preferencia):
    mapa = {"0":"Inviernista", "1":"Veranista"}
    return mapa.get(preferencia, False)
    
def mascota(preferencia):
    mapa = {"0":False,  "1":True}
    return mapa.get(preferencia, False)
    
def mapear_preferencias(preferencias):
    return dict(
        temporada=temporada(preferencias[0]),
        catlover = mascota(preferencias[1]),
        doglover = mascota(preferencias[2]))

archivo_ruta = "preferencias.csv"

preferencias = []
with open(archivo_ruta,'r') as archivo:
    reader =  csv.DictReader(archivo, delimiter=',')     
    for elem in reader:
        if valida_binario(elem.get('Preferencia',"a")):
            elem["mapa"] = mapear_preferencias(elem.get("Preferencia", "000"))
            preferencias.append(elem)


print(preferencias)

    
    
    
