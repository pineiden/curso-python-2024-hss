def valida_edad(valor, inicial=0, final=150):
    if inicial <= valor < final:
        return True
    else:
        return False
        
print(valida_edad(25, inicial=12))

rangos_etareos = {
"menor": (0,18),
"adulto": (18,55),
"tercera_edad" :(55, 150)
}

import json

archivo_ruta = "perfiles.json"
perfiles = {}
with open(archivo_ruta,'r') as archivo:
    perfiles = json.load(archivo)
    print(perfiles)
    for persona in perfiles:
        edad = persona.get('edad',-1)
        persona["existe"] = False
        persona["rango_etareo"] = "ninguno"
        for rango_etareo, tupla in rangos_etareos.items():
            kwargs = dict(zip(("inicial","final"), tupla))
            validar = valida_edad(edad, **kwargs)
            if validar:
                persona["existe"] = True
                persona["rango_etareo"] = rango_etareo
                
            
           
print(perfiles)


# reporte

from itertools import groupby

groups = []
uniquekeys = []
data = sorted(perfiles, key= lambda persona: persona.get('edad',-1))

for k, g in groupby(data, lambda e: e.get('rango_etareo')):
    groups.append(list(g))
    uniquekeys.append(k)
    
print("Mostrar por rango etareo")

for i, k in enumerate(uniquekeys):
    print("Para rango etareo %s" %k)
    for elem in groups[i]:
        print(elem.get('nombre'), elem.get('edad'))
        
