import re

texto = "padres.txt"
with open(texto, 'r') as archivo:
    padres = archivo.read()
    regex = re.compile("[mp]a[mp]á")
    for match in regex.finditer(padres):
        print(match)


with open(texto, 'r') as archivo:
    padres = archivo.read()
    regex = re.compile("[mp]a[mp]á")
    resultado = regex.sub("familiar", texto)
    resultado_n = regex.subn("familiar", texto)
    print("caso 1", resultado)
    print("caso 2", resultado_n)
