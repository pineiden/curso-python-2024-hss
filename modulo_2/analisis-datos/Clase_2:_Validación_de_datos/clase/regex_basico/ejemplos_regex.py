"""
Validar binario

si la cadena es solo de 010101

^[01]+$ : de princio (^) valores 0,1 hasta el fin, al menos un caracter ($) 
"""
import re

def es_binario(cadena):
    regex =  re.compile("^[01]+$")
    if regex.match(cadena):
        return True
    else:
        return False

def es_numero(cadena):
    """
    Las formas sencillas:
    232323
    232332.5656
    "^[0-9]*\.?[0-9]+$" :: de inicio (^). Pueden haber de nada a varios numeros (*), 
        además un "." opcional, luego uno o mas números (+)
        hasta el final
    """
    regex = re.compile("^[0-9]*\.?[0-9]+$")
    if regex.match(cadena):
        return True
    else:
        return False
    
