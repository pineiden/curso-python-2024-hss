(TeX-add-style-hook
 "02_clase"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("babel" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted"
    "babel"
    "fancyhdr")
   (LaTeX-add-labels
    "sec:org5e32008"
    "sec:org7d83b79"
    "sec:org7aa5adb"
    "sec:org0439f56"
    "sec:org2ea9ade"
    "sec:orgc9fa707"
    "sec:org953199e"
    "sec:orgb0cc5ff"
    "sec:org3dd66fa"
    "sec:org3c3428b"
    "sec:orgf470360"
    "sec:org0488852"
    "sec:org91dd538"
    "sec:orgd7224be"
    "sec:orgbea0b96"
    "sec:org3c58487"
    "sec:org35d4737"
    "sec:org5410fa8"
    "sec:org281278a"
    "sec:org9b2d760"
    "sec:orgbdbb69e"
    "sec:org3d35397"
    "sec:org15cd579"
    "sec:orgaf5aa17"
    "sec:org60150b0"
    "sec:orgbfef7a4"
    "sec:orgfc1739b"
    "sec:org9d802e3"
    "sec:org0b7e336"
    "sec:org5fee646"
    "sec:orgca505be"
    "sec:org7c83317"
    "sec:org3c632c1"
    "sec:orgd97994a"
    "sec:orgc9dbb9f"
    "sec:orgb1a4e84"
    "sec:org2d505fc"
    "sec:org3250b3c"
    "sec:org8b23ecd"
    "sec:org884f578"
    "sec:org95d18da"
    "sec:org691738d"
    "sec:orga2654a2"
    "sec:orgdf750a5"
    "sec:orgee49a05"
    "sec:org0a2794b"
    "sec:org1d800a2"
    "sec:org4f154cc"
    "sec:org0f338fb"))
 :latex)

