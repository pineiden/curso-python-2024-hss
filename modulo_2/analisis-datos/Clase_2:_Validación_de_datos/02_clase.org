
#+REVEAL_ROOT: ../reveal.js
#+TITLE: Validación de Datos
#+Author: David Pineda Osorio
#+Email: davidpineda@quux.cl
#+DATE: 8 de marzo 2020
#+REVEAL_THEME: league
#+REVEAL_TRANS: linear    
#+REVEAL_INIT_OPTIONS: slideNumber:true,width:1366,height:768, center:false
#+OPTIONS: toc:nil reveal_mathjax:t
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \pagestyle{fancyplain}
#+LATEX_HEADER: \lhead{}
#+LATEX_HEADER: \rhead{Quux Ltda, www.quux.cl, contacto@quux.cl}
#+LATEX_HEADER: \cfoot{Módulo 2, Clase 02: Validación de datos }
#+LANGUAGE: es
#+OPTIONS: H:2 toc:1 num:f author:t date:f email:t
#+OPTIONS: ^:nil
#+REVEAL_EXTRA_CSS: ../css/estilos.css

* Objetivo de la clase

Esta clase tiene como objetivo aprender técnicas y metodologías para que la
información obtenida de diferentes fuentes sea correctamente procesada.

Para distintos tipos de dato a recuperar desde una fuente, en cada caso
seleccionaremos específicamente un trozo de información a identificar como un
dato de ese tipo. Se valida que la cadena (string, bytes) pueda convertirse al tipo de dato, y
se aplica la conversión respectiva.

* Esquema general de validación

De manera general, será necesario tener en cuenta lo siguiente cuando tengamos
diferentes fuentes de información.

#+attr_latex: :width 300px
#+attr_html: :width 700px
[[file:./img/esquema_validacion.png]]

* Validar un número

** Tabla de pruebas

Lo primero, debemos conocer las posibles presentaciones de un número, razonar en
torno a los patrones que observemos. Acotar, por ejemplo, a solo números de
base decimal (no binario, hexadecimal).

¿Visualmente, estos valores son números?

#+REVEAL: split

|          Valor | Tipo      | número? |
|----------------+-----------+---------|
|             -5 | int       | True    |
|         10.343 | float     | True    |
|  156.34.123.34 | ip        | False   |
| 10,343,365.346 | float     | True    |
|     -23,456.45 | float     | True    |
|     10_545_342 | int       | True    |
|       -100_657 | int       | True    |
|         "102e10" | int/float | True    |
|         "\--10e-4" | int       | True    |
|     AFD4341AFC | str       | False   |

** Características observadas

Listamos ahora las características que observamos, investigamos también otras
posibles formas de que aparezcan números. Cómo la notación científica (45x10^6),
que no estaba en la tabla.

- Tiene dígitos {0,1,2,3,4,5,6,7,8,9}
- Puede ser negativo, con *-* al principio, a la izquierda
- Puede tener un separador decimal *,*
- Puede tener un separador de cifras {",","_"}
- Notación científica con *QeW=Qx10^W*

** Una solución ya conocida

Verifica que un valor ingresado sea número. No considera todos los casos
necesarios.

#+BEGIN_SRC python           
def es_numero(valor):
    a_evaluar = valor.lstrip("-").replace(".","",1)
    if a_evaluar.isdigit():
        return True, float(valor)
    else:
        return False, valor
#+END_SRC

** Complementar la solución. 

Necesitamos, además, que considere el separador de cifras, y los números con
notación científica.

La notación científica considera la forma en informática *eW* (e de exponenente
y W de número entero) o bien la notación matemática *x10^W*-

Cómo texto son dos casos distintos pero característicos. Vemos dos partes. Si
existe una *e* o *x10*, como cadena de caracteres, tenemos un texto separado en

#+BEGIN_EXAMPLE
15e98 -> [15 98]
-45x10^87 -> [-45 87]
abce405 -> [abc 405]
#+END_EXAMPLE

#+REVEAL: split

Consideramos que cada parte podría ser un número y debe ser verificada. Podemos
utilizar una *recursión*  de la misma función.  Y, en caso de, si la separación
tiene dos elementos, proceder a verificar cada parte.


#+BEGIN_src python
def es_numero(valor):
    valor = valor.replace("x10^", "e")
    not_cientifica = "e"
    separacion = valor.split(not_cientifica)
    if len(separacion)==2:
        par_numerico = separacion
        base = es_numero(par_numerico[0])
        exp = es_numero(par_numerico[0])
        if base and exp:
            return True, float(valor)
        else:
            return False, valor
    else:
        a_evaluar = valor.lstrip("-").replace(".","",1)
        if a_evaluar.isdigit():
            return True, float(valor)
        else:
            return False, valor
#+END_SRC

* Validar un número en torno a un tramo

** La edad como un número con un rango de valores válidos.

Se tiene un archivo *perfiles.json*. Verificar que la edad sea un valor válido.

Será considerar que la edad es un valor de *0* a *150*, según registros.

Una función que valide edad sería. Además, para complementar, podemos
parametrizar los valores *inicial* y *final* para que la función sea de utilidad
para cualquier tipo de validación de edad. Por ejemplo, si necesitamos solamente
a los adultos, ¿cómo sería?

#+BEGIN_SRC python
def valida_edad(valor, inicial=0, final=150):
    if inicial <= valor < final:
        return True
    else:
        return False
#+END_SRC

** Aplicación. Tabla de rangos etáreos

- ¿Quienes son niños?
- ¿Quienes son adultos sobre los 18 años?
- ¿Quienes son tercera edad?

|--------------+--------+-------|
| Rango        | Inicio | Final |
|--------------+--------+-------|
| Menores      |      0 |    18 |
| Adultos      |     18 |    55 |
| Tercera Edad |     55 |   150 |
|--------------+--------+-------|

** La tabla a un diccionario

#+BEGIN_SRC python
rangos_etareos = {
"menores": (0,18),
"adultos": (18,55),
"tercera_edad" :(55, 150)
}
#+END_SRC

** Lectura del archivo

Se lee el archivo y se valida cada persona según su edad.

#+BEGIN_SRC python
import json

archivo_ruta = "perfiles.json"
perfiles = {}
with open(archivo_ruta,'r') as archivo:
    perfiles = json.load(archivo)
    print(perfiles)
    for persona in perfiles:
        edad = persona.get('edad',-1)
        persona["existe"] = False
        persona["rango_etareo"] = "ninguno"
        for rango_etareo, tupla in rangos_etareos.items():
            kwargs = dict(zip(("inicial","final"), tupla))
            validar = valida_edad(edad, **kwargs)
            if validar:
                persona["existe"] = True
                persona["rango_etareo"] = rango_etareo

#+END_SRC

** Reporte final

Agrupamos según *rango_etareo* y mostramos los resultados.

#+BEGIN_SRC python
from itertools import groupby

groups = []
uniquekeys = []
data = sorted(perfiles, key= lambda persona: persona.get('edad',-1))

for k, g in groupby(data, lambda e: e.get('rango_etareo')):
    groups.append(list(g))
    uniquekeys.append(k)
    
print("Mostrar por rango etareo")

for i, k in enumerate(uniquekeys):
    print("Para rango etareo %s" %k)
    for elem in groups[i]:
        print(elem.get('nombre'), elem.get('edad'))
#+END_SRC

* Validar pertenencia a un conjunto

** Invernistas, veranistas, catlovers, doglovers

Se tiene una lista de personas con sus preferencias sobre temporada y mascotas.
Respecto a la temporada puede ser una o la otra exclusivamente, pero respecto a
mascotas se puede ser ambas.

Además, para ahorrar espacio en memoria, se almacenan los valores como valores
binarios. De manera que usted tendrá que validar y transformar el valor.

** Tabla de ejemplo

Por ejemplo, las siguientes personas tienen las preferencias que se muestran a continuación.

|----------+-----------+----------+----------+-------|
| Nombre   | Veranista | Catlover | Doglover | Valor |
|----------+-----------+----------+----------+-------|
| David    |         1 |        0 |        1 |   101 |
| Josefina |         0 |        1 |        0 |   010 |
| Catalina |         1 |        1 |        1 |   111 |
| Pedro    |         1 |        0 |        0 |   100 |
| Daniela  |         0 |        1 |        1 |   011 |
|----------+-----------+----------+----------+-------|

** Validar el valor binario

Verificamos que el valor de la preferencia sea binario y de tres caracteres.

#+BEGIN_SRC python
def valida_binario(valor):
    conjunto = {'0', '1'}
    lista  = list(map(lambda e: e in conjunto, valor))
    return all(lista) and len(valor)==3
#+END_SRC

** Transformar el valor a un objeto

Hacemos la transformación del valor binario en un diccionario.

#+BEGIN_SRC python
def temporada(preferencia):
    mapa = {"0":"Inviernista", "1":"Veranista"}
    return mapa.get(preferencia, False)
    
def mascota(preferencia):
    mapa = {"0":False, "1":True}
    return mapa.get(preferencia, False)
    
def mapear_preferencias(preferencias):
    return dict(
        temporada=temporada(preferencias[0]),
        catlover = mascota(preferencias[1]),
        doglover = mascota(preferencias[2]))
#+END_SRC

** Cargamos una lista con los elementos validados.

Solamente cargamos a la lista aquellos elementos válidos.

#+BEGIN_SRC python
import csv
archivo_ruta = "preferencias.csv"

preferencias = []
with open(archivo_ruta,'r') as archivo:
    reader =  csv.DictReader(archivo, delimiter=',')     
    for elem in reader:
        if valida_binario(elem.get('Preferencia',"a")):
            elem["mapa"] = mapear_preferencias(elem.get("Preferencia", "000"))
            preferencias.append(elem)
#+END_SRC

¿Cómo sería generar un reporte de por temporada, separando los catlover de los
doglover en cada caso?

* Codificar y decodificar

** El tamaño de la información

Puede ocurrir que el tamaño de un archivo sea muy grande. Exista información de
un csv que se repita constantemente en algunos campos. O necesitemos comunicar
dos puntos teniendo cuidado de la capacidad de transmisión.

Para eso podemos establecer un *Protocolo* o *Tablas de conversión*, en que esta
información, en vez de ocupar *n bytes* (si es una palabra de texto), ocuparía
menos: *1 byte*, por ejemplo.

Debemos considerar la cantidad de opciones que nos provee *n bits*, la relación
es sencilla:

\begin{equation}
Opciones posibles = 2^n
\end{equation}

** Cantidad de opciones por n bits.

Cantidad de elementos diferentes en una tabla de n bits.

|----+------+-------+----------------------|
|  n |  2^n | bytes |                total |
|----+------+-------+----------------------|
|  1 |  2^1 |       |                    2 |
|  2 |  2^2 |       |                    4 |
|  4 |  2^4 |       |                   16 |
|  8 |  2^8 |     1 |                  256 |
| 16 | 2^16 |     2 |                65536 |
| 32 | 2^32 |     4 |           4294967296 |
| 64 | 2^64 |     8 | 18446744073709551616 |
|----+------+-------+----------------------|


** ¿Qué concluimos?

Según la cantidad de elementos diferentes, para *codificar* la información
deberemos establecer una cantidad de *n bits* suficiente, de tal manera con
*2^n* se superior a la cantidad.

| Estación del año | Decimal | Conteo |
|------------------+---------+--------|
| Inviernista      |    0    |     00 |
| Veranista        |    1    |     01 |
| Primaverista     |    2    |     10 |
| Otoñista         |    3    |     11 |
|------------------+---------+--------|

Entonces, si alguien es inviernista, le correspondería el código *00*. Si
alguien tiene preferencia por la primavera, le corresponde el código *10*.

** ¿Para que nos sirve?

Con un ejemplo, si hacemos una *deliciosa torta*, pero nos costaría mucho dinero
enviarla a *Punta Arenas*. Podemos *codificar la torta* en una *receta*. De tal
manera que en *Punta Arenas*, la persona que la *decodifique* pueda
reconstruirla y cocinar otra *deliciosa torta*.

** Nuestra lista de Personas y sus preferencias.

Necesitamos completar y mejorar la lista de personas con sus preferencias de
temporada y mascotas. Para eso crearemos un *archivo binario* y almacenaremos la
información en *bytes*.

Notar que el string *101* tiene *3 bytes*, y el valor binario *101* tiene *3
bits* y ocupará *1 bytes*. Imagina cuanto podrías ahorrar en un archivo de 10GB
pasándolo de texto a binario.

Comparemos.

#+BEGIN_SRC python
valor=2
# binario de 2 decimal
print(valor.to_bytes(2,"little"))
binario=bin(2)
# binario de string '010' = 2 decimal
print(binario.encode())
#+END_SRC

** Un valor binario a entero

Se debe considera una cadena de 0 y 1, además de la base 2. O un valor entero a bytes.

#+BEGIN_SRC python
valor="010"
int(valor,2) 
valor="0101101"
int(valor,2) 
numero = 20
numero.to_bytes(2,"little")
numero_2 = 10001
numero_2.to_bytes(2,"big")
#+END_SRC

** Tabla de mascotas

Algunos tipos de amantes de mascotas. También se pueden elegir varios.

|----------------+---------+---------|
| Nombre         | Decimal | Boolean |
|----------------+---------+---------|
| Catlover       |       0 | 0/1     |
| Doglover       |       1 | 0/1     |
| Ovejalover     |       2 | 0/1     |
| Serpientelover |       3 | 0/1     |
| Gallinalover   |       4 | 0/1     |
| Equinolover    |       5 | 0/1     |
|----------------+---------+---------|

Construimos un código de *6 bits* para identificar las preferencias de mascotas.

** Usando lo que tenemos.

Creamos los diccionarios y las conversiones.


#+BEGIN_SRC python
temporadas = {
    0:"Inviernista",
    1:"Veranista",
    2:"Primaverista",
    3:"Otoñista"
}

mascotas = {
    0:"Catlover",  
    1:"Doglover",     
    2:"Ovejalover",   
    3:"Serpientelover", 
    4:"Gallinalover", 
    5:"Equinolover"  
}
#+END_SRC

** Funciones de conversión

Generar el código binario para selección de mascotas.

#+BEGIN_SRC python
def genera_binario(diccionario, seleccion):
    valor = ''
    for key in sorted(diccionario.keys()):
        if key in seleccion:
            valor += "1"
        else:
            valor += "0"
    return valor
            
def binario_mascotas(seleccion):
    return genera_binario(mascotas, seleccion)
#+END_SRC

** Convertir un string de bits a binario

#+BEGIN_SRC python
import math

def size_byte(val):
    largo = len(val)
    n=1
    while largo>=2**n:
        n+=1
    return math.ceil(int(2**n/8))

def bitstring_to_bytes(s):
    size = size_byte(s)
    return int(s, 2).to_bytes(size, byteorder='little')
#+END_SRC

** Funciones para simplificar el script de codificación

#+BEGIN_SRC python
import csv
from tablas import (temporadas, mascotas, 
                    binario_mascotas, bitstring_to_bytes)


def print_dict(diccionario:dict):
    for key, value in diccionario.items():
        print(key, ":", value)
        
def obtener_preferencias():
    nombre = input("Nombre: ")
    print("Selecciona temporada")
    print_dict(temporadas)
    temporada = int(input("Temporada "))    
    print("Selecciona mascotas (numeros, separa por ,)")
    print_dict(mascotas)
    mascotas_sel = input("Mascotas ")
    lista_mascotas = set(map(int, mascotas_sel.split(",")))
    mascotas_bin = binario_mascotas(lista_mascotas)
    seguir = input("Seguir? (no: parar)")
    return nombre, temporada, (lista_mascotas, mascotas_bin), seguir
#+END_SRC


** Función para guardar los datos 

Los datos tomados al usuario se almacenan en los archivos.

#+BEGIN_SRC python
def guardar_data(nombre, temporada, sel_mascotas, archivo, writer):
    lista_mascotas, mascotas_bin = sel_mascotas
    mascotas_lista = [mascotas.get(key) for key in lista_mascotas]    
    dato = [nombre, temporadas.get(temporada), ":".join(mascotas_lista)]
    dato_bytes = [nombre.encode(), temporada.to_bytes(2,'little'),  bitstring_to_bytes(mascotas_bin), "\n".encode()]        
    datos_binario =  b','.join(dato_bytes)
    # al csv
    writer.writerow(dato)
    # al binario
    archivo.write(datos_binario)
#+END_SRC


** El script para tomar datos al usuario 

Consiste en pedirle los datos al usuario de distintas personas y almacenarlos en
los archivos, utilizando las funciones creadas.

#+BEGIN_SRC python

archivo_ruta = "preferencias.bin"
archivo_csv_ruta = "preferencias.csv"

with open(archivo_ruta,'wb') as archivo, open(archivo_csv_ruta, "w") as archivo_csv:
    writer = csv.writer(archivo_csv, delimiter=',')
    nombre, temporada, sel_mascotas, seguir = obtener_preferencias()
    guardar_data(nombre, temporada, sel_mascotas, archivo, writer)
    while seguir!="no":
        nombre, temporada, sel_mascotas, seguir = obtener_preferencias()
        guardar_data(nombre, temporada, sel_mascotas, archivo, writer)
#+END_SRC

** Funciones de decodificar.

#+BEGIN_SRC python
def bytes_to_bitstring(val):
    return int.from_bytes(val, byteorder='little')


def decode_mascotas(mascotas_bin):
    return "{0:6b}".format(bytes_to_bitstring(mascotas_bin)).replace(" ","0")

def expresa_mascotas(bin_code):
    resultado = []
    for pos, val in enumerate(bin_code):
        if val=="1":
            resultado.append(mascotas.get(pos))
    return resultado
#+END_SRC

** Leer los archivos, decodificar

Leer los archivos de vuelta, verificamos correctamente que ambos entregan la
misma información.

#+BEGIN_SRC python
import csv
from tablas import (temporadas, mascotas, 
                    binario_mascotas, bitstring_to_bytes, 
                    bytes_to_bitstring, decode_mascotas,
                    expresa_mascotas)


archivo_ruta = "preferencias.bin"
archivo_csv_ruta = "preferencias.csv"

with open(archivo_ruta,'rb') as archivo, open(archivo_csv_ruta, "r") as archivo_csv:
    print("Lectura del binario")
    for line in archivo.readlines():
        elements_bin = line.split(b',')
        print("Binario")
        print(elements_bin)
        elements = [elements_bin[0].decode('utf8'), 
            bytes_to_bitstring(elements_bin[1]),
            decode_mascotas(elements_bin[2]), 
            elements_bin[3].decode("utf8")]
        print("Decoded ...")
        print(elements[:-1])
    print("Lectura csv")
    reader =  csv.reader(archivo_csv, delimiter=',')
    for line in reader:
        print(line)
#+END_SRC

** Comparativa de tamaño del archivo.

Para una prueba del programa, resulta

|-----------------+----------+-----------------+---------------------|
| archivo         | tamaño   | ventaja         | desventaja          |
|-----------------+----------+-----------------+---------------------|
| preferencia.csv | 101bytes | lectura humana  | crece tamaño        |
| preferencia.bin | 30bytes  | tamaño compacto | ilegible sin tablas |
|-----------------+----------+-----------------+---------------------|

* Patrones en un texto

** Buscar texto dentro de uno más grande.

A veces será necesario buscar la *existencia* o *coincidencia* de un texto
dentro de otro. Hasta el momento hemos ocupado *str.find()*. Es suficiente para
buscar un texto preciso. Pero, si necesitamos una *mayor expresividad* de las
variantes. 

- Considerar casos con mayúsculas.
- Considerar casos opcionales, posibles
- Casos bajo un patrón específico, pero no exacto de un texto.
- Repeticiones de patrones

Para eso te será de utilidad conocer las *expresiones regulares*

** Módulo de expresiones regulares

En *Python* se puede utilizar el módulo de la biblioteca estándar *re*, que
provee todo lo necesario para crear *expresiones regulares* y utilizarlas.

#+BEGIN_SRC python
import re

# busqueda clásica, a través de todo el str
regex = lambda valor: re.search("\.pdf$", valor)
archivos = ["lista.txt", "libro.pdf", "datos.csv", "pdfiles.xlxs"]
resultados = list(map(regex, archivos))
# busqueda exacta, desde el principio
regex = re.compile("\w+\.pdf$")
archivos = ["lista.txt", "libro.pdf", "datos.csv", "pdfiles.xlxs"]
resultados = list(map(regex.match, archivos))
resultados = list(map(regex.search, archivos))
#+END_SRC

- Documentación :: https://docs.python.org/3/library/re.html

** Métodos de utilidad 

|------------+-------------------------------------|
| método     | uso                                 |
|------------+-------------------------------------|
| match()    | coincidencia a partir del principio |
| search()   | coincidencia en el texto            |
| findall()  | entrega lista de coincidencias      |
| finditer() | entrega iterador de coincidencias   |
|------------+-------------------------------------|


** Validar un valor binario

Si la cadena es solo del tipo 010101

- "^[01]+$" :: de principio (^) valores 0,1 hasta el fin, al menos un carácter ($) 

#+BEGIN_SRC python
import re

def es_binario(cadena):
    regex =  re.compile("^[01]+$")
    if regex.match(cadena):
        return True
    else:
        return False
#+END_SRC

** Validar un número.

Las formas sencillas:
232323
232332.5656
- "^[0-9]*\.?[0-9]+$" :: de inicio (^). Pueden haber de nada a varios numeros (*), 
    además un "." opcional, luego uno o mas números (+)
    hasta el final


#+BEGIN_SRC python
def es_numero(cadena):
    regex = re.compile("^[0-9]*\.?[0-9]+$")
    if regex.match(cadena):
        return True
    else:
        return False
#+END_SRC    

** Tabla de símbolos posibles

|---------+----------------------------|
| símbolo | uso                        |
|---------+----------------------------|
| A a Z   | mayúsculas                 |
| a a z   | minúsculas                 |
| 0 a 9   | números                    |
| \textbackslash d      | dígitos                    |
| \textbackslash D      | no dígito                  |
| \textbackslash w      | alfanuméricos (ASCII)      |
| \textbackslash W      | no alfanuméricos (Unicode) |
| \textbackslash s      | espacio, tab, newline      |
| \textbackslash S     | no {espacio,tab, newline}  |
|---------+----------------------------|

** Tabla de referencias y conteo

|--------------+-------------------------|
| símbolo      | uso                     |
|--------------+-------------------------|
| "^"          | parte con lo que sigue  |
| "$"          | termina con lo anterior |
| "[a-z]"      | de la a-z               |
| "[abc]"      | solo a,b,c              |
| "[abc]{n}"   | a,b,c *n* veces         |
| "[abc]{n,m}" | a,b,c *n* o *m* veces   |
| "[abc]?"     | a,b,c al menos una vez  |
| "[abc]*"     | a,b,c 0 o más veces     |
| "[abc]+"     | a,b,c 1 o mas veces     |
| "(ab \vert b[cd])" |  ab o {bc,bd}           |
|--------------+-------------------------|

** ¿Cómo leer o crear una regex?

La creación de una *regex* se debe considerar cómo una serie de bloques, en que
cada bloque tenga una serie posible de símbolos, en cierta cantidad, bajo
ciertas condiciones de orden de principio a fin.

Si buscamos *mamá* y *papá* dentro de un texto. ¿Qué podemos hacer?

Tradicionalmente:

#+BEGIN_SRC python
mama = texto.find("mamá")
papa = texto.find("papá")
#+END_SRC

Pero a veces necesitaremos encontrar todas las coincidencias, para ambos casos a
la vez. Para eso podemos recurrir a alguna *regex*.

** Búsqueda de coincidencias en texto

Buscar mamá y papá en el texto.

#+BEGIN_SRC python
import re

texto = "padres.txt"
with open(texto, 'r') as archivo:
    padres = archivo.read()
    regex = re.compile("[mp]a[mp]á")
    for match in regex.finditer(padres):
        print(match)
#+END_SRC

** Reemplazar la regex por *familiar*

Hay dos formas, una en que retorna el texto reemplazado *(sub(reemplazo, string,
count=0))* y la otra en que retorna una tupla con el reemplazo y la cantidad de
cambios *(subn())*. El parámetro *count* define la cantidad de reemplazos.

#+BEGIN_SRC python
with open(texto, 'r') as archivo:
    padres = archivo.read()
    regex = re.compile("[mp]a[mp]á")
    resultado = regex.sub("familiar", texto)
    resultado_n = regex.subn("familiar", texto)
    print("caso 1", resultado)
    print("caso 2", resultado_n)
#+END_SRC

** Textos recomendados

Hay varios textos de interés para estudiar a profundidad las *regex*.

- Beggining Regular Expressions :: libro para comenzar con regex

#+attr_latex: :width 100px
#+attr_html: :width 100px
file:./img/begin_regex.jpg

- Mastering Expresiones Regulares, Jeffrey E.F. Friedl :: libro práctico de
     regex

#+attr_latex: :width 100px
#+attr_html: :width 100px
[[file:./img/regex_.jpg]]

- SICP, Structure and Interpretation of Computer Programs :: libro de teoría de computación

#+attr_latex: :width 100px
#+attr_html: :width 100px
[[file:./img/sicp.jpg]]

** Enlaces

- Puzzles con regex :: https://f-droid.org/app/de.chagemann.regexcrossword
- Regex en comando grep ::
     https://linuxize.com/post/regular-expressions-in-grep/
- Tablas regex  :: https://www.rexegg.com/regex-quickstart.html
