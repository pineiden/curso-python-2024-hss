* Un número en palabras

Este ejercicio se trata de transformar todos los números en dígitos de un texto, cualquiera
que se entregue en el número en palabras.

Ejemplo:

#+BEGIN_EXAMPLE
Tenía 9 vidas. Pasó ella y me quedaron 8.
#+END_EXAMPLE

A:

#+BEGIN_EXAMPLE
Tenía nueve vidas. Pasó ella y me quedaron ocho.
#+END_EXAMPLE

Puedes crear las funciones que consideres necesarias.

* Validar un RUT

El RUT o RUN es un identificador único. Consiste de un número y una cifra que es
el resultado de la operación específica de los dígitos.

Crear una función (o grupo de funciones) que te validen si un trozo de texto
entregado es un RUT válido o no.

Ver el procedimiento:

https://es.wikipedia.org/wiki/Rol_%C3%9Anico_Tributario

Puedes revisar las diferentes implementaciones que hay en los enlaces. Pero para
que aprendas te recomiendo trabajar en la tuya propia antes.

* Regex para fechas.

Se tiene el archivo *eventos.csv*, que contiene una serie de eventos. 
Crear una *regex* que permita encontrar y entregue solamente aquellos eventos
sucedidos después del día 15, de cada mes.

Dadas las fechas del tipo *año-mes-dia*, ejemplo: 

- 2020-06-20 :: Cumpleaños de Rebeca -> si debe mostrarse
- 2020-08-02 :: Reunión ex-compañeros -> no debe mostrarse

Una *regex* que te podría servir para partir, buscar para los días del 15 a 19,
todos los 20 y los 30. Seguramente necesita algún ajuste:

#+BEGIN_EXAMPLE
\d{4}-\d{2}-(1[56789]|2[0-9]|3[01])
#+END_EXAMPLE

* Comunas y habitantes

Teniendo la lista de comunas y habitantes. Realizar lo siguiente.

- Comunas con más de 100.000 habitantes
- Comunas en que tengan más del 50% tercera edad.
- Comunas con crecimiento desde el 30%
- Comunas cuyos ingresos sean principalmente agricultura
- Comunes cuyos ingresos sean principalmente comercio
- Comunas con mayor cantidad de diabéticos
- Comunas con mayor cantidad de madres solteras

Fuentes de datos:

- https://es.datachile.io/
- https://www.ine.cl/

* Codificación de números enteros

** Contar en binario

Cuando *hablamos de cantidad* podemos ver que *cada elemento* de un conjunto puede
ser correspondido con un *valor binario*. Es una relación *biyectiva*.  Y lo
que debemos saber es la *cantidad de bits necesarios* para contar nuestro conjunto.

Las tablas *Unicode* son una relación de símbolos con valores *binarios*. Los

- recomendado ver :: https://www.youtube.com/watch?v=Ch_jsdSRTbI

** Los números enteros con signo (negativos y positivos).

Para poder escribir un número con signo, positivo o negativo. Será necesario
conocer el valor positivo y luego hay dos modos de definir e negativo.

- Complemento a 1
- Complemento a 2


- binarios con signo :: https://www.youtube.com/watch?v=PzNYQg7J4bs


* Lectura: Cadenas genéticas y uso de regex

Las *expresiones regulares* se puede utilizar para encontrar coincidencias sobre cadenas genéticas.

Por ejemplo:

#+BEGIN_SRC python
import re
DNA = 'GAGCGCTAGCCAAA'
match = re.search('AAA', DNA)
#+END_SRC

Recomendable leer:
https://towardsdatascience.com/using-regular-expression-in-genetics-with-python-175e2b9395c2

