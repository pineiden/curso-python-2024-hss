(TeX-add-style-hook
 "03_clase"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("babel" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted"
    "babel"
    "fancyhdr")
   (LaTeX-add-labels
    "sec:org2530f28"
    "sec:org24937cd"
    "sec:org36eb49e"
    "sec:orgce3daf7"
    "sec:orgbb20085"
    "sec:org19a824e"
    "sec:org6fa6ef4"
    "sec:org7d7f69a"
    "sec:org0ed39dc"
    "sec:orgee5b1d3"
    "sec:org57c19f0"
    "sec:orgd2bb1a5"
    "sec:orgdfad6a8"
    "sec:org929d778"
    "sec:orgabb646e"
    "sec:org64b8e83"
    "sec:orge712ff3"
    "sec:org171ab2c"
    "sec:org5347f96"
    "sec:orge35a549"
    "sec:org77d3a47"
    "sec:org694a439"
    "sec:orgcd7f026"
    "sec:org01f74b0"
    "sec:org1c2c07a"
    "sec:orgc51f7f6"
    "sec:org680e075"
    "sec:org0529f19"
    "sec:org482e194"
    "sec:orga15ec7e"
    "sec:org5fe427d"
    "sec:orgd353557"
    "sec:org8f33ea0"
    "sec:orgdaa2bcd"
    "sec:orgdfe55f8"
    "sec:orgd1feace"
    "sec:org1566c1c"
    "sec:org673a568"
    "sec:org8a16889"
    "sec:org814146f"
    "sec:org03baea3"
    "sec:org431f149"
    "sec:org7cc249b"
    "sec:orga406031"
    "sec:org68dece4"
    "sec:org24503eb"
    "sec:orgca6f4c8"))
 :latex)

