import xml.etree.ElementTree as ET
from animal import Animal
tree = ET.parse('animales.xml')
dom = tree.getroot()
animales = []
for animal in dom:
    data = {}
    for elem in animal:
        data[elem.tag] = elem.text
    animal = Animal(**data)
    animales.append(animal)
print("Animales en lista")
[print(animal.nombre, animal.tipo) for animal in animales]
