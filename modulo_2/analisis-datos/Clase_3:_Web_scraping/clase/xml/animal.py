from datetime import datetime

class Animal:
    omnivoro = {"carne", "vegetal", "hongo"}
    carnivoro = {"carne"}
    vegetariano = {"vegetal"}
    def __init__(self, nombre, patas, ojos, tipo, fecha,
                medio="Tierra", alimentacion="Omnívora"):
        self.nombre = nombre
        self.patas = patas
        self.ojos = ojos
        self.tipo = tipo
        self.medio = medio
        self.alimentacion = alimentacion
        if alimentacion=="Omnívora":
            self.comidas = self.omnivoro
        elif alimentacion == "Carnívoro":
            self.comidas = self.carnivoro
        elif alimentacion == "Vegetariano":
            self.comidas = self.vegetariano            
        self.vivir()
        self.fecha_nacimiento = datetime.strptime(fecha,"%Y/%m/%d")
        self.fecha_muerte = None
        
    def vivir(self):
        self.vive = True
        
    def morir(self):
        self.vive = False
        now = datetime.now()
        self.fecha_muerte = now
        
    def sonar(self):
        pass
        
    def mover(self):
        pass
        
    def comer(self, algo):
        if algo in self.comidas:
            print(self.nombre, "come", algo)
        else:
            print(self.nombre, "no puede comer", algo, "porque es", self.alimentacion)
            
    @property
    def edad(self):
        day = 60*60*24
        now = datetime.now()
        if not self.fecha_muerte:
            return (now-self.fecha_nacimiento).total_seconds()/day
        else:
            return (self.fecha_muerte-self.fecha_nacimiento).total_seconds()/day

