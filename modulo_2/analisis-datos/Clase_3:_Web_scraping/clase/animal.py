class Animal:
    def __init__(self, nombre):
        self.nombre = nombre
        self.vivir()
        
    def vivir(self):
        self.vive = True
        
    def morir(self):
        self.vive = False


dompi = Animal("Dompi")
lili = Animal("Lili")
if dompi.vive and lili.vive:
    print(dompi.nombre, "y", lili.nombre, "son amigos")
    
dompi.morir()
if dompi.vive and lili.vive:
    print(dompi.nombre, "y", lili.nombre, "son amigos")
elif dompi.vive and not lili.vive:
    print(dompi.nombre, "está triste porque", lili.nombre, "ya no existe")
elif not dompi.vive and lili.vive:
    print(lili.nombre, "está triste porque", dompi.nombre, "ya no existe")    
