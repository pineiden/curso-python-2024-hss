import os
from pathlib import Path
import logging
from logging.handlers import RotatingFileHandler


default_fmt = '%(asctime)s %(levelname)s %(process)d'+\
                '%(pathname)s %(filename)s %(module)s '+\
                '%(funcName)s %(message)s'
                
def get_logger(archivo, directorio='./logs', id_log='test.0', 
                level="DEBUG",
                formato=default_fmt,
                max_bytes=10240, 
                backup_count=10):
    logpath = Path(directorio)
    if not logpath.exists():
        os.makedirs(str(logpath))
    file_path = logpath/archivo
    logger = logging.getLogger(id_log)
    handler = RotatingFileHandler(file_path, mode='w', 
                maxBytes=max_bytes, backupCount=backup_count)
    formatter = logging.Formatter(fmt=formato)
    handler.setLevel(level)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger

from herencia_animal import Perro

if __name__ == "__main__":
    archivo = 'registro.log'
    logger =  get_logger(archivo)
    logger.info("Se inicia logger")
    nombre = "Baloo"
    fecha_nacimiento = '2015/11/10'
    baloo = Perro(nombre, fecha_nacimiento)
    logger.warning("Acaba de nacer %s el día %s" %(nombre, fecha_nacimiento))
    print("Pasan los años...")
    baloo.morir()
    if not baloo.vive:
        logger.critical("%s ha muerto, viviendo %d días" %(baloo.nombre, baloo.edad))
        print("Baloo muere")

