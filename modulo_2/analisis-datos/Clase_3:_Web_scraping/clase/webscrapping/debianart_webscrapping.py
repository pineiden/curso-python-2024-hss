from urllib.parse import urlparse
import urllib.request
from pathlib import Path

url = "https://www.deviantart.com/cjcnightfox/"+\
        "gallery/66148905/my-best-bird-photography"


"""
Leer desde la web y descargar el texto html
"""
web = ""
with urllib.request.urlopen(url) as f:
    web=f.read().decode('utf-8')    
    
with open("galeria_aves.html","w") as libro:
    libro.write(web)

id_galeria = "sub-folder-gallery"
"""
Buscamos aquellas etiquetas <img> dentro  'id_galeria'
"""
from bs4 import BeautifulSoup
soup = BeautifulSoup(web, 'html.parser')

"""
Vemos el html
"""
#print(soup.prettify())

galeria =soup.find(id=id_galeria)
imagenes = galeria.find_all("img")
print(galeria)

"""
Carpeta destino de imágenes
"""
import os
destino = Path("./aves")
if not destino.exists():
    os.makedirs(str(destino))
    
import re

# solo las que son consumidas desde images-wixmp
source = re.compile("^https://images-wixmp*")

c = 0
for img in imagenes:
    if source.search(img["src"]):
        print(img["alt"])
        name =  img.get("alt")
        src = img["src"]
        urllib.request.urlretrieve(src, './%s/%s.jpg' % (destino, name))

    
        

