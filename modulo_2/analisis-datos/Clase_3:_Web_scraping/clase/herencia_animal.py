from animal_2 import Animal

class Perro(Animal):
    def __init__(self, nombre, fecha):
        super().__init__(nombre, 4, 2, "Perro", fecha)
        
    def sonido(self):
        print("Gau!Guau")
        
    def mover(self):
        print("Mueve la cola")
    
class Gato(Animal):
    pass

class Oveja(Animal):
    pass
    
class Vaca(Animal):
    pass


b=Perro("Baloo", "2015/10/05")

print(b.nombre, b.comidas)
