import os
from pathlib import Path

"""
Definimos el directorio
Creamos el directorio si es que no existe
"""
directorio = '.logs/'
logpath = Path(directorio)
if not self.logpath.exists():
    os.makedirs(str(self.logpath))
            
"""
Definimos el nombre del archivo
Creamos el objeto 'file_path'
"""
ruta = "registro.log"
file_path = logpath/ruta

"""
Activamos el Logger 
"""
import logging

id_log = "test.0"
logger = logging.getLogger(id_log)


"""
Creamos un gestor de logs 
"""

from logging.handlers import RotatingFileHandler

max_bytes = 10240
backup_count = 10
handler = RotatingFileHandler(file_path, mode='w', 
            maxBytes=max_bytes, backupCount=backup_count)

"""
Definir formato de almacenado
"""

formatter = logging.Formatter(
    fmt='%(asctime)s %(levelname)s  %(process)d '+\
    '%(pathname)s %(filename)s %(module)s '+\
    '%(funcName)s %(message)s')

"""
Asociamos logger al handler y definimos el nivel aceptable.

"""
handler.setLevel(LOG_LEVEL)
logger.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(LOG_LEVEL)
