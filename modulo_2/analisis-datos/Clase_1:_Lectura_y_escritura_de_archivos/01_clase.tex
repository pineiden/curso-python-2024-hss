% Created 2020-05-13 mié 22:06
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage[spanish]{babel}
\usepackage{fancyhdr}
\pagestyle{fancyplain}
\lhead{}
\rhead{Quux Ltda, www.quux.cl, contacto@quux.cl}
\cfoot{Módulo 2, Clase 01: Lectura y escritura de archivos de distintos tipos }
\author{David Pineda Osorio\thanks{davidpineda@quux.cl}}
\date{8 de marzo 2020}
\title{Lectura y escritura a archivos de distintos tipos}
\hypersetup{
 pdfauthor={David Pineda Osorio},
 pdftitle={Lectura y escritura a archivos de distintos tipos},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.1 (Org mode 9.1.9)}, 
 pdflang={Spanish}}
\begin{document}

\maketitle
\setcounter{tocdepth}{1}
\tableofcontents


\section{Objetivo de la clase}
\label{sec:org43886f6}

Esta clase tiene como objetivo aprender técnicas y metodologías para el
procesamiento adecuado de la información almacenada en archivos.

Existe una miriada diferente de tipos de archivos. En esta ocasión mostraremos
algunos ejemplos de como trabajar con los tipos más comunes.

\section{Conocer la información de un archivo}
\label{sec:org9e3a67e}

Un archivo se caracteriza por \textbf{contener información}, cada tipo de archivo tiene
una extensión y codificación. Siempre es más fácil trabajar con archivos de texto.

\begin{description}
\item[{txt,csv,json}] archivo de texto, utf-8
\item[{json}] archivo de texto, utf-8
\item[{xlsx}] planilla de cálculo MS Office, Office Open XML
\item[{odc}] planilla de cálculo OpenDocument
\end{description}

Además, debes considerar los \textbf{permisos de lectura/escritura}. Estos deben estar
habilitados para la tarea que sea necesaria.

\begin{center}
\includegraphics[width=.9\linewidth]{./img/propiedades_win.jpg}
\end{center}

\section{Archivos de texto}
\label{sec:orge98bf59}

\subsection{Lectura de archivos (completo)}
\label{sec:org92ce081}

Para leer archivos será necesario, al menos, utilizar la función \textbf{open()} y se
recomienda usar con el gestor de contexto \textbf{with}.

\begin{minted}[]{python}
ruta_archivo = 'ejemplo.csv'
with open(ruta_archivo, 'r') as objeto_archivo:
    print(objeto_archivo.read())
\end{minted}

También, se puede abrir el archivo sin \textbf{contexto}, pero se debe tener cuidado de cerrar.

\subsection{Leer un trozo de n símbolos}
\label{sec:org0e95042}

Cada carácter \textbf{utf-8} consta de \textbf{8 bits=1 byte} si es del conjunto \textbf{ASCII}.
También, si son símbolos especiales, pueden ser de bloques de hasta \textbf{4 bytes}.
Por lo que, dependiendo del modo de lectura será posible leer \textbf{n bytes} o \textbf{n
caracteres}. Si el archivo es \textbf{texto}, el intérprete únicamente leerá
caracteres. En otros casos leerá bytes. 

\begin{minted}[]{python}
ruta_archivo = 'ejemplo.csv'
archivo = open(ruta_archivo, 'r') 
# leer 10 símbolos
archivo.read(10)
archivo.close()
\end{minted}

\subsection{Moverse en un archivo con \textbf{seek}}
\label{sec:org2e63b6f}

Permite ubicar el cursor de lectura del archivo en una posición determinada. El
parámetro \textbf{desde\_donde (whence)} es por defecto 0 para desde el inicio, 1 desde
la posición actual, 2 desde el final del archivo. Se debe usar el modo 'rb' para
poder moverse entre \textbf{bytes}. Sin embargo, la función read(n) leerá por símbolos
(caracteres). 

Es de utilidad para procesar archivos grandes, extrayendo solo lo necesario.

\begin{verbatim}
archivo.seek(posicion, desde_donde={0,1,2})
\end{verbatim}

\begin{center}
\includegraphics[width=.9\linewidth]{./img/seek.png}
\end{center}

\subsection{Obtener la posición en lectura con \textbf{tell}}
\label{sec:org870c0fb}

Si necesitamos saber en qué caracter está posicionado la lectura en el archivo,
entonces podemos usar el método \textbf{tell()}.

\begin{minted}[]{python}
ruta_archivo = 'ejemplo.csv'
archivo = open(ruta_archivo, 'r') 
print(archivo.tell())
# leer 10 simbolos
archivo.read(10)
print(archivo.tell())
archivo.close()
\end{minted}

\subsection{Un ejemplo de uso de seek(pos, whence), tell()}
\label{sec:orgec445ba}

Por ejemplo

\begin{minted}[]{python}
ruta_archivo = 'ejemplo.csv'
archivo = open(ruta_archivo, 'rb')
posicion = 8
archivo.seek(posicion)
print("Ubicación", archivo.tell())
print(archivo.read(16))
archivo.seek(posicion, 1)
print(archivo.read(16))
# contar hacia atrás, usando whence=2
archivo.seek(-15,2)
print(archivo.read())
archivo.close()
\end{minted}

\subsection{Lectura de archivos (iterando)}
\label{sec:orgd8d6f76}

Para leer archivos será necesario, al menos, utilizar la función \textbf{open()} y se
recomienda usar en contexto con \textbf{with}.

\begin{minted}[]{python}
ruta_archivo = 'ejemplo.csv'
with open(ruta_archivo, 'r') as objeto_archivo:
    for fila in objeto_archivo.readlines():
        print(fila)
\end{minted}

\subsection{Lectura como csv}
\label{sec:orga7934cb}

De manera mucho más efectiva, leer un archivo csv con la ayuda del módulo \textbf{csv}
te permitirá obtener cada fila como una \textbf{tupla} de datos.

\begin{minted}[]{python}
import csv
ruta_archivo = 'ejemplo.csv'
with open(ruta_archivo, 'r') as objeto_archivo:
    reader =  csv.reader(objeto_archivo, delimiter=',', doublequote=True)
    for fila in reader:
        print(fila)
        print(list(map(type,fila)))
\end{minted}

\subsection{Lectura como csv (deduciendo formato)}
\label{sec:org0bf3121}

Para determinar las características de construcción del archivo \textbf{csv} (depende
el sistema operativo hay caracteres invisibles).

\begin{minted}[]{python}
import csv
ruta_archivo = 'ejemplo_2.csv'
with open(ruta_archivo, 'r') as objeto_archivo:
    # se crea sniffer
    sniffer = csv.Sniffer()
    # se lee la primera serie de datos, leyendo 1024 caracteres
    sample = objeto_archivo.read(1024)
    dialect = sniffer.sniff(sample)
    # se muestra si tiene encabezado
    print("Tiene encabezado", sniffer.has_header(sample))
    # se vuelve a la posición 0
    objeto_archivo.seek(0)
    # se crea objeto lector csv
    reader =  csv.reader(objeto_archivo, dialect)
    for fila in reader:
        print(fila)
        print(list(map(type,fila)))
\end{minted}

Probar con \textbf{ejemplo2.csv}. El \textbf{sniffer} nos entrega la flexibilidad, por ejemplo
frente al cambio del símbolo separador (',' o ';' o ':').

\subsection{Escritura de archivos}
\label{sec:org2e9a4cd}

Si deseamos escribir un \textbf{nuevo archivo} abrir con \textbf{w}, si se desea agregar a lo
que ya hay se usa \textbf{a}. Usando \textbf{:=}, válido para Python v3.8.

\begin{minted}[]{python}
archivo_ruta = "frases.txt"
with open(archivo_ruta,'w') as frases:
    while (resp := input("Alguna frase? (salir->no)")) != 'no' :
        print(f"Se escribe a archivo {archivo_ruta} la frase {resp}")
        frases.write(resp+"\n")
\end{minted}

\subsection{Escritura a archivo csv}
\label{sec:org80728b7}

Podemos también escribir a un archivo \textbf{csv}, agregando nueva información 'ab+' o bien
creando un nuevo archivo 'wb+'. Se añade 'b' para posibilitar la navegación con
\textbf{seek}. El uso de \textbf{+} permite leer y escribir.

\begin{minted}[]{python}
def detectar(archivo, trozo=1024):
    ids = set()
    sniffer = csv.Sniffer()
    archivo.seek(0)
    sample = archivo.read(trozo)
    dialect = sniffer.sniff(sample)
    archivo.seek(0)
    reader =  csv.reader(archivo, dialect)     
    if sniffer.has_header(sample):
        header = next(reader)
    for fila in reader:
        idr = int(fila[0])
        ids.add(idr)
    archivo.seek(0)
    return ids, header, dialect
\end{minted}

Creamos función que entrega un nuevo id válido.

\begin{minted}[]{python}
def new_id(ids, start=0):
    nid = start
    while nid in ids:
        nid += 1
    ids.add(nid)
    return nid
\end{minted}


Creamos el código para escribir al \textbf{csv}.

\begin{minted}[]{python}
archivo_ruta = "ejemplo_escritura.csv"
with open(archivo_ruta,'a+') as archivo:
    ids, header, dialect = detectar(archivo)
    writer =  csv.writer(archivo, dialect, quoting=csv.QUOTE_NONNUMERIC)     
    nid = max(ids)
    while (nombre := input("nombre?")) != "no":
         edad = int(input("edad"))
         nid = new_id(ids, nid)
         fila = [nid, nombre, edad]
         writer.writerow(fila) 
\end{minted}

Una alternativa es usar \textbf{csv.DictWriter}.

\begin{minted}[]{python}
with open(archivo_ruta,'a+') as frases:
    ids, header, dialect = detectar(archivo)
    writer =  csv.DictWriter(objeto_archivo, dialect, , quoting=csv.QUOTE_NONNUMERIC)     
    nid = max(ids)
    while (nombre := input("nombre?")) != "no":
         edad = int(input("edad"))
         nid = new_id(ids, nid)
         datos = dict(zip(header, [nid, nombre, edad]))
         writer.writerow(datos) 
\end{minted}

\subsection{Lectura de archivos JSON}
\label{sec:org1fe4904}

Un archivo JSON contiene una estructura de datos similar a \textbf{diccionario}, con
llaves y valores. Tiene tipos de datos \textbf{string}, \textbf{número}, \textbf{boolean}. Puede
contener una combinación de \textbf{listas} y \textbf{diccionarios json}. Todo en formato de
archivo de texto, es texto.

\begin{minted}[]{python}
import json

ruta = "station.json"
archivo = open(ruta,'r')
datos = json.load(archivo)
print(type(datos))
[print(elem) for elem in datos]
\end{minted}

\subsection{Escritura de archivos JSON}
\label{sec:orgd0e321b}

Por ejemplo, si recolectamos el perfil de varias personas, lo podemos guardar en \textbf{json}.

\begin{minted}[]{python}
import json

archivo_ruta = "perfiles.json"
perfiles = []

keys = ["id", "nombre", "edad"]
idp = 0
while (nombre := input("nombre? ")) != "no":
     edad = int(input("edad "))
     perfil = dict(zip(keys, [idp, nombre, edad]))
     perfiles.append(perfil)
     idp += 1

archivo = open(archivo_ruta,'w')
# guardar lista como json
json.dump(perfiles, archivo, indent=4)
archivo.close()
\end{minted}

\subsection{Convertir CSV a JSON}
\label{sec:org87d362d}

A veces será conveniente transformar un archivo \textbf{csv} a un \textbf{json}. Que es un
formato que lo pueden leer fácilmente los lenguajes modernos. Considerando
siempre que el tamaño del \textbf{csv} no sea grande (gestión de la memoria).

Leeremos un archivo en formato \textbf{csv} y lo transformaremos a un \textbf{json}.

\begin{minted}[]{python}
import csv
import json

ruta = "mes_temp.csv"
ruta_json = "mes_temp.json"

lista = []

with open(ruta,'r') as archivo:
    reader = csv.DictReader(archivo)
    for elem in reader:
        lista.append(elem)

print(lista)
\end{minted}

Ahora, transformamos la lista de diccionarios en un json string.

\begin{minted}[]{python}
with open(ruta_json, 'w', encoding='utf8') as archivo_json:
    # transformamos a un objeto json_data antes de guardar
    # nos asegura la codificacion
    json_data = json.dumps(lista, indent=2, ensure_ascii = False)
    # vemos el tipo de dato antes de guardar
    # es un string
    print(type(json_data))
    archivo_json.write(json_data)
\end{minted}

\section{Planillas de cálculo}
\label{sec:org4ff67db}

Las planillas de cálculo son documento que contienen información ordenada en
\textbf{filas, columnas}. Pueden contener varias hojas, fórmulas y macros.

Instalaremos el módulo \textbf{openpyxl} para acceder a trabajar con planillas de
cálculo. (\url{https://openpyxl.readthedocs.io})

\begin{minted}[]{bash}
pip install openpyxl
\end{minted}

Con este módulo será posible no solo leer información sino añadir imágenes,
tablas y datos, almacenando la nueva información en el mismo archivo.

\subsection{Trabajo de archivos Excel}
\label{sec:orgce47d91}

Leeremos un directorio de bibliotecas, está almacenado en un archivo
\textbf{donde\_leer.xlsx} y extraeremos alguna información de interés. Supongamos que
nos interesan los campos (nombre, dirección, comuna, teléfono, correo electrónico).

\begin{itemize}
\item ¿En cuál hoja está la información?
\item ¿En qué posición están los campos de interés?
\item ¿Qué nombre tienen en el archivo?
\end{itemize}

\subsection{Listar las hojas de un excel}
\label{sec:org3b83977}

Llamando a la función que permite acceder a la planilla.

\begin{minted}[]{python}
from openpyxl import load_workbook

def show_sheets(wb):
    print(wb.sheetnames)

def sheet_active(wb):
    print(wb.active)
    return wb.active

def sheet_name(sheet):
    print(sheet.title)

archivo_excel = "donde_leer.xlsx"
wb = load_workbook(archivo_excel)
show_sheets(wb)
sheet = sheet_active(wb)
print(sheet, type(sheet), sheet_name(sheet))
\end{minted}

\subsection{Estudiar el código fuente de WorkSheet}
\label{sec:orgd3b05d6}

El objeto que nos permitirá manipular la hoja será \textbf{WorkSheet}, las acciones
posibles y atributos se pueden estudiar desde el código fuente del módulo.

\url{https://bitbucket.org/openpyxl/openpyxl/src/default/openpyxl/worksheet/worksheet.py}

Es buena costumbre estudiar el \textbf{código fuente} y la \textbf{documentación} del software
que utilizarás. Así tendrás seguridad de lo que haces y aprenderás siempre.

\subsection{Buscar los campos de interés}
\label{sec:org80e6901}

Tomaremos la primera fila y mostraremos los nombres de cada campo.

Veremos las columnas. La primera y la última.

\begin{minted}[]{python}
def show_columns(sheet):
    primera = sheet.min_column
    ultima = sheet.max_column
    header = sheet.iter_cols(min_row=1, max_row=1, 
        min_col=primera, max_col=ultima,)
    for cell in header:
        print(cell[0].coordinate, cell[0].value)
\end{minted}

\subsection{Tabla de posiciones}
\label{sec:org8a48fad}

El resultado de la inspección no da la siguiente tabla:

\begin{center}
\begin{tabular}{lllrr}
\hline
Campo & Nombre & Posicion & Fila & Columna\\
\hline
nombre & Nombre \ldots{} & D1 & 1 & 4\\
dirección & Dirección & H1 & 1 & 8\\
comuna & Comuna & I1 & 1 & 9\\
teléfono & Teléfono (solo.. & J1 & 1 & 10\\
email & Dirección de .. & K1 & 1 & 11\\
 &  &  &  & \\
\hline
\end{tabular}
\end{center}

Debemos convertirla a un diccionario para trabajar..

Construimos el diccionario con la información que consideramos relevante.

\begin{minted}[]{python}
seleccion = {
    "nombre" : {'letra':"D", 'column':4},
    "dirección" : {'letra':"H", 'column':8},
    "comuna" : {'letra':"I", 'column':9},
    "teléfono" : {'letra':"J", 'column':10},
    "email" : {'letra':"K", 'column':11},
}

\end{minted}

\subsection{Mostrar la información del archivo seleccionada}
\label{sec:org552d26e}

Ahora, iteramos sobre todas las filas  y extramemos los campos seleccionados.
construimos un diccionario en base a la selección, que es la tabla representada
como diccionario. 

\begin{minted}[]{python}
def get_seleccion(sheet, seleccion):
    # ahora, desde la segunda fila hasta la úlima
    primera = sheet.min_row + 1
    ultima = sheet.max_row
    filas = sheet.iter_rows(min_row=primera, max_row=ultima)
    seleccion_data = []
    for row_tup in filas:
        elem = {}
        for key, value in seleccion.items():
            columna = value.get('column')
            cell = row_tup[columna-1]
            if isinstance(cell.value, float):
                elem[key] = str(int(cell.value))
            else:
                elem[key] = str(cell.value)
        print(elem)
        seleccion_data.append(elem)
    return seleccion_data
\end{minted}

\subsection{Guardar a un json}
\label{sec:org7d149a4}

Repasando, guardar una lista de diccionarios a json.

\begin{minted}[]{python}
import json
ruta_json = "donde_leer.json"
seleccion_data = get_seleccion(sheet, seleccion)
with open(ruta_json, 'w', encoding='utf8') as archivo_json:
    json_data = json.dumps(seleccion_data, indent=2, ensure_ascii = False)
    archivo_json.write(json_data)
\end{minted}

¿Cómo debe ser para guardar a csv?

\section{Trabajo con imágenes}
\label{sec:orga0e0141}

\subsection{Las imágenes como archivos.}
\label{sec:orge79c989}

Los archivos de imágenes, como \textbf{jpg, gif, png, tiff} contienen metainformación
(que puede ser tamaño, dimensión,fecha de generación del archivo, formato), así como la
información misma de la imagen. Son las llamada \textbf{mapa de bits}, es decir se
componen de matrices de una o más dimensiones, en que cada celda llamada \textbf{pixel}
contiene un valor entero.

Una imagen a \textbf{color} se compone de tres capas \{rojo, verde, azul\} y cada una de
ellas es del mismo tamaño, cada pixel tiene un valor que va desde 0 a 255.

En los siguientes ejercicios realizaremos algunas acciones que son posibles de
realizar con imágenes. Leeremos algunos archivos y transformaremos la
información que contienen.

\subsection{Leer y mostrar}
\label{sec:orgf8c263a}

Tenemos dos imágenes, una a color, otra en blanco y negro.

\begin{center}
\begin{tabular}{lll}
\hline
imagen & características & tamaño\\
\hline
eva\_green.jpg & color & 6.0kb\\
charles\_chaplin.jpeg & blanco/negro & 101.1kb\\
\hline
\end{tabular}
\end{center}

\subsection{Uso de pillow, Image}
\label{sec:org6e36ed6}

Instalaremos el módulo \textbf{pillow}, que nos entrega las herramientas básicas para
acceder a las imágenes.

\begin{minted}[]{bash}
pip install pillow
\end{minted}

\begin{description}
\item[{Documentación}] \url{https://pillow.readthedocs.io}
\end{description}

\subsection{La metainformación de una imagen.}
\label{sec:org55d7dc1}

Se extrae la información característica de cada archivo

\begin{minted}[]{python}
def show_info(img):
    print(img.format, img.size, img.mode)

[show_info(img) for img in imgs]
\end{minted}

\subsection{Visualizar la imagen}
\label{sec:org73153d4}

Para ver las imágenes se utiliza \textbf{im.show()}

\begin{minted}[]{python}
from PIL import Image

lista = ["eva_green.jpg", "charles_chaplin.jpeg"]

def read_image(ruta):
    return Image.open(ruta)

imgs = list(map(read_image, lista))

[img.show() for img in imgs]
\end{minted}

\subsection{Rotar la imagen}
\label{sec:org03372d1}

La primera en 45° y la segunda en 60°. Se guardan con el "rot\_"+nombre

\begin{minted}[]{python}
rotacion = [45, 60]

rotadas=[img.rotate(rotacion[i]) for i, img in enumerate(imgs)]
nombres_rot = ["rot_%s" %nombre for nombre in lista]

[img.show() for img in rotadas]
[img.save(nombres_rot[i]) for i, img in enumerate(rotadas) ]
\end{minted}

\subsection{Visualizar las capas por separado.}
\label{sec:org11b09ba}

Se utiliza el método \textbf{split()} sobre el objeto imagen. 

\begin{minted}[]{python}
def show_capas(img):
    capas = dict(zip(('r','g','b'),(0,1,2)))
    source = img.split()
    [layer.show() for layer in source]

[show_capas(img) for img in imgs]
\end{minted}

\subsection{Intercambiar las capas RGB -> BRG}
\label{sec:orgf90b929}

Se utiliza la función \textbf{merge} de la clase \textbf{Image}.

\begin{minted}[]{python}
def new_order(img, new_order=("b","r","g")):
    capas = dict(zip(('r','g','b'),(0,1,2)))
    new_capas = []
    source = img.split()
    for nc in new_order:
        index = capas[nc]
        new_capas.append(source[index])
    return Image.merge("RGB",new_capas)    

new_images = [new_order(img) for img in imgs]

[img.show() for img in new_images]
\end{minted}

\subsection{Redimensionar}
\label{sec:org24a51da}

Para redimensionar a un tamaño específico.

\begin{minted}[]{python}
new_size = (280, 280)

imgs_chicas = [img.resize(new_size) for img in imgs]
[img.show() for img in imgs_chicas]
\end{minted}

\subsection{Crear una función de escalado}
\label{sec:org1f28138}

Necesitamos, sin embargo, no perder la proporción de la imagen. Para eso debemos
definir una función de escalado.

\begin{itemize}
\item si es \textbf{float} de 0 a 1 se escalará a un porcentaje.
\item si es \textbf{int} se escala a ese tamaño en pixeles
\item los parámetros son \textbf{width} o \textbf{height}.
\item si están los dos escoger el mayor valor
\item obtener el nuevo tamaño a redimensionar
\end{itemize}

Para implementar la función será necesario un código como este:

\begin{minted}[]{python}
def escalar(img, **kwargs):
    width = abs(kwargs.get("width",0))
    height = abs(kwargs.get("height",0))
    parametro = max([width, height])
    prim = 0
    sec = 1    
    if parametro == height:
        prim = 1
        sec = 0
    a = img.size[prim]
    b = img.size[sec]        
    if isinstance(parametro,float):
        print("Es float")
        if parametro<=1.0:
            parametro = int(parametro * a)
        else:
            parametro =  int(parametro)
        print("Resulta param", parametro)
    proporcion =  a/b
    new_parametro = int(parametro/proporcion)
    if prim == 0:
        new_size = (parametro, new_parametro)
    else:
        new_size = (new_parametro, parametro)
    return img.resize(new_size)
\end{minted}

\subsection{Leer gif animado}
\label{sec:org6ff19f5}

Algún gif divertido, como \textbf{the\_mask.gif} nos será de ayuda:

\begin{minted}[]{python}
from PIL import Image

nombre_gif="the_mask.gif"
im = Image.open(nombre_gif)
im.seek(1) # skip to the second frame
try:
    while 1:
        im.show()
        im.seek(im.tell()+5)        
        # do something to im
except EOFError:
    pass # end of sequence
\end{minted}

\section{Serialización de datos}
\label{sec:org5ebe373}

\subsection{Una esquema para mejor manejo de datos}
\label{sec:orgf3194f3}

Para tener claro todo, nos servirá este esquema. Nos habla de cómo será
necesario transformar un dato desde su fuente hasta que sea un elemento
utilizable.

\begin{center}
\includegraphics[width=.9\linewidth]{./img/dato_util.png}
\end{center}

\subsection{Explicación de cada paso}
\label{sec:orge9a5ed6}

\begin{description}
\item[{validación}] consiste en verificar que un valor puede ser transformado a un
tipo de dato adecuado. Revisar si un dato en campo de \textbf{email} es
un correo electrónico.

\item[{transformación}] en caso de que sea un dato válido, se transforma al tipo de
dato adecuado.

\item[{agrupación}] se agrupa con otros datos ya validados para construir un objeto
de utilidad para el trabajo requerido.
\end{description}

\subsection{Serialización}
\label{sec:org72a1559}

La serialización es la acción que permite conservar un objeto como una cadena de
bytes. No todos los objetos son serializables, por lo que hay que tener cuidado.

\begin{center}
\includegraphics[width=.9\linewidth]{./img/trx_dato.png}
\end{center}


\subsection{Transformación de bytes a string y viceversa}
\label{sec:org4491684}

El proceso de transformar información desde una cadena de \textbf{bytes} a un \textbf{objeto}
que sea posible manipular y viceversa se llama \textbf{serialización} (byte a objeto) y
\textbf{deserialización} (objeto a byte).

Lo que se de-serializa desde Python es la representación del objeto como bytes,
proceso que permite reconstruirlo en memoria principal. Para datos básicos la
representación será el objeto mismo. 

Internamente, cuando se lee un archivo cada \textbf{byte} se de-serializa en una cadena de
caracteres. 

\begin{minted}[]{python}
def texto_codificar(texto):
    txt_bytes=texto.encode('utf-8')    
    print("Texto codificado", txt_bytes)
    print(type(txt_bytes))
    decodificado=txt_bytes.decode('utf-8')
    print("Texto de-codificado", decodificado)    
    # ver los valores decimales de cada símbolo
    [print(char) for char in txt_bytes]
    # error!
    try:
        [print(ord(char)) for char in txt_bytes]
    except Exception as e:
        print("Hubo error %s" %e)
    # ver los valores binarios de cada símbolo
    # lo que ve el computador en memoria
    [print(bin(char)) for char in txt_bytes]
\end{minted}

Ahora, probamos la función con dos textos. Uno que contenga todos sus símbolos
en el grupo \textbf{ASCII} y otro que tiene símbolos extra.

\begin{minted}[]{python}
texto="Tres tristes tigres"
texto_codificar(texto)
texto2="Este cabrón me quitó el terreno"
texto_codificar(texto2)
\end{minted}

\subsection{La tabla unicode}
\label{sec:orge652044}

Para conocer más respecto a los símbolos que construyen un texto, podemos
indagar en las tablas \textbf{unicode}. En \textbf{Python} tenemos disponible el módulo
\textbf{unicodedata}, que nos provee la información de las tablas \textbf{unicode}.

Podemos obtener la información de cada símbolo de un texto.

\begin{minted}[]{python}
import unicodedata

def info_unicode(char):
    value = ord(char)
    info = {
        "name":unicodedata.name(char),
        "category":unicodedata.category(char),
        "bidirectional":unicodedata.bidirectional(char), 
        "decomposition": unicodedata.decomposition(char),
        "hexadecimal": hex(value),
        "decimal": value,    
    }
    return info

[print(info_unicode(char)) for char in "Hola mundo!"]
\end{minted}

\subsection{El módulo pickle}
\label{sec:org6173986}

Para objetos más complejos, como diccionarios o listas, será necesario que cada
elemento particular que le componen sea \textbf{serializable}. Una opción que se tiene
es transformar la \textbf{representación} del objeto a una cadena de \textbf{bytes}. Para eso
nos será de utilidad el módulo \textbf{pickle}.

Este módulo permitirá transformar \textbf{cualquier objeto serializable} en un cadena
de bytes y luego poder recuperarlo. Permitiendo así la posibilidad de construir
aplicaciones que operan de manera independiente, compartiendo aún sus elementos.

Tiene utilidad para \textbf{comunicación de datos}, ya que debe ocurrir en un \textbf{stream
de bytes} y no cómo el objeto alojado en memoria.

\subsection{Pickle a un string}
\label{sec:orgbcbd95f}

Aplicar a un string

\begin{minted}[]{python}
import pickle

texto ="Este cabrón me quitó el terreno"
texto_serializado = pickle.dumps(texto)
print(texto, texto_serializado)
recuperado = pickle.loads(texto_serializado)
print(texto, recuperado, type(recuperado))
\end{minted}

\subsection{Pickle a un número}
\label{sec:org68152fd}

Similar también es para un tipo básico numérico

\begin{minted}[]{python}
numero = 1000.23
numero_serializado = pickle.dumps(numero)
print(numero, numero_serializado)
recuperado = pickle.loads(numero_serializado)
print(numero, recuperado, type(recuperado))
\end{minted}


\subsection{Pickle a un diccionario}
\label{sec:orgb8b0018}

Para una estructura algo más compleja como un diccionario.


\begin{minted}[]{python}
seleccion = {
    "nombre" : {'letra':"D", 'column':4},
    "dirección" : {'letra':"H", 'column':8},
    "comuna" : {'letra':"I", 'column':9},
    "teléfono" : {'letra':"J", 'column':10},
    "email" : {'letra':"K", 'column':11},
}
seleccion_serializado = pickle.dumps(seleccion)
print(seleccion, seleccion_serializado)
recuperado = pickle.loads(seleccion_serializado)
print(seleccion, recuperado, type(recuperado))
\end{minted}
\end{document}