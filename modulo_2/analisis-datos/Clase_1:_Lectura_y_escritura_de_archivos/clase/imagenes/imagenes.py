from PIL import Image

lista = ["eva_green.jpg", "charles_chaplin.jpeg"]

def read_image(ruta):
    return Image.open(ruta)
    
imgs = list(map(read_image, lista))

[img.show() for img in imgs]

def show_info(img):
    print(img.format, img.size, img.mode)
    
[show_info(img) for img in imgs]

rotacion = [45, 60]

rotadas=[img.rotate(rotacion[i]) for i, img in enumerate(imgs)]
nombres_rot = ["rot_%s" %nombre for nombre in lista]

[img.show() for img in rotadas]
[img.save(nombres_rot[i]) for i, img in enumerate(rotadas) ]


def show_capas(img):
    capas = dict(zip(('r','g','b'),(0,1,2)))
    source = img.split()
    [layer.show() for layer in source]
    return source

[show_capas(img) for img in imgs]

def new_order(img, new_order=("b","r","g")):
    capas = dict(zip(('r','g','b'),(0,1,2)))
    new_capas = []
    source = img.split()
    for nc in new_order:
        index = capas[nc]
        new_capas.append(source[index])
    return Image.merge("RGB",new_capas)    
    
new_images = [new_order(img) for img in imgs]

[img.show() for img in new_images]

"""
Redimensionar
"""

new_size = (280, 280)

imgs_chicas = [img.resize(new_size) for img in imgs]
[img.show() for img in imgs_chicas]


"""
Escalar

- si es *float* de 0 a 1 se escalará a un porcentaje.
- si es *int* se escala a ese tamaño en pixeles
- los parámetros son *width* o *height*.
- si están los dos escoger el mayor valor
- obtener el nuevo tamaño a redimensionar
"""

def escalar(img, **kwargs):
    width = abs(kwargs.get("width",0))
    height = abs(kwargs.get("height",0))
    parametro = max([width, height])
    prim = 0
    sec = 1    
    if parametro == height:
        prim = 1
        sec = 0
    a = img.size[prim]
    b = img.size[sec]        
    if isinstance(parametro,float):
        print("Es float")
        if parametro<=1.0:
            parametro = int(parametro * a)
        else:
            parametro =  int(parametro)
        print("Resulta param", parametro)
    proporcion =  a/b
    new_parametro = int(parametro/proporcion)
    if prim == 0:
        new_size = (parametro, new_parametro)
    else:
        new_size = (new_parametro, parametro)
    return img.resize(new_size)

opts = {'width':.4}
imgs_escaladas = [escalar(img, **opts) for img in imgs]

print("Imágenes escaladas")
[show_info(img) for img in imgs_escaladas]

[img.show() for img in imgs_escaladas]
