
from PIL import Image

nombre_gif="the_mask.gif"
im = Image.open(nombre_gif)
im.seek(1) # skip to the second frame
try:
    while 1:
        im.show()
        im.seek(im.tell()+5)        
        # do something to im
except EOFError:
    pass # end of sequence
