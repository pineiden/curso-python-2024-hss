from openpyxl import load_workbook

def show_sheets(wb):
    print(wb.sheetnames)

def select_sheet(wb, sheetname):
    sheet_index = wb.sheetnames.index(sheetname)    
    wb.active = sheet_index
    
def sheet_active(wb):
    print(wb.active)
    return wb.active
    
def sheet_name(sheet):
    print(sheet.title)

def show_columns(sheet):
    primera = sheet.min_column
    ultima = sheet.max_column
    header = sheet.iter_cols(min_row=1, max_row=1, 
        min_col=primera, max_col=ultima,)
    for cell_tup in header:
        cell = cell_tup[0]        
        print(cell.coordinate, "ROW:", cell.row, "COLUMN:", cell.column, cell.value)
    
archivo_excel = "donde_leer.xlsx"
wb = load_workbook(archivo_excel)
show_sheets(wb)
sheet = sheet_active(wb)
print(sheet, type(sheet), sheet_name(sheet))

show_columns(sheet)

seleccion = {
    "nombre" : {'letra':"D", 'column':4},
    "dirección" : {'letra':"H", 'column':8},
    "comuna" : {'letra':"I", 'column':9},
    "teléfono" : {'letra':"J", 'column':10},
    "email" : {'letra':"K", 'column':11},
}

"""
Ahora, iteramos sobre todas las filas 
y extramemos los campos seleccionados.
construimos un diccionario en base a la selección, 
que es la tabla representada como diccionario.
"""
def get_seleccion(sheet, seleccion):
    # ahora, desde la segunda fila hasta la úlima
    primera = sheet.min_row + 1
    ultima = sheet.max_row
    filas = sheet.iter_rows(min_row=primera, max_row=ultima)
    seleccion_data = []
    for row_tup in filas:
        elem = {}
        for key, value in seleccion.items():
            columna = value.get('column')
            cell = row_tup[columna-1]
            if isinstance(cell.value, float):
                elem[key] = str(int(cell.value))
            else:
                elem[key] = str(cell.value)
        print(elem)
        seleccion_data.append(elem)
    return seleccion_data
    
"""
Guardar la info como csv
"""
import json
ruta_json = "donde_leer.json"
seleccion_data = get_seleccion(sheet, seleccion)
with open(ruta_json, 'w', encoding='utf8') as archivo_json:
    # transformamos a un objeto json_data antes de guardar
    # nos asegura la codificacion
    json_data = json.dumps(seleccion_data, indent=2, ensure_ascii = False)
    # vemos el tipo de dato antes de guardar
    # es un string
    print(type(json_data))
    archivo_json.write(json_data)
