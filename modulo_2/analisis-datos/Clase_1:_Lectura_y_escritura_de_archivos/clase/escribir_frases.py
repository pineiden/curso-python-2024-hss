
"""
Para python>=v3.8 operador 'morsa' :=

"""
archivo_ruta = "frases.txt"
with open(archivo_ruta,'w') as frases:
    while (resp := input("Alguna frase? (salir->no)")) != 'no' :
        print(f"Se escribe a archivo {archivo_ruta} la frase {resp}")
        frases.write(resp+"\n")

"""
Python <v3.8

archivo_ruta = "frases.txt"
with open(archivo_ruta,'w') as frases:
    resp = input("Alguna frase?")
    while resp != 'no' :
        print(f"Se escribe a archivo {archivo_ruta} la frase {resp}")
        frases.writeline(resp)
        resp = input("Alguna frase?")
"""
