import pickle

"""
Aplicar a un string
"""
texto ="Este cabrón me quitó el terreno"
texto_serializado = pickle.dumps(texto)
print(texto, texto_serializado)
recuperado = pickle.loads(texto_serializado)
print(texto, recuperado, type(recuperado))

"""
Apicar a un numero
"""

numero = 1000.23
numero_serializado = pickle.dumps(numero)
print(numero, numero_serializado)
recuperado = pickle.loads(numero_serializado)
print(numero, recuperado, type(recuperado))


"""
Aplicar a un diccionario
"""

seleccion = {
    "nombre" : {'letra':"D", 'column':4},
    "dirección" : {'letra':"H", 'column':8},
    "comuna" : {'letra':"I", 'column':9},
    "teléfono" : {'letra':"J", 'column':10},
    "email" : {'letra':"K", 'column':11},
}
seleccion_serializado = pickle.dumps(seleccion)
print(seleccion, seleccion_serializado)
recuperado = pickle.loads(seleccion_serializado)
print(seleccion, recuperado, type(recuperado))



