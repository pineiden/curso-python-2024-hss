def texto_codificar(texto):
    txt_bytes=texto.encode('utf-8')    
    print("Texto codificado", txt_bytes)
    print(type(txt_bytes))
    decodificado=txt_bytes.decode('utf-8')
    print("Texto de-codificado", decodificado)    
    # ver los valores decimales de cada símbolo
    [print(char) for char in txt_bytes]
    # error!
    try:
        [print(ord(char)) for char in txt_bytes]
    except Exception as e:
        print("Hubo error %s" %e)
    # ver los valores binarios de cada símbolo
    # lo que ve el computador en memoria
    [print(bin(char)) for char in txt_bytes]


texto="Tres tristes tigres"
texto_codificar(texto)
texto2="Este cabrón me quitó el terreno"
texto_codificar(texto2)

import unicodedata

def info_unicode(char):
    value = ord(char)
    info = {
        "name":unicodedata.name(char),
        "category":unicodedata.category(char),
        "bidirectional":unicodedata.bidirectional(char), 
        "decomposition": unicodedata.decomposition(char),
        "hexadecimal": hex(value),
        "decimal": value,    
    }
    return info
