import csv
ruta_archivo = 'ejemplo_2.csv'
with open(ruta_archivo, 'r') as objeto_archivo:
    # se crea sniffer
    sniffer = csv.Sniffer()
    # se lee la primera serie de datos, leyendo 1024 caracteres
    sample = objeto_archivo.read(1024)
    dialect = sniffer.sniff(sample)
    # se muestra si tiene encabezado
    print("Tiene encabezado", sniffer.has_header(sample))
    # se vuelve a la posición 0
    objeto_archivo.seek(0)
    # se crea objeto lector csv
    reader =  csv.reader(objeto_archivo, dialect)
    for fila in reader:
        print(fila)
        print(list(map(type,fila)))
