import csv

def detectar(archivo, trozo=1024):
    ids = set()
    sniffer = csv.Sniffer()
    archivo.seek(0)  
    sample = archivo.read(trozo)
    print("muestra", sample)
    dialect = sniffer.sniff(sample)
    archivo.seek(0)
    reader =  csv.reader(archivo, dialect)     
    if sniffer.has_header(sample):
        header = next(reader)
    for fila in reader:
        idr = int(fila[0])
        ids.add(idr)
    archivo.seek(0)
    return ids, header, dialect
    
def new_id(ids, start=0):
    nid = start
    while nid in ids:
        nid += 1
    ids.add(nid)
    return nid

archivo_ruta = "ejemplo_escritura.csv"
with open(archivo_ruta,'a+') as archivo:
    ids, header, dialect = detectar(archivo, trozo=100)
    writer =  csv.writer(archivo, dialect, quotechar='"', 
        quoting=csv.QUOTE_NONNUMERIC)     
    nid = max(ids)
    while (nombre := input("nombre? ")) != "no":
         edad = int(input("edad "))
         nid = new_id(ids, nid)
         fila = [nid, nombre, edad]
         writer.writerow(fila) 
