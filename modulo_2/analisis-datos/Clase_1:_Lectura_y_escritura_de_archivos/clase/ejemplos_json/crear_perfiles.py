import json

archivo_ruta = "perfiles.json"
perfiles = []

keys = ["id", "nombre", "edad"]
idp = 0
while (nombre := input("nombre? ")) != "no":
     edad = int(input("edad "))
     perfil = dict(zip(keys, [idp, nombre, edad]))
     perfiles.append(perfil)
     idp += 1
     
archivo = open(archivo_ruta,'w')
# guardar lista como json
json.dump(perfiles, archivo, indent=4)
archivo.close()
