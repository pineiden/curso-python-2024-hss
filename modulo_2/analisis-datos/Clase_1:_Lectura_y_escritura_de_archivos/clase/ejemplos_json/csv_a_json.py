import csv
import json

ruta = "mes_temp.csv"
ruta_json = "mes_temp.json"

lista = []

with open(ruta,'r') as archivo:
    reader = csv.DictReader(archivo)
    for elem in reader:
        lista.append(elem)
        
        
print(lista)

with open(ruta_json, 'w', encoding='utf8') as archivo_json:
    # transformamos a un objeto json_data antes de guardar
    # nos asegura la codificacion
    json_data = json.dumps(lista, indent=2, ensure_ascii = False)
    # vemos el tipo de dato antes de guardar
    # es un string
    print(type(json_data))
    archivo_json.write(json_data)
