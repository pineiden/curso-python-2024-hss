(TeX-add-style-hook
 "01_clase"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("babel" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted"
    "babel"
    "fancyhdr")
   (LaTeX-add-labels
    "sec:org43886f6"
    "sec:org9e3a67e"
    "sec:orge98bf59"
    "sec:org92ce081"
    "sec:org0e95042"
    "sec:org2e63b6f"
    "sec:org870c0fb"
    "sec:orgec445ba"
    "sec:orgd8d6f76"
    "sec:orga7934cb"
    "sec:org0bf3121"
    "sec:org2e9a4cd"
    "sec:org80728b7"
    "sec:org1fe4904"
    "sec:orgd0e321b"
    "sec:org87d362d"
    "sec:org4ff67db"
    "sec:orgce47d91"
    "sec:org3b83977"
    "sec:orgd3b05d6"
    "sec:org80e6901"
    "sec:org8a48fad"
    "sec:org552d26e"
    "sec:org7d149a4"
    "sec:orga0e0141"
    "sec:orge79c989"
    "sec:orgf8c263a"
    "sec:org6e36ed6"
    "sec:org55d7dc1"
    "sec:org73153d4"
    "sec:org03372d1"
    "sec:org11b09ba"
    "sec:orgf90b929"
    "sec:org24a51da"
    "sec:org1f28138"
    "sec:org6ff19f5"
    "sec:org5ebe373"
    "sec:orgf3194f3"
    "sec:orge9a5ed6"
    "sec:org72a1559"
    "sec:org4491684"
    "sec:orge652044"
    "sec:org6173986"
    "sec:orgbcbd95f"
    "sec:org68152fd"
    "sec:orgb8b0018"))
 :latex)

