from gui import Galeria
from pathlib import Path
from rich import print

if __name__=="__main__":
    BASE = Path(__file__).parent.parent.parent
    gatitos_path = BASE/"galeria/gatitos"
    gui = Galeria(gatitos_path)
    print(gui)
    gui.run()
