from pathlib import Path
from tkinter import Tk, Canvas, PhotoImage, NW
from PIL import Image, ImageTk
from itertools import chain
from dataclasses import dataclass


@dataclass
class Galeria:
    path: Path 
    app: Tk 
    canvas: Canvas
    images:list[Image]
    w:int = 600
    h:int = 600
    active_index: int = 0

    def __init__(self, path:Path):
        self.app = Tk()
        self.build()
        self.widgets()

        self.images = []
        if path.exists():
            self.path=path
            self.load_images()
        else:
            print(f"No existe directorio {path}")

    def build(self):
        self.base_size = (self.w, self.h)
        shape = f"{self.w}x{self.h}"
        self.app.geometry(shape)

    def widgets(self):
        self.canvas = Canvas(self.app, bg="black")
        self.canvas.pack(
            anchor="nw", 
            fill="both", 
            expand=True)

    def load_images(self):
        extensiones = [
            "*.jpeg",
            "*.jpg",
            "*.png"
        ]
        images_path = list(
            chain.from_iterable([
                self.path.glob(patron) for patron in extensiones
            ])
        )
        self.images = [Image.open(path) for path in images_path]

    def show(self, index:int=0):
        if self.images:
            self.active_index = index
            image = self.images[index]
            self.active_image = image
            canvas_size = self.canvas.winfo_width(), self.canvas.winfo_height()
            if canvas_size==(1,1):
                canvas_size = self.base_size

            self.resized_image = image.resize(
                canvas_size, Image.LANCZOS)

            image_tk = ImageTk.PhotoImage(self.resized_image)
            
            self.image_id = self.canvas.create_image(
                0,0,image=image_tk, anchor="nw")

            self.canvas.image = image_tk
            self.canvas.bind("<Configure>", self.resize_image)
        else:
            print("No hay imagenes en la lista")

    def resize_image(self, event):
        self.show(self.active_index)

    def run(self):
        self.show()
        self.app.mainloop()


