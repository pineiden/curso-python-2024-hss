from tkinter import Tk, Canvas, PhotoImage, NW
from pathlib import Path
from PIL import Image, ImageTk
import itertools


"""
Ejemplo tomado de:
https://www.youtube.com/watch?v=4ehHuDDH-uc

Uso de Canvas:
https://www.tutorialspoint.com/python/tk_canvas.htm
"""
class Galeria:
    path:Path
    app:Tk
    canvas:Canvas
    images:list[Image]
    w:int=600
    h:int=600
    active_index:int=0

    def __init__(self, path:Path):
        self.app = Tk()
        self.build()
        self.widgets()
        self.images = []
        if path.exists():
            self.path = path
            self.load_images()
        else:
            print(f"No existe directorio {path}")

    def build(self):
        self.base_size = (self.w,self.h)
        shape = f"{self.w}x{self.h}"
        self.app.geometry(shape)

    def widgets(self):
        canvas = Canvas(self.app, bg="black")
        canvas.pack(anchor="nw", fill="both", expand=True)
        self.canvas = canvas

    def load_images(self):
        extensions = ["*.png", "*.jpg","*.jpeg", "*.bmp", "*.gif"]
        images = list(
            itertools.chain.from_iterable(
                (self.path.glob(pattern) for pattern in extensions)
            )
        )
        self.images = [Image.open(path) for path in images]

    def show(self, index:int=0):
        if self.images:
            self.active_index = index
            image = self.images[index]
            self.active_image = image
            canvas_size = self.canvas.winfo_width(), self.canvas.winfo_height()
            if canvas_size==(1,1):
                canvas_size = self.base_size
            print("Canvas size",canvas_size)
            self.resized_image = image.resize(canvas_size,Image.LANCZOS)
            
            print(self.resized_image.size)
            #print("Resized->",resized_image)

            image_tk = ImageTk.PhotoImage(self.resized_image)
            self.image_id = self.canvas.create_image(0,0,image=image_tk, anchor="nw")
            self.canvas.pack(anchor=NW, fill="both", expand=1)
            self.canvas.image = image_tk
            self.canvas.bind("<Configure>", self.resize_image)
        else:
            print("Images is empty list", self.images)

    def resize_image(self, event):
        #print(self.active_image)
        width, height = self.canvas.winfo_width(), self.canvas.winfo_height()
        #print(event.width, event.height)
        #print(width, height)
        #self.image = ImageTk.PhotoImage(self.image.resize((width, height)))
        #self.canvas.itemconfig(self.image_id, image=self.image)
        self.show(self.active_index)

    def run(self):
        self.show()
        self.app.mainloop()

if __name__=="__main__":
    BASE = Path(__file__).parent.parent.parent
    gatitos_path = BASE/"galeria/gatitos"
    gui = Galeria(gatitos_path)
    gui.run()
