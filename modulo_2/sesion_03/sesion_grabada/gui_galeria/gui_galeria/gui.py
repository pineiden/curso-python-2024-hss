from tkinter import Tk, Canvas, PhotoImage, NW
from pathlib import Path
from PIL import Image, ImageTk
from itertools import chain
from dataclasses import dataclass
from rich import print


@dataclass
class Galeria:
    path:Path 
    app:Tk 
    canvas:Canvas 
    images:list[Image]
    width:int = 600
    height:int = 600
    active_index:int = 0

    def __init__(self, path:Path):
        self.app = Tk()
        self.build()
        self.widgets()
        self.images = []
        if path.exists():
            self.path = path 
            self.load_images()
        else:
            print(f"No existe directorio {path}")

    def build(self):
        self.base_size = (self.width, self.height)
        shape = f"{self.width}x{self.height}"
        self.app.geometry(shape)

    def widgets(self):
        canvas = Canvas(self.app, bg="black")
        canvas.pack(anchor="nw", fill="both", expand=True)
        self.canvas = canvas

    def load_images(self):
        extensiones = ["*.png", "*.jpg","*.jpeg", "*.bmp", "*.gif"]
        images = list(
            chain.from_iterable([
                self.path.glob(patron) for patron in extensiones
            ])
        )
        self.images = [Image.open(path) for path in images]


    def show(self, index:int=0):
        if self.images:
            image = self.images[index]
            self.active_index = index
            self.active_image = image
            canvas_size = self.canvas.winfo_width(), self.canvas.winfo_height()
            if canvas_size==(1,1):
                canvas_size = self.base_size
            print("Canvas size", canvas_size)
            self.resized_image = image.resize(canvas_size,
                                              Image.LANCZOS)

            image_tk = ImageTk.PhotoImage(self.resized_image)
            self.image_id = self.canvas.create_image(
                0,0,
                image=image_tk,
                anchor="nw")
            self.canvas.pack(anchor=NW, fill="both", expand=1)
            self.canvas.image = image_tk
            self.canvas.bind("<Configure>", self.resize_image)
        else:
            print("El directorio de imágenes está vacio")

    def resize_image(self, event):
        self.show(self.active_index)

    def run(self):
        self.show()
        self.app.mainloop()


if __name__=="__main__":
    BASE = Path(__file__).parent.parent.parent
    gatitos_path = BASE / "galeria/gatitos"
    gui = Galeria(gatitos_path)
    print(gui)
    gui.run()
