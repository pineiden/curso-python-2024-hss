#+TITLE: Interfaces II y POO usando herencia.

* Interfaces II

En esta ocasión crearemos una interfaz que muestra una imagen o
varias, pudiendo cambiarlas, tal cual una galería de imágenes pero
simplificada.

La idea es que en las sesiones próximas vayamos construyendo una
interfáz gráfica que permita representar diferentes figuras y tener
cierto control de la información que se está dibujandointerfaz

En primera instancia:

- Tener un grupo de imágenes 
- Activar una ventana con Tk 
- Activar un widget canvas en Tk GUI.


* Introducción a Herencia.

Sabemos que la creación de clases nos permite generar *abstracciones*
sobre como describimos una situación y resolvemos los problemas.

Cuando creamos una clase tenémos, básicamente:

- atributos 
- acciones (funciones asociadas) 

Las funciones asociadas pueden ser de cualquier subtipo de funciones
posibles en python.

Los atributos pueden ser asignados directo de objectos o mediante
decoración de @property.

En esta sesión realizaremos una introducción al concepto de herencia,
cómo se define y para que se podría usar mediante una serie de
problemas sencillos de comprender.

* Estudiar el objeto base.

En python tenemos un objeto base *object* que contiene los elementos
principales y la estructura de una clase, toda clase que se cree en
python hereda de *object*, es decir toda instancia de clase también
será de tipo *object*.

** Realizamos una inspección

#+begin_src bash
pip install -q rich
#+end_src

#+RESULTS:

#+begin_src python :results output
import inspect

print(inspect.getmembers(object))
print(inspect.isclass(object))
#+end_src

#+RESULTS:
: [('__class__', <class 'type'>), ('__delattr__', <slot wrapper '__delattr__' of 'object' objects>), ('__dir__', <method '__dir__' of 'object' objects>), ('__doc__', 'The base class of the class hierarchy.\n\nWhen called, it accepts no arguments and returns a new featureless\ninstance that has no instance attributes and cannot be given any.\n'), ('__eq__', <slot wrapper '__eq__' of 'object' objects>), ('__format__', <method '__format__' of 'object' objects>), ('__ge__', <slot wrapper '__ge__' of 'object' objects>), ('__getattribute__', <slot wrapper '__getattribute__' of 'object' objects>), ('__getstate__', <method '__getstate__' of 'object' objects>), ('__gt__', <slot wrapper '__gt__' of 'object' objects>), ('__hash__', <slot wrapper '__hash__' of 'object' objects>), ('__init__', <slot wrapper '__init__' of 'object' objects>), ('__init_subclass__', <built-in method __init_subclass__ of type object at 0x9643e0>), ('__le__', <slot wrapper '__le__' of 'object' objects>), ('__lt__', <slot wrapper '__lt__' of 'object' objects>), ('__ne__', <slot wrapper '__ne__' of 'object' objects>), ('__new__', <built-in method __new__ of type object at 0x9643e0>), ('__reduce__', <method '__reduce__' of 'object' objects>), ('__reduce_ex__', <method '__reduce_ex__' of 'object' objects>), ('__repr__', <slot wrapper '__repr__' of 'object' objects>), ('__setattr__', <slot wrapper '__setattr__' of 'object' objects>), ('__sizeof__', <method '__sizeof__' of 'object' objects>), ('__str__', <slot wrapper '__str__' of 'object' objects>), ('__subclasshook__', <built-in method __subclasshook__ of type object at 0x9643e0>)]
: True
: 


** La Herencia.

Cuando necesitamos definir diferentes tipos de objetos pero que tengan
en *común* alguna acción o atributo, es posible ordenar una estructura
de /jerarquías/ que permita crear clases lo más genéricas posibles con
el fin que al crear otras clases se *hereden* y en la clase misma se
definan particularidades.

** Enumeraciones.

Las enumeraciones son un típico específico de clases que pueden
describir un conjunto limitado de elementos u objetos. Son una
herramienta de utilidad para crear flujos de programación legibles  y
flexibles. 

Se crean heredando del módulo *enum* y pueden ser de tipo IntEnum,
StrEnum o por defecto (numérico)

https://docs.python.org/3/library/enum.html

La creación de de una clase Enum implica heredar del objeto base que
permite manejar enumeraciones. 

En el caso de Enum básico cada objeto del enum tiene asignado un número

#+begin_src python
from enum import Enum 

class Semaforo(Enum):
    ROJO = 1 
    VERDE = 2
    AMARILLO = 3
#+end_src

Asimismo será posible definir específicamente que sean valores
enteros.

#+begin_src python
class enum import IntEnum

class ColorCMYK(IntEnum):
    CYAN = 0
    MAGENTA = 1
    AMARILLO = 2
    NEGRO = 2
#+end_src

Cómo también usar directamente strings:

#+begin_src python
class enum import StrEnum

class TipoNombre(StrEnum):
    NOMBRE = "nombre"
    APELLIDO = "apellido"
#+end_src

** Clases abstractas e interfaces


El uso de *clases abstractas* permite definir de manera /abstractas/
una clase padre que es una especie de /declaración de principios/ o
/requerimientos/ que pedirá que cada clase hija deba implementar los
métodos que requiere.

#+begin_src python
from abc import ABC


class Base(ABC):
    @abstractmethod
    def calculo(self)->float:


class Heredera(Base):
    def calculo(self)->float:
        return 100_000.100
#+end_src


https://docs.python.org/3/library/abc.html


** Un ejemplo simple: Materiales

Podemos definir que los materiales se pueden encontrar en tres
estados dependiendo la temperatura y presión:

- sólido
- líquido 
- gaseoso

Obviando otros estados inestables, consideramos la termodinámica
clásica.

Podemos decorar la clase padre, definiendola además como clase
*abstract* que hara posible crear clases hijas, pero no crear
instancias de la clase Material porque no tiene la implementación de
los métodos requeridos.

#+begin_src python
from dataclasses import dataclass
from enum import IntEnum
from abc import ABC

class Estado(IntEnum):
    SOLIDO=0
    LIQUIDO=1
    GAS=2

@dataclass
class Material(ABC):
    temperatura:float
    presion:float = 1.0

    @property
    @abstractmethod
    def estado(self)->Estado:
        ...
#+end_src

Luego, podemos considerar que por defecto se trabaja con 1 atmófera
para simplificar. 

https://es.wikipedia.org/wiki/Punto_triple#/media/Archivo:Phase-diag_es.svg

Creamos la clase Agua:

#+begin_src python
class Agua(Material):
    @property
    def estado(self)->Estado:
        t = self.temperatura
        if t<0.0:
            return Estado.SOLIDO
        elif 0.0<=t<=100:
            return Estado.LIQUIDO
        else t>100:
            return Estado.GAS
#+end_src

Así como otro material como el Mercurio

#+begin_src python
class Mercurio(Material):
    @property
    def estado(self)->Estado:
        t = self.temperatura
        if t<-38.83:
            return Estado.SOLIDO
        elif -38.83<=t<=356.73:
            return Estado.LIQUIDO
        else t>356.73:
            return Estado.GAS
#+end_src

Al ser cualquier Material un dataclass, las clases heredan esta
característica. Solo cambia el modo en que se entrega la propiedad del estado. 

¿Existirá alguna otra forma de definir los estados? Observando los
patrones de cambio de estado, es posible ver que es necesario conocer
3 valores de temperatura límite para cierto nivel de presión. Por lo
que estos valores también pueden ser atributos. Y al definir cada
clase hija se determinen como atributos ya definidos y no escritos en
la función.

¿Cómo se podrían definir estas clases si estuviesemos en Marte o la
Luna o Venus?

** Volvamos a la geometría.

En geometría 2D los polígonos son /figuras/ que presentan
características como:

- regular o no
- cantidad de lados
- ubicación del centro
  
Un polígono regular se puede determinar por la cantidad de lados, el
centro y el apotegma determinado por el radio del círculo que lo
inscribe.

En cambio, un polígono regular se determina por una lista de puntos
ubicados en el plano 2d.

Asimismo, un polígono irregular definido por la lista de puntos de sus
vértices puede llegar a ser convexo o no (cóncavo). Entonces hay modos
de determinar esto.

Considerando además que el área de cada polígono se puede determinar
por la suma del área de sus triángulos.

En términos de programación, antes de escribir código será necesario
diseñar esquemáticamente que se está programando.

#+begin_src plantuml :file geom.svg
abstract class Figura {
bool regular
bool convexo
float area()
float poligono()
}

class PoligonoRegular {
bool regular=True		
}


class Triangulo {
  a:float
  b:float
  c:float
  Array[] from_points() -> Triangulo
}

Figura <|-- PoligonoRegular
Figura <|-- Triangulo
#+end_src

#+RESULTS:
[[file:geom.svg]]

** Uso de super

Cuando necesitamos *sobreescribir* el comportamiento de una función ya
definida, podemos simplemente escribir la misma función con su fimar
correspondiente.

Si necesitamos activar todo lo que hace la clase padre pero también
agregar nuevas características, se puede user *super* que es una
función que llama exactamente el método de la clase padre y luego se
continúa con lo que se requiera en la clase hija.

#+begin_src python
import statistics

class Base:
    valores:list[float]
    
    def __init__(self, valores:list[float]):
        self.valores = valores

    def estadisticos(self)->dict[str, float]:
        if valores:
            return {"promedio":sum(valores)/len(valores)}
        else:
            return {}

class Heredera(Base):
    proporcion:float

    def __init__(self, valores:list[float], proporcion:float):
        super().__init__(valores)
        self.proporcion = proporcion
    
    def estadisticos(self):
        resultado = super().estadisticos()
        resultado.update({"desv_estandar":statistics.stdev(self.valores)})
        return resultado
#+end_src

