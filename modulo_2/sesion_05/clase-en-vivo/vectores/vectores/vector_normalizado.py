from dataclasses import dataclass
from typing import TypeVar
import math
from .vector import Vector

VectorNormalizado = TypeVar("VectorNormalizado")

@dataclass
class VectorNormalizado:
    magnitud:float 
    direccion:Vector
    
    def __post_init__(self, *args, **kwargs):
        m, v  = self.direccion.par
        self.direccion = v
        self.magnitud *= m

    
    @property
    def vector(self):
        return self.direccion*self.magnitud

    @property
    def par(self):
        return self.magnitud, self.direccion


    def __mul__(self, value:float)->VectorNormalizado:
        magnitud = self.magnitud * value 
        return VectorNormalizado(magnitud, self.direccion)

    def __truediv__(self, value:float)->VectorNormalizado:
        magnitud = self.magnitud / value 
        return VectorNormalizado(magnitud, self.direccion)

    def __add__(self, v2:VectorNormalizado):
        suma_vector =  self.vector + v2.vector 
        magnitud, direccion = suma_vector.par
        return VectorNormalizado(magnitud, direccion)

    def __sub__(self, v2:VectorNormalizado):
        subs_vector =  self.vector - v2.vector 
        magnitud, direccion = subs_vector.par
        return VectorNormalizado(magnitud, direccion)
