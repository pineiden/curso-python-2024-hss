from dataclasses import dataclass
from typing import TypeVar
from .vector_normalizado import VectorNormalizado
from .posicion import Posicion
from .tiempo import Tiempo

Velocidad = TypeVar("Velocidad")


@dataclass
class Velocidad(VectorNormalizado):
    unidad_medida = "metro/segundo"

    @property
    def um(self):
        return "m/s"

    def __mul__(self, tiempo:Tiempo)->Posicion:
        """
        Transform Velocidad*Tiempo->Direccion
        """
        magnitud = self.magnitud * tiempo 
        return Posicion(magnitud, self.direccion)

    def __add__(self, v2:Velocidad)->Velocidad:
        v1 = VectorNormalizado(1,self.vector)
        v2 = VectorNormalizado(1,v2.vector)
        v3 = v1 + v2
        magnitud, direccion = v3.par
        return Velocidad(magnitud, direccion)

    def ecuacion(self, 
                 posicion_t:Posicion, 
                 dt:Tiempo):
        pn = posicion_t + self * float(dt) 
        magnitud, direccion = pn.par
        return Posicion(magnitud, direccion)
