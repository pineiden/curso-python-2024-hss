from dataclasses import dataclass, field
import math
from typing import TypeVar

from .vector_normalizado import VectorNormalizado
from .aceleracion import Aceleracion
      

@dataclass
class Fuerza(VectorNormalizado):
    unidad_medida = "Newton"
    """
    F = m * a
    """
    def aceleracion(self, masa:float):
        if masa>0:
            return Aceleracion(
                self.magnitud/masa, 
                self.direccion)
        else:
            raise Exception("La masa no puede ser negativa")

    @property
    def um(self):
        return "N"
