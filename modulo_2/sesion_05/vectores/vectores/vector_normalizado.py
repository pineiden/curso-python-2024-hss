from dataclasses import dataclass
from typing import TypeVar
import math

from .vector import Vector, EPSILON

VectorNormalizado = TypeVar("Direccion")

@dataclass
class VectorNormalizado:
    magnitud:float 
    direccion:Vector

    def __post_init__(self, *args, **kwargs):
        m, v = self.direccion.par()
        self.direccion = v
        self.magnitud *= m

    @property
    def par(self):
        return self.magnitud, self.direccion

    def __eq__(self, v:VectorNormalizado)->bool:
        return (
            abs(self.magnitud-v.magnitud)<=EPSILON and 
            self.direccion==v.direccion
        )

    @property
    def vector(self)->Vector:
        return self.direccion * self.magnitud


    def __mul__(self, value:float):
        magnitud = self.magnitud * value 
        return VectorNormalizado(magnitud, self.direccion)


    def __truediv__(self, value:float):
        magnitud = self.magnitud / value 
        return VectorNormalizado(magnitud, self.direccion)

    def __add__(self, v2:Vector):
        suma_vector =  self.vector + v2.vector 
        magnitud, direccion = suma_vector.par()
        return VectorNormalizado(magnitud, direccion)

    def __sub__(self, v2:Vector):
        subs_vector =  self.vector - v2.vector 
        magnitud, direccion = subs_vector.par()
        return VectorNormalizado(magnitud, direccion)



