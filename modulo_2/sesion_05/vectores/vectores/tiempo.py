from enum import IntEnum
from dataclasses import dataclass, field

class UnidadTiempo(IntEnum):
    MILISEG = 0
    SEGUNDO = 1
    MINUTO = 2
    HORA = 3
    DIA = 4

@dataclass
class Tiempo(float):
    value:value = field(default=0.0)
    unidad:UnidadTiempo = field(default=UnidadTiempo.SEGUNDO)
    
    def __post_init__(self):
        if self.value<0:
            raise ValueError("El valor debe ser 0 o positivo")
        self.value = float(self.value)

    @property
    def total_segundos(self)->float:
        match self.unidad:
            case UnidadTiempo.MILISEG:
                return self.value/1000
            case UnidadTiempo.SEGUNDO:
                return self.value
            case UnidadTiempo.MINUTO:
                return self.value * 60
            case UnidadTiempo.HORA:
                return self.value * 60 * 60
            case _:
                return self.value     

    def __float__(self):
        return float(self.total_segundos)

    def __int__(self):
        return int(self.total_segundos)

    @property
    def um(self):
        match self.unidad:
            case UnidadTiempo.MILISEG:
                return "ms"
            case UnidadTiempo.SEGUNDO:
                return "s"
            case UnidadTiempo.MINUTO:
                return "min"
            case UnidadTiempo.HORA:
                return "hr"
            case _:
                return "s"     

