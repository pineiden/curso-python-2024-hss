from dataclasses import dataclass
from typing import TypeVar
import math

EPSILON = .1e-6

Vector = TypeVar("Direccion")


@dataclass
class Vector:
    x:float
    y:float
    z:float=0

    @property
    def items(self):
        return (self.x,self.y,self.z)

    @property
    def tamaño(self):
        return math.sqrt(self.x**2+self.y**2+self.z**2)
    
    def __add__(self, v2:Vector)->Vector:
        params = {
            "x": self.x + v2.x,
            "y": self.y + v2.y,
            "z": self.z + v2.z
        }
        return Vector(**params)


    def __sub__(self, v2:Vector)->Vector:
        params = {
            "x": self.x - v2.x,
            "y": self.y - v2.y,
            "z": self.z - v2.z
        }
        return Vector(**params)


    def __mul__(self, value:float)->Vector:
        x = self.x * value 
        y = self.y * value 
        z = self.z * value
        return Vector(x,y,z)

    
    def __pow__(self, value:float)->Vector:
        x = self.x**value 
        y = self.y**value 
        z = self.z**value
        return Vector(x,y,z)


    def __truediv__(self, value:float)->Vector:
        x = self.x/value 
        y = self.y/value 
        z = self.z/value
        return Vector(x,y,z)

    def distancia(self, p2:Vector)->(float, tuple[int]):
        p_result = self - p2 
        v2 = Vector(*p_result.items)
        signos = tuple(math.copysign(1, value) for value in p_result.items)
        return v2.tamaño, signos

    def normalizar(self, r:float)->Vector:
        if r>0:
            values = [v/r for v in self.items]
            return Vector(*values)
        else:
            return Vector(0,0,0)

    def par(self):
        m = self.tamaño
        return (m, self.normalizar(m))

    
