from .vector_normalizado import VectorNormalizado
from dataclasses import dataclass


@dataclass
class Posicion(VectorNormalizado):
    unidad_medida = "metro"

    @property
    def um(self):
        return "m"

