from dataclasses import dataclass, field
from typing import TypeVar
from .vector_normalizado import VectorNormalizado
from .velocidad import Velocidad
from .tiempo import Tiempo

Aceleracion = TypeVar("Aceleracion")


@dataclass
class Aceleracion(VectorNormalizado):
    unidad_medidad = "metro/segundo^2"

    @property
    def um(self):
        return "m/s^2"

    def __add__(self, v2:Velocidad)->Velocidad:
        v1 = VectorNormalizado(1,self.vector)
        v2 = VectorNormalizado(1,v2.vector)
        v3 = v1 + v2
        magnitud, direccion = v3.par
        return Aceleracion(magnitud, direccion)

    def __mul__(self, value:float):
        """
        Transform Aceleracion*Tiempo->Velocidad
        """
        magnitud = self.magnitud * value 
        return Aceleracion(magnitud, self.direccion)

    def ecuacion(self, 
                  velocidad_0:Velocidad, 
                  dt: Tiempo)->Velocidad:
        vn = velocidad_0 + self * float(dt) 
        magnitud, direccion = vn.par
        return Velocidad(magnitud, direccion)
