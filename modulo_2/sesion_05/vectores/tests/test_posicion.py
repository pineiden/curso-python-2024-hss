from vectores import Vector,Posicion

def test_posicion():
    x = 3
    y = 4
    v = Vector(3,4)
    vn = Posicion(1,v)
    m = v.tamaño

    assert m==vn.magnitud



def test_posicion_to_vector():
    x = 3
    y = 4
    v = Vector(x,y)
    vn = Posicion(1,v)
    m = v.tamaño

    assert v==vn.vector

def test_posicion_multiplica():
    x = 6
    y = 8
    v = Vector(x,y)
    vn = Posicion(1,v)
    vn2 = vn/2
    assert vn2.magnitud == 5


def test_posicion_suma():
    x = 3
    y = 4
    v = Vector(x,y)
    vn1 = Posicion(1,v)

    x = 3
    y = 4
    v = Vector(x,y)
    vn2 = Posicion(1,v)

    vn3 = vn1 + vn2
    assert vn3 == vn1*2



def test_posicion_resta():
    x = 6
    y = 8
    v = Vector(x,y)
    vn1 = Posicion(1,v)

    x = 3
    y = 4
    v = Vector(x,y)
    vn2 = Posicion(1,v)

    vn3 = vn1 - vn2
    assert vn3 == vn2
