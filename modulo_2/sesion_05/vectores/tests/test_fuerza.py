from vectores import Vector, Aceleracion, Fuerza


def test_fuerza_m():
    x = 3
    y = 4
    v = Vector(x,y)
    fn = Fuerza(1,v)
    masa = 5
    acel = fn.aceleracion(masa)
    assert acel.magnitud==1
