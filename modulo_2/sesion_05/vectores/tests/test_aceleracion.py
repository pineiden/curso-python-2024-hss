from vectores import Vector,Posicion,Tiempo,Velocidad,Aceleracion

def test_aceleracion_mul_tiempo():
    x = 6
    y = 8
    v = Vector(x,y)
    vn = Aceleracion(1,v)
    m, dir = vn.par
    magn = m * 1.5
    ts = Tiempo(1.5)
    pos = Aceleracion(magn, dir)
    vn2 = vn*ts
    assert vn2 == pos



def test_aceleracion_sum_result_velocidad():
    x = 3
    y = 4
    v = Vector(x,y)
    an1 = Aceleracion(1,v)

    x = 3
    y = 4
    v = Vector(x,y)
    an2 = Aceleracion(1,v)

    an3 = an1 + an2
    assert an3.magnitud==10
    assert an3.um == "m/s^2"


def test_aceleracion_ecuacion():
    x = 6
    y = 8
    v = Vector(x,y)
    an = Aceleracion(1,v)
    m, dir = an.par
    magn = m * 1.5
    ts = Tiempo(1.5)
    vel = Velocidad(magn, dir)
    v_next = an.ecuacion(vel, ts)
    assert isinstance(v_next, Velocidad)
    assert v_next.magnitud==30.0
