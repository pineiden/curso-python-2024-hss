from vectores import Vector

def test_vector_items():
    x = 1.0
    y = 2.5
    
    vector = Vector(x,y)
    assert vector.items==(x,y,0)


def test_vector_suma():
    x = 1.0
    y = 2.5
    
    vector1 = Vector(x,y)

    x = 1.0
    y = 3.5
    
    vector2 = Vector(x,y)

    vector3 = Vector(2,6)
    
    assert vector1+vector2==vector3


def test_vector_resta():
    x = 1.0
    y = 2.5
    
    vector1 = Vector(x,y)

    x = 1.0
    y = 3.5
    
    vector2 = Vector(x,y)

    vector3 = Vector(0,-1)
    
    assert vector1-vector2==vector3



def test_vector_escalar():
    x = 1.0
    y = 2.5
    
    vector1 = Vector(x,y) * 2
    vector2 = Vector(2,5)
    
    assert vector1==vector2



def test_vector_pow():
    x = 1.0
    y = 2
    
    vector1 = Vector(x,y) ** 2
    vector2 = Vector(1,4)
    
    assert vector1==vector2



def test_vector_division():
    x = 3.0
    y = 9
    
    vector1 = Vector(x,y) / 3
    vector2 = Vector(1,3)
    
    assert vector1==vector2



def test_vector_distancia():
    x = 4
    y = 5
    vector1 = Vector(x,y)
    x = 1
    y = 1
    vector2 = Vector(x,y)
    medida, signos = vector1.distancia(vector2)
    assert medida==5
    assert signos==(1,1,1)
    medida, signos = vector2.distancia(vector1)
    assert medida==5
    assert signos==(-1,-1,1)
