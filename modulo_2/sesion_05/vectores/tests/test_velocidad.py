from vectores import Vector,Posicion,Tiempo,Velocidad

def test_velocidad_mul_tiempo():
    x = 6
    y = 8
    v = Vector(x,y)
    vn = Velocidad(1,v)
    m, dir = vn.par
    magn = m * 1.5
    ts = Tiempo(1.5)
    pos = Posicion(magn, dir)
    vn2 = vn*ts
    assert vn2 == pos



def test_velocidad_sum_result_velocidad():
    x = 3
    y = 4
    v = Vector(x,y)
    vn1 = Velocidad(1,v)

    x = 3
    y = 4
    v = Vector(x,y)
    vn2 = Velocidad(1,v)

    vn3 = vn1 + vn2
    assert vn3.magnitud==10
    assert vn3.um == "m/s"


def test_velocidad_ecuacion():
    x = 6
    y = 8
    v = Vector(x,y)
    vn = Velocidad(1,v)
    m, dir = vn.par
    magn = m * 1.5
    ts = Tiempo(1.5)
    pos = Posicion(magn, dir)
    v_next = vn.ecuacion(pos, ts)
    assert isinstance(v_next, Posicion)
    assert v_next.magnitud==30.0
