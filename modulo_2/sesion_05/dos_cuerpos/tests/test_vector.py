from dos_cuerpos import Vector, Posicion


def test_vector_sum():
    p1 = Posicion(0,0,0)
    v1 = Vector(10, p1)
    p2 = Posicion(1,1,1)
    v2 = Vector(10, p2)
    v3 = Vector(10, p2)
    vsum = v1+v2
    assert vsum == v3



def test_vector_mul():
    p1 = Posicion(1,0,0)
    v1 = Vector(10, p1)
    vmul = v1 * (1/10)
    assert vmul.magnitud == 1



def test_vector_div():
    """Test vector
    al dividir por 5 la magnitud ==1
    """
    p1 = Posicion(3,4,0)
    v2 = Vector(1, p1)
    v3 = v2 / 5
    assert v3.magnitud == 1


def test_vector_start():
    """Test vector
    initizalizacion debe tener |direccion|==1
    """
    p1 = Posicion(3,4,0)
    v2 = Vector(10, p1)
    norma = v2.direccion.magnitud() 
    assert norma == 1 and v2.magnitud==50


def test_vector_restar():
    p1 = Posicion(0,0,0)
    v1 = Vector(10, p1)
    p2 = Posicion(1,1,1)
    v2 = Vector(10, p2)
    v3 = Vector(10, p2)
    vr = v2-v1
    assert vr == v3
