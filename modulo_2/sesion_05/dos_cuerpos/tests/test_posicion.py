from dos_cuerpos import Posicion

def test_posicion_suma():
    p1 = Posicion(0,0,0)
    p2 = Posicion(1,1,1)
    assert p1+p2==p2


def test_posicion_resta_mul():
    p1 = Posicion(0,0,0)
    p2 = Posicion(1,1,1)
    assert p1-p2==p2*(-1)

def test_posicion_div():
    p2 = Posicion(12,16,0)
    p2_div = p2/4
    p1 = Posicion(3,4,0)
    assert p2_div==p1

def test_posicion_magnitud():
    p1 = Posicion(3,4,0)
    m = p1.magnitud()
    assert m==5

def test_posicion_pow():
    p1 = Posicion(3,4,0)
    p1_pow = p1**2
    assert p1_pow.x==9 and p1_pow.y==16 and p1_pow.z==0


def test_posicion_normalizar():
    p1 = Posicion(3,-4,0)
    m,signos = p1.distancia(Posicion(0,0,0))
    assert m==5 and signos==(1,-1,1)
