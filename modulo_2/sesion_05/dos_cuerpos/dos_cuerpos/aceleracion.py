from dataclasses import dataclass, field
from typing import TypeVar

from .constantes import EPSILON
from .direccion import Direccion
from .velocidad import Velocidad
from .vector import Vector
from .tiempo import Tiempo

Aceleracion = TypeVar("Aceleracion")


@dataclass
class Aceleracion(Vector):
    def __mul__(self, value:float):
        """
        Transform Aceleracion*Tiempo->Velocidad
        """
        magnitud = self.magnitud * value 
        return Velocidad(magnitud, self.direccion)

    def ecuacion(self, 
                  velocidad_0:Velocidad, 
                  dt: Tiempo)->Velocidad:
        m, p = self.magnitud, self.direccion
        aceleracion_1 = Aceleracion(m,p)
        return velocidad_0 + aceleracion_1 * float(dt)
