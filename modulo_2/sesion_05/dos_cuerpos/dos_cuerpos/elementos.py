from .base import ObjetoCeleste
from dataclasses import dataclass, field
from .constantes import Rt, Rl, Mt, Ml, D_t_l
from .direccion import Direccion

@dataclass
class Tierra(ObjetoCeleste):
    name:str = field(default="Tierra")
    radio:float = field(default=Rt)
    masa:float = field(default=Mt)
    posicion:Direccion = field(default=Direccion(0,0,0))


@dataclass
class Luna(ObjetoCeleste):
    name:str = field(default="Luna")
    radio:float = field(default=Rl)
    masa:float = field(default=Ml)
    posicion:Direccion = field(default=Direccion(D_t_l,0,0))
