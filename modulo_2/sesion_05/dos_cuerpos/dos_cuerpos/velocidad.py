from dataclasses import dataclass, field
from typing import TypeVar

from .constantes import EPSILON
from .direccion import Direccion
from .vector import Vector
from .tiempo import Tiempo

Velocidad = TypeVar("Velocidad")


@dataclass
class Velocidad(Vector):

    def __mul__(self, value:float)->Vector:
        """
        Transform Velocidad*Tiempo->Direccion
        """
        magnitud = self.magnitud * value 
        return Vector(magnitud, self.direccion)

    def __add__(self, v2:Velocidad):
        suma_velocidad =  self.vector + v2.vector 
        magnitud, direccion = suma_velocidad.par()
        return Velocidad(magnitud, direccion)

    def ecuacion(self, 
                 posicion_0:Direccion, 
                 dt:Tiempo):
        m, p = self.magnitud, self.direccion
        velocidad_1 = Velocidad(m,p)
        posicion_anterior = Vector(1, posicion_0)
        # vector + vector
        return posicion_anterior + velocidad_1 * float(dt) 
