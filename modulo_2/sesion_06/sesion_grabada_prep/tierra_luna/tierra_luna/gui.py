from gui_base.gui import GUIBase
import tkinter as tk
from PIL import Image, ImageTk
import math
from pathlib import Path
from dataclasses import dataclass
from rich import print
from vector_fisico.constantes import G,Rt,Rl,Dtl,Mt,Ml
from vector_fisico.fuerza import Fuerza, FuerzaGravitacional
from vector_fisico.posicion import Posicion
from vector_fisico.velocidad import Velocidad
from vector_fisico.aceleracion import Aceleracion
from vectores import VectorNormalizado, Vector


@dataclass
class Punto:
    x:float 
    y:float 
    
    def par(self):
        return self.x, self.y

BASE = Path(__file__).parent.resolve() / "assets"

class ObjetoCeleste:
    def __init__(self, 
                 canvas, 
                 image_path, 
                 x,
                 y, 
                 scale):
        self.canvas = canvas
        self.image = Image.open(str(image_path))
        self.scale = scale

        #breakpoint()
        self.image = self.image.resize(
            (
                int(self.image.width*self.scale),
                int(self.image.height*self.scale)
            ),
            Image.LANCZOS
        )
        self.image = ImageTk.PhotoImage(self.image)
        self.cid = self.canvas.create_image(
            x,
            y,
            image=self.image)
        self.x = x
        self.y = y

    def move_to(self, x,y):
        self.x = int(x)
        self.y = int(y)
        self.canvas.coords(self.cid, self.x, self.y)

class Tierra(ObjetoCeleste):
    def __init__(self, canvas, x, y, scale=1):
        super().__init__(canvas, BASE/"earth.png",x,y,scale)


class Luna(ObjetoCeleste):
    def __init__(self, canvas, x, y, scale=1):
        super().__init__(canvas, BASE/"moon.png",x,y,scale)
        velocidad_magnitud = 4.63 # m/s
        vector  = Vector(0,1,0)
        self.velocidad = Velocidad(velocidad_magnitud, vector)
        self.posicion_tierra = Posicion(1, Vector(0,0,0))
        self.posicion_luna = Posicion(1, Vector(Dtl,0,0))
        self.fuerza = FuerzaGravitacional(self.posicion_tierra,
                                          self.posicion_luna)

    def mover_en_torno_a_tierra(self, 
                                centro_x, 
                                centro_y, 
                                radio, 
                                angulo):
        """
        calcular las posicione x,y,z basados en la fuerza
        """
        dt = 300
        aceleracion = self.fuerza.aceleracion_luna()
        velocidad = aceleracion.ecuacion(self.velocidad, dt)
        # nueva velocidad
        self.velocidad = velocidad
        posicion = velocidad.ecuacion(self.posicion_luna, dt)
        # update fuerza on new posicion
        self.fuerza = FuerzaGravitacional(self.posicion_tierra,
                                          posicion, 
                                          velocidad)
        ###
        x = centro_x + posicion.magnitud*posicion.vector.x
        y = centro_y + posicion.magnitud*posicion.vector.y
        print(x,y)
        return Punto(x,y)

# limite max distancia
Max_tl = Dtl + 2*Rl
import math

def planet_scale(R:float):
    pendiente = Max_tl
    return R/pendiente

@dataclass
class SistemaTierraLuna(GUIBase):

    def __init__(self, width:int=800, height:int=800):
        self.width = width
        self.height = height
        escala_tierra  = planet_scale(Rt)*3
        escala_luna  = planet_scale(Rl)*3
        self.luna = None
        self.tierra = None
        self.escala_luna = escala_luna
        self.escala_tierra = escala_tierra
        self.radio_luna = 0
        super().__init__()
        self.radio_luna = self.scale(Dtl)
        self.tierra = Tierra(
            self.canvas, 
            self.centro_x, 
            self.centro_y, 
            escala_tierra)
        self.luna = Luna(
            self.canvas,
            self.centro_x+self.radio_luna,
            self.centro_y,
            self.escala_luna
        )


    @property
    def centro_x(self):
        return self.width//2
    
    @property
    def centro_y(self):
        return self.height//2

    def montar_widgets(self):
        super().montar_widgets()
        self.add_slider()


    def scale(self, distance:float)->int:
        alpha = self.max_distance/self.min_distance
        factor = (1-alpha)
        constant = ((Max_tl)-(Rt+Rl)*alpha) / factor
        slope = ((Rt+Rl) - constant)/self.min_distance
        # inverse lineal value
        return int((distance - constant) // slope)


    @property
    def min_distance(self):
        return 100.0

    @property
    def max_distance(self):
        return float(self.width/2)
    

    def add_slider(self):
        self.distance_slider = tk.Scale(
            self.app,
            from_=int(Rt+Rl),
            to=int(Dtl),
            orient='horizontal',
            label='Distancia de la luna al centro de la tierra',
            command=self.set_distance_tl
        )
        # valor por defector
        midpoint = (Dtl + Rt - Rl) / 2
        self.distance_slider.set(midpoint)
        self.distance_slider.pack(side='bottom', fill='x')


    def set_distance_tl(self, event):
        value = self.distance_slider.get()
        pixels = self.scale(value)
        #valor = self.distance_slider.get()
        position_x = self.centro_x + pixels
        self.radio_luna = pixels
        self.luna.move_to(position_x, self.luna.y)


    def create_frame(self):
        """
        Generar la lista de puntos de cada posicion
        en este caso, se moverá la luna en torno a la tierra
        """
        angulo = self.current_frame
        if self.luna:
             punto = self.luna.mover_en_torno_a_tierra(
                 self.centro_x,
                 self.centro_y,
                 self.radio_luna,
                 angulo
             )
             self.current_frame = (self.current_frame+1) % 360
             return punto
        else:
            return Punto(self.radio_luna,0)

    def update_canvas(self):
        if self.frames and self.running and not self.paused:
            punto_luna = self.frames.pop(0)
            if self.luna:
                luna_id = self.luna.cid
                x,y = punto_luna.par()
                self.luna.move_to(x,y)
            else:
                print("No hay luna")

        if self.running:
            self.app.after(50, self.update_canvas)
