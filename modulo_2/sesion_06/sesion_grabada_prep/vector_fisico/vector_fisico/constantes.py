# constante gravitatoria universal
G = 6.672e-11 # N*m^2/kg^2
# radio tierra
Rt = 6371000 # m
# radio luna
Rl = 1737400 # m
# distancia tierra-luna
Dtl = 384400000 # m
# masa luna
Mt = 5.972e24
# masa tierra
Ml = 7.348e22
# limite max distancia
Max_tl = Dtl + 2*Rl
