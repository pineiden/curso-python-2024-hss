from dataclasses import dataclass, field
from .base import MagnitudFisica
from .aceleracion import Aceleracion
from .posicion import Posicion
from .velocidad import Velocidad
from .constantes import G, Rt,Rl, Dtl, Mt, Ml
from vectores import VectorNormalizado, Vector

@dataclass
class Fuerza(
        MagnitudFisica, 
        VectorNormalizado):
    
    def __init__(self, 
                 magnitud:float, 
                 vector:Vector):
        MagnitudFisica.__init__(self, "newton", "kg * m/s^2")
        VectorNormalizado.__init__(self, magnitud, vector)


    def aceleracion(self, masa:float):
        """
        G mt ml / rtl^2
        """
        if masa>0:
            return Aceleracion(self.magnitud/masa, self.vector)
        else:
            raise Exception("La masa no puede ser 0 o negativa")


@dataclass
class FuerzaGravitacional:
    posicion_inicial_tierra: Posicion
    posicion_inicial_luna: Posicion
    masa_tierra: float=Mt # kg
    masa_luna: float=Ml # kg

    @property
    def fuerza(self):
        # posicion tierra 
        # pt
        # posicion de la luna 
        # pl
        # Rv distancia entre los dos centros: Tierra y Luna
        pl = self.posicion_inicial_luna
        pt = self.posicion_inicial_tierra
        Rv = (pl-pt)
        breakpoint()
        rmag, rvec = Rv.magnitud, Rv.direccion
        Fmag = G * (self.masa_tierra * self.masa_luna) / rmag**2
        return Fuerza(Fmag, rvec)

    def aceleracion_luna(self):
        aceleracion = self.fuerza.aceleracion(self.masa_luna)
        return aceleracion


    def aceleracion_tierra(self):
        aceleracion = self.fuerza.aceleracion(self.masa_tierra)
        return aceleracion
