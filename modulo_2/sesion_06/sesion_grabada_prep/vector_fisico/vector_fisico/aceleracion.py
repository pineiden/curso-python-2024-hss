from dataclasses import dataclass, field
from typing import TypeVar
from .base import MagnitudFisica
from .posicion import Posicion
from .velocidad import Velocidad
from vectores import VectorNormalizado, Vector

Aceleracion = TypeVar("Aceleracion")

@dataclass
class Aceleracion(
        MagnitudFisica, 
        VectorNormalizado):
    
    def __init__(self, 
                 magnitud:float, 
                 vector:Vector):
        MagnitudFisica.__init__(self, "aceleracion", "m/s^2")
        VectorNormalizado.__init__(self, magnitud, vector)

    def __add__(self, v2:Aceleracion):
        vn = super().__add__(v2)
        m,p = vn.magnitud, vn.direccion
        return Aceleracion(m, p)

    def scale(self, value:float):
        # value: adimensional
        m,p = self.magnitud, self.direccion
        return Aceleracion(m*value, p)

    def __mul__(self, value:float):
        # value: tiempo en s
        vn = super().__mul__(value)
        m,p = vn.magnitud, vn.direccion
        return Velocidad(m, p)

    def ecuacion(
            self, 
            velocidad_0:Velocidad, 
            dt:float)->Velocidad:
        # dt en segundos
        return velocidad_0 + self * dt

