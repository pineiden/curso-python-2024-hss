from dataclasses import dataclass
from typing import TypeVar
import math

Vector = TypeVar("Vector")

@dataclass
class Vector:
    x:float 
    y:float 
    z:float = 0

    @property
    def items(self)->tuple[float,float,float]:
        return (self.x,self.y,self.z)

    @property
    def tamaño(self)->float:
        return math.sqrt(self.x**2+self.y**2+self.z**2)

    def __add__(self, v2:Vector)->Vector:
        params = {
            "x":self.x+v2.x,
            "y":self.y+v2.y,
            "z":self.z+v2.z
        }
        return Vector(**params)


    def __sub__(self, v2:Vector)->Vector:
        params = {
            "x":self.x-v2.x,
            "y":self.y-v2.y,
            "z":self.z-v2.z
        }
        return Vector(**params)

    def __mul__(self, valor:float)->Vector:
        x = self.x * valor
        y = self.y * valor
        z = self.z * valor
        return Vector(x,y,z)

    def __pow__(self, valor:float)->Vector:
        x = self.x**valor
        y = self.y**valor
        z = self.z**valor
        return Vector(x,y,z)

    def __truediv__(self, valor:float)->Vector:
        x = self.x / valor
        y = self.y / valor
        z = self.z / valor
        return Vector(x,y,z)


    def distancia(self, v2:Vector)->float:
        # esto es otro vector
        resultado =  self - v2
        return resultado.tamaño

    def normalizar(self, r:float)->Vector:
        if r>0:
            valores = [v/r for v in self.items]
            return Vector(*valores)
        else:
            return Vector(0,0,0)

    @property
    def par(self)->tuple[float, Vector]:
        m = self.tamaño
        return (m, self.normalizar(m))
