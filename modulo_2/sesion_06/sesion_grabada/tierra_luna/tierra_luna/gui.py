from gui_base.gui import GUIBase
import tkinter as tk
from PIL import Image, ImageTk
import math
from pathlib import Path
from dataclasses import dataclass
from rich import print


@dataclass
class Punto:
    x:float 
    y:float 
    
    def par(self):
        return self.x, self.y

BASE = Path(__file__).parent.resolve() / "assets"

class ObjetoCeleste:
    def __init__(self, 
                 canvas, 
                 image_path, 
                 x,
                 y, 
                 scale):
        self.canvas = canvas
        self.image = Image.open(str(image_path))
        self.scale = scale

        #breakpoint()
        self.image = self.image.resize(
            (
                int(self.image.width*self.scale),
                int(self.image.height*self.scale)
            ),
            Image.LANCZOS
        )
        self.image = ImageTk.PhotoImage(self.image)
        self.cid = self.canvas.create_image(
            x,
            y,
            image=self.image)
        self.x = x
        self.y = y

    def move_to(self, x,y):
        self.x = int(x)
        self.y = int(y)
        self.canvas.coords(self.cid, self.x, self.y)

class Tierra(ObjetoCeleste):
    def __init__(self, canvas, x, y, scale=1):
        super().__init__(canvas, BASE/"earth.png",x,y,scale)


class Luna(ObjetoCeleste):
    def __init__(self, canvas, x, y, scale=1):
        super().__init__(canvas, BASE/"moon.png",x,y,scale)

    def mover_en_torno_a_tierra(self, 
                                centro_x, 
                                centro_y, 
                                radio, 
                                angulo):
        x = centro_x + radio*math.cos(math.radians(angulo))
        y = centro_y + radio*math.sin(math.radians(angulo))
        return Punto(x,y)


class SistemaTierraLuna(GUIBase):

    def __init__(self, 
                 escala_tierra=0.2, 
                 escala_luna=0.05):
        self.luna = None
        self.tierra = None
        self.escala_luna = escala_luna
        self.escala_tierra = escala_tierra
        self.radio_luna = 0
        super().__init__()
        self.radio_luna = 200
        self.tierra = Tierra(
            self.canvas, 
            self.centro_x, 
            self.centro_y, 
            escala_tierra)
        self.luna = Luna(
            self.canvas,
            self.centro_x+self.radio_luna,
            self.centro_y,
            self.escala_luna
        )


    @property
    def centro_x(self):
        return self.width//2
    
    @property
    def centro_y(self):
        return self.height//2

    def montar_widgets(self):
        super().montar_widgets()
        print("Montando widgets en clase SistemaTierraLuna")
        self.add_slider()


    def add_slider(self):
        self.distance_slider = tk.Scale(
            self.app,
            from_=50,
            to=300,
            orient='horizontal',
            label='Distancia de la luna al centro de la tierra',
            command=self.set_distance_tl
        )
        # valor por defector
        self.distance_slider.set(200)
        self.distance_slider.pack(side='bottom', fill='x')


    def set_distance_tl(self, event):
        value = self.distance_slider.get()
        print("Posicion inicial", value)
        #valor = self.distance_slider.get()
        position_x = self.centro_x + value
        self.radio_luna = value
        self.luna.move_to(position_x, self.luna.y)


    def create_frame(self):
        """
        Generar la lista de puntos de cada posicion
        en este caso, se moverá la luna en torno a la tierra
        """
        angulo = self.current_frame
        if self.luna:
             punto = self.luna.mover_en_torno_a_tierra(
                 self.centro_x,
                 self.centro_y,
                 self.radio_luna,
                 angulo
             )
             self.current_frame = (self.current_frame+1) % 360
             return punto
        else:
            return Punto(self.radio_luna,0)

    def update_canvas(self):
        if self.frames and self.running and not self.paused:
            punto_luna = self.frames.pop(0)
            if self.luna:
                luna_id = self.luna.cid
                x,y = punto_luna.par()
                self.luna.move_to(x,y)
            else:
                print("No hay luna")

        if self.running:
            self.app.after(50, self.update_canvas)
