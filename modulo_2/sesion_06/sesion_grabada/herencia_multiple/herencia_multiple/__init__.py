from dataclasses import dataclass

@dataclass
class A:
    pelo:str

    def saltar(self):
        print("Saltando")

    def amasar(self):
        print("Amasando sopaipilla")
        return "Sopaipilla"

@dataclass
class B:
    color: str

    def correr(self):
        print("Corriendo")

    def amasar(self):
        print("Amasando pan")
        return "Pan"

@dataclass
class C(A,B):
    def amasar(self):
        # super sobre B 
        return B.amasar(self)
