from dataclasses import dataclass, field
from typing import TypeVar
from .base import MagnitudFisica
from vectores import VectorNormalizado, Vector

Posicion = TypeVar('Posicion')

@dataclass
class Posicion(
        MagnitudFisica,
        VectorNormalizado):
    def __init__(self, 
                 magnitud:float, 
                 vector:Vector):
        MagnitudFisica.__init__(self, "metro", "m")
        VectorNormalizado.__init__(self, magnitud, vector)
        
    def __add__(self, v2:Posicion):
        vn = super().__add__(v2)
        m,p = vn.magnitud, vn.direccion
        return Posicion(m, p)


    def __mul__(self, value:float):
        # value: adimensional
        vn = super().__mul__(value)
        m,p = vn.magnitud, vn.direccion
        return Posicion(m, p)
