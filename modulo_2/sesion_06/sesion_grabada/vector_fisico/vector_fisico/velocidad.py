from dataclasses import dataclass, field
from typing import TypeVar
from .base import MagnitudFisica
from .posicion import Posicion
from vectores import VectorNormalizado, Vector

Velocidad = TypeVar('Velocidad')

@dataclass
class Velocidad(
        MagnitudFisica,
        VectorNormalizado):
    def __init__(self, 
                 magnitud:float, 
                 vector:Vector):
        MagnitudFisica.__init__(self, "velocidad", "m/s")
        VectorNormalizado.__init__(self, magnitud, vector)

    def __add__(self, v2:Velocidad):
        vn = super().__add__(v2)
        m,p = vn.magnitud, vn.direccion
        return Velocidad(m, p)

    def scale(self, value:float):
        # value: adimensional
        m,p = self.magnitud, self.direccion
        return Velocidad(m*value, p)

    def __mul__(self, value:float):
        # value: tiempo en s
        vn = super().__mul__(value)
        m,p = vn.magnitud, vn.direccion
        return Posicion(m, p)

    def ecuacion(
            self, 
            posicion_0:Posicion, 
            dt:float)->Posicion:
        # dt en segundos
        return posicion_0 + self * dt
