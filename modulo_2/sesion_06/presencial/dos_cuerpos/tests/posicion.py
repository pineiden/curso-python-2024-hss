from dos_cuerpos import Posicion

def test_posicion_suma():
    p1 = Posicion(0,0,0)
    p2 = Posicion(1,1,1)
    assert p1+p2==p2

def test_posicion_resta():
    p1 = Posicion(0,0,0)
    p2 = Posicion(1,1,1)
    assert p1-p2==-1*p2
