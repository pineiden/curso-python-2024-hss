from dos_cuerpos import Vector, Posicion, Velocidad


def test_velocidad_ecuacion():
    posicion_0 = Posicion(10,0,0)
    velocidad_inicial = Velocidad(50, posicion_0)
    assert velocidad_inicial.magnitud==500
 

def test_velocidad_mul():
    posicion_0 = Posicion(10,0,0)
    vector = Velocidad(1, posicion_0)*5
    assert vector.magnitud==50


def test_velocidad_sum():
    posicion_0 = Posicion(10,0,0)
    v1 = Velocidad(1, posicion_0)

    posicion_1 = Posicion(10,0,0)
    v2 = Velocidad(1, posicion_1)
    
    v3 = v1+v2
    assert v3.magnitud==20 and v2.direccion==posicion_1/10


def test_velocidad_ecuacion():    
    posicion_0 = Posicion(10,0,0)
    v1 = Velocidad(1, posicion_0)

    posicion_1 = Posicion(0,5,0)
    dt = 5
    vector = v1.ecuacion(posicion_1, dt)
    print(vector)
    assert isinstance(vector,Vector)
    
