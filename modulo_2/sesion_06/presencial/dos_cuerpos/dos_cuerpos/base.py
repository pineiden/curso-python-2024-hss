from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import TypeVar

from .constantes import G,Rt,Rl,Mt,Ml, D_t_l, EPSILON
from .vector import Vector
from .fuerza import Fuerza

ObjetoCeleste = TypeVar("ObjetoCeleste")

@dataclass
class ObjetoCeleste(ABC):
    masa: float
    radio: float
    posicion:Vector 
    name: str = "Roca"

    @property
    def M(self):
        return self.masa

    @property
    def R(self):
        return self.radio

    def borde_a_borde(self, obj2:ObjetoCeleste)->float:
        dtotal = self.posicion.distancia(obj2.posicion)
        return dtotal - self.radio - obj2.radio

    def fuerza(self, obj2:ObjetoCeleste)->Fuerza:
        r = self.posicion.distancia(obj2.posicion)
        magnitud = -G*self.masa*obj2.masa/r**2
        vector = self.posicion.vector(r)
        return Fuerza(F, vector)

