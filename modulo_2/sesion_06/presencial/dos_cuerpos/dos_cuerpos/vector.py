from dataclasses import dataclass, field
from typing import TypeVar
from .direccion import Direccion

Vector = TypeVar("Vector")


@dataclass
class Vector:
    # positive value>=0
    magnitud: float 
    direccion: Direccion

    def __post_init__(self, *args, **kwargs):
        m = self.direccion.magnitud()
        p = self.direccion.normalizar(m)
        self.direccion = p
        self.magnitud *= m

    def __eq__(self, v:Vector)->bool:
        return (
            abs(self.magnitud-v.magnitud)<=EPSILON and 
            self.direccion==v.direccion
        )

    @property
    def vector(self)->Direccion:
        return self.direccion*self.magnitud

    def __mul__(self, value:float):
        magnitud = self.magnitud * value 
        return Vector(magnitud, self.direccion)

    def __truediv__(self, value:float):
        magnitud = self.magnitud / value 
        return Vector(magnitud, self.direccion)

    def __add__(self, v2:Vector):
        suma_vector =  self.vector + v2.vector 
        magnitud, direccion = suma_vector.par()
        return Vector(magnitud, direccion)

    def __sub__(self, v2:Vector):
        subs_vector =  self.vector - v2.vector 
        magnitud, direccion = subs_vector.par()
        return Vector(magnitud, direccion)

