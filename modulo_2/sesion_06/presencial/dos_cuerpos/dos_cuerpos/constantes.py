# Constante gravitacional: 
# unidades: Nm2/kg2
G = 6.672e10-11 
# radio de la tierra
Rt = 6.378e+6
# masa de la tierra
Mt = 5.972e24
# radio de la luna
Rl = 1.74e6
# masa de la luna
Ml = 7.35e22
# distancia Tierra a luna
D_t_l = 384_400_000
# epsilon para comparare
EPSILON=1e-10
