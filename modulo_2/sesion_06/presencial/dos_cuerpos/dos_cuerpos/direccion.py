from dataclasses import dataclass, field
from typing import TypeVar
import math

from .constantes import EPSILON

Direccion = TypeVar("Direccion")

@dataclass
class Direccion:
    """Direccion
    de un objeto en el espacio 3D

    """
    x:float
    y:float
    z:float = 0
    
    @property
    def items(self):
        return (self.x, self.y, self.z)

    def __add__(self, p2:Direccion):
        params = {
            "x":self.x + p2.x,
            "y":self.y + p2.y,
            "z":self.z + p2.z,
        }
        return Direccion(**params)

    def __sub__(self, p2:Direccion):
        params = {
            "x":self.x - p2.x,
            "y":self.y - p2.y,
            "z":self.z - p2.z,
        }
        return Direccion(**params)

    def __mul__(self, value:float):
        x = self.x * value 
        y = self.y * value 
        z = self.z * value
        return Direccion(x,y,z)

    def __pow__(self, value:float):
        x = self.x**value 
        y = self.y**value 
        z = self.z**value
        return Direccion(x,y,z)

    def __truediv__(self, value:float):
        x = self.x/value 
        y = self.y/value 
        z = self.z/value
        return Direccion(x,y,z)
    
    def distancia(self, p2:Direccion)->(float, tuple[int]):
        p_result = self - p2 
        xx = (p_result.x)**2
        yy = (p_result.y)**2
        zz = (p_result.z)**2
        signos = tuple(math.copysign(1, value) for value in p_result.items)
        return math.sqrt(xx+yy+zz), signos

    def magnitud(self)->float:
        dist, signos = self.distancia(Direccion(0,0,0))
        return dist

    def normalizar(self, r:float)->Direccion:
        if r>0:
            values = [v/r for v in self.items]
            return Direccion(*values)
        else:
            return Direccion(0,0,0)

    def par(self):
        m = self.magnitud()
        return (m, self.normalizar(m))

    def __eq__(self, p:Direccion):
        return (
            abs(self.x-p.x)<=EPSILON and 
            abs(self.y-p.y)<=EPSILON and 
            abs(self.z-p.z)<=EPSILON)
