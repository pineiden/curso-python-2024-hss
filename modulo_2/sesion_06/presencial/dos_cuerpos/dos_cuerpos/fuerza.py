from abc import ABC, abstractmethod
from dataclasses import dataclass, field
import math
from typing import TypeVar

from .vector import Vector
from .aceleracion import Aceleracion
      

@dataclass
class Fuerza(Vector):
    """
    F = m * a
    """
    def aceleracion(self, masa:float):
        if masa>0:
            return Aceleracion(
                self.magnitud/masa, 
                self.vector)
        else:
            raise Exception("La masa no puede ser negativa")
