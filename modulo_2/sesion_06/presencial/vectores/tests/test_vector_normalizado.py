from vectores import Vector,VectorNormalizado


def test_vector_normalizado():
    x = 3
    y = 4
    v = Vector(3,4)
    vn = VectorNormalizado(1,v)
    m = v.tamaño
    assert m==vn.magnitud
    assert vn.direccion.tamaño==1


def test_vector_normalizado_a_vector():
    x = 3
    y = 4
    v = Vector(x,y)
    vn = VectorNormalizado(1,v)
    m = v.tamaño

    assert v==vn.vector


def test_vector_norm_multiplica():
    x = 6
    y = 8
    v = Vector(x,y)
    vn = VectorNormalizado(1,v)
    vn2 = vn/2
    assert vn2.magnitud == 5


def test_vector_suma():
    x = 3
    y = 4
    v = Vector(x,y)
    vn1 = VectorNormalizado(1,v)

    x = 3
    y = 4
    v = Vector(x,y)
    vn2 = VectorNormalizado(1,v)

    vn3 = vn1 + vn2
    assert vn3 == vn1*2



def test_vector_resta():
    x = 6
    y = 8
    v = Vector(x,y)
    vn1 = VectorNormalizado(1,v)

    x = 3
    y = 4
    v = Vector(x,y)
    vn2 = VectorNormalizado(1,v)

    vn3 = vn1 - vn2
    assert vn3 == vn2
