from dataclasses import dataclass

@dataclass
class MagnitudFisica:
    nombre:str
    abreviacion:str
    
    @property
    def um(self):
        return f"[{self.abreviacion}]"
