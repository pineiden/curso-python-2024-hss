from dataclasses import dataclass, field
from typing import TypeVar
from .base import MagnitudFisica
from vectores import VectorNormalizado,Vector

Aceleracion = TypeVar("Aceleracion")


@dataclass
class Aceleracion(
        MagnitudFisica, 
        VectorNormalizado):

    def __init__(self, magnitud:float, vector:Vector):
        MagnitudFisica.__init__(self, "metro/segundo^2", "m/s^2")
        VectorNormalizado.__init__(self, magnitud, vector)

    # def __mul__(self, value:float):
    #     """
    #     Transform Aceleracion*Tiempo->Velocidad
    #     """
    #     magnitud = self.magnitud * value 
    #     return Velocidad(magnitud, self.direccion)

    # def ecuacion(self, 
    #               velocidad_0:Velocidad, 
    #               dt: Tiempo)->Velocidad:
    #     m, p = self.magnitud, self.direccion
    #     aceleracion_1 = Aceleracion(m,p)
    #     return velocidad_0 + aceleracion_1 * float(dt)
