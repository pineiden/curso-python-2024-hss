from .base import MagnitudFisica
from vectores import VectorNormalizado
from dataclasses import dataclass
from .aceleracion import Aceleracion

@dataclass
class FuerzaGravitacional(
        MagnitudFisica, 
        VectorNormalizado):

    def aceleracion(self, masa:float):
        if masa>0:
            return Aceleracion(
                self.magnitud/masa, 
                self.vector)
        else:
            raise Exception("La masa no puede ser negativa")
