from tkinter import Tk,Canvas, ttk
import tkinter as tk
from typing import Any
from dataclasses import dataclass
from rich import print
import threading
import time

@dataclass
class Frame:
    x:float 
    y:float


class GUIMovimiento:
    frames:list[Any]
    width:int=600
    height:int=600
    paused:bool=False
    running:bool=True
    current_frame:int = 0

    def __init__(self):
        self.app = Tk()
        self.build()
        self.montar_widgets()
        self.frames = []
        self.current_frame = 0
        # activar el hilo para procesar la funcion
        self.calculate_thread = threading.Thread(
            target=self.calculate_frames
        )
        self.calculate_thread.start()
        self.update_canvas()
        self.app.protocol("WM_DELETE_WINDOW", self.close)

    def build(self):
        self.base_size = (self.width, self.height)
        shape = f"{self.width}x{self.height}"
        self.app.geometry(shape)

    def montar_widgets(self):
        self.montar_canvas()
        self.montar_botones()

    def montar_canvas(self):
        self.canvas = Canvas(self.app, bg="white")
        self.canvas.pack(
            anchor="nw",
            fill="both",
            expand=True
        )

    def montar_botones(self):
        style = ttk.Style()
        style.configure(
            "TButton",
            font=("Helvetica",12),
            padding=10
        )
        style.map(
            "TButton",
            background=[
                ("active","#FFDDC1"),
                ("!active","#FFA07A")
            ]
        )
        # boton de pause
        self.pause_button = ttk.Button(
            self.app,
            text="⏸ Pausar",
            command=self.toggle_pause,
            style="TButton"
        )
        self.pause_button.pack(
            side=tk.LEFT,
            padx=10,
            pady=10
        )

        self.stop_button = ttk.Button(
            self.app, 
            text="⏹ Detener", 
            command=self.stop_animation, 
            style='TButton')
        self.stop_button.pack(
            side=tk.LEFT, 
            padx=10, 
            pady=10)
        
        self.close_button = ttk.Button(
            self.app, 
            text="✖️ Cerrar", 
            command=self.close, 
            style='TButton')
        self.close_button.pack(
            side=tk.RIGHT, 
            padx=10, 
            pady=10)
    
    def toggle_pause(self):
        self.paused = not self.paused 
        if self.paused:
            self.pause_button.config(text="▶️ Continuar")
        else:
            self.pause_button.config(text="⏸ Pausar")

    def stop_animation(self):
        self.running = False

    def close(self):
        self.stop_animation()
        self.app.destroy()

    def run(self):
        self.app.mainloop()

    def update_canvas(self):
        if self.frames and self.running and not self.paused:
            frame = self.frames.pop(0)
            self.canvas.delete("all")
            self.canvas.create_oval(
                frame.x,
                frame.y,
                frame.x+20,
                frame.y+20,
                fill="blue"
            )
            self.current_frame += 1
        if self.running:
            self.app.after(50, self.update_canvas)


    def create_frame(self)->Frame:
        frame = {
            "x":self.current_frame%600,
            "y":200
        }
        return Frame(**frame)

    
    def calculate_frames(self):
        while self.running:
            if not self.paused:
                frame = self.create_frame()
                self.frames.append(frame)
            time.sleep(0.1)
