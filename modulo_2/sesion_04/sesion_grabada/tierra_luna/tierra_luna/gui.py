from gui_base.gui import GUIBase
from dataclasses import dataclass
import tkinter as tk
from PIL import Image, ImageTk
import math
from pathlib import Path
from rich import print


@dataclass
class Punto:
    x:float 
    y:float 
    
    def par(self):
        return self.x, self.y


BASE = Path(__file__).parent.resolve() / "assets"

class ObjetoCeleste:
    def __init__(self, 
                 canvas:tk.Canvas, 
                 image_path:Path,
                 x:float, 
                 y:float, 
                 scale:float):
        self.canvas = canvas
        self.image = Image.open(str(image_path))
        self.scale = scale

        self.image = self.image.resize(
            (
                int(self.image.width*self.scale),
                int(self.image.height*self.scale),                
            ), Image.LANCZOS
        )

        self.image = ImageTk.PhotoImage(self.image)
        self.cid = self.canvas.create_image(
            x,y,image=self.image
        )
        self.x = x
        self.y = y

    def move_to(self, x:float, y:float):
        self.x = int(x)
        self.y = int(y)
        self.canvas.coords(self.cid, self.x, self.y)


class Tierra(ObjetoCeleste):
    def __init__(self, canvas, x,y,scale=1):
        super().__init__(canvas, BASE/"earth.png",x,y,scale)

class Luna(ObjetoCeleste):
    def __init__(self, canvas, x,y,scale=1):
        super().__init__(canvas, BASE/"moon.png",x,y,scale)

    def mover_en_torno_a_tierra(
            self,
            centro_x:int,
            centro_y:int,
            radio:float,
            angulo:float):
        angulo_rads = math.radians(angulo)
        x = centro_x + radio * math.cos(angulo_rads)
        y = centro_y + radio * math.sin(angulo_rads)

        return Punto(x,y)


@dataclass
class SistemaTierraLuna(GUIBase):
    def __init__(self, escala_tierra=0.2, escala_luna=0.05):
        self.luna = None
        self.tierra = None
        self.escala_luna = escala_luna
        self.escala_tierra = escala_tierra
        self.radio_luna = 0        
        # aqui se monta canvas y botones
        # se inicia hilo
        super().__init__()
        self.radio_luna = 200
        self.tierra = Tierra(self.canvas, 
                             self.centro_x,
                             self.centro_y, 
                             escala_tierra)
        self.luna = Luna(self.canvas, 
                         self.centro_x + self.radio_luna,
                         self.centro_y,
                         self.escala_luna)

    @property
    def centro_x(self):
        return self.width // 2

    @property
    def centro_y(self):
        return self.height // 2


    def create_frame(self):
        angulo = self.current_frame
        if self.luna:
            punto = self.luna.mover_en_torno_a_tierra(
                self.centro_x,
                self.centro_y,
                self.radio_luna,
                angulo
            )
            self.current_frame = (self.current_frame+1)%360
            return punto
        else:
            return Punto(self.radio_luna, 0)

    def update_canvas(self):
        if self.frames and self.running and not self.paused:
            punto_luna = self.frames.pop(0)
            if self.luna:
                luna_id = self.luna.cid
                x,y = punto_luna.par()
                self.luna.move_to(x,y)
            else:
                print("No hay luna")

        if self.running:
            self.app.after(50, self.update_canvas)
