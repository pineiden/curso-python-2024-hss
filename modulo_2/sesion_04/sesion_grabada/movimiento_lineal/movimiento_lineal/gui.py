from tkinter import Tk, Canvas, ttk
import tkinter as tk
from typing import Any
from dataclasses import dataclass
from rich import print
import threading
import time
import math

@dataclass
class Punto:
    x: float 
    y: float

@dataclass
class GUIMovimiento:
    frames:list[Any]
    width:int=600
    height:int=600
    paused:bool=False
    running:bool=True
    current_frame:int = 0

    def __init__(self):
        self.app = Tk()
        # montar visual
        self.build()
        self.montar_widgets()
        # activar animacion
        self.frames = []
        self.current_frame = 0
        # activar el hilo ||
        self.calculate_thread = threading.Thread(
            target=self.calculate_frames
        )
        self.calculate_thread.start()
        # dibujar la primera imagen
        self.update_canvas()
        self.app.protocol("WM_DELETE_WINDOW", self.close)


    def montar_widgets(self):
        self.montar_canvas()
        self.montar_botones()

    def montar_canvas(self):
        self.canvas = Canvas(self.app, bg="black")
        self.canvas.pack(
            anchor="nw",
            fill="both",
            expand=True
        )

    def montar_botones(self):
        # definir un estilo
        style = ttk.Style()
        style.configure(
            "TButton",
            font=("Helvetica", 12),
            padding=10
        )
        style.map(
            "TButton",
            background=[
                ("active", "#FFDDC1"),
                ("!active", "#FFA07A")
            ]
        )
        # ubicamos los botones:
        # - pausa 
        # - detencion
        # - cerrar
        self.pause_button = ttk.Button(
            self.app,
            text="⏸ Pausar",
            command=self.toggle_pause,
            style="TButton"
        )
        self.pause_button.pack(
            side=tk.LEFT,
            padx=10,
            pady=10
        )

        self.stop_button = ttk.Button(
            self.app, 
            text="⏹ Detener", 
            command=self.stop_animation, 
            style='TButton')
        self.stop_button.pack(
            side=tk.LEFT, 
            padx=10, 
            pady=10)

        self.close_button = ttk.Button(
            self.app, 
            text="✖️ Cerrar", 
            command=self.close, 
            style='TButton')
        self.close_button.pack(
            side=tk.RIGHT, 
            padx=10, 
            pady=10)

    def close(self):
        self.stop_animation()
        self.app.destroy()

    def stop_animation(self):
        self.running = False
        
    def toggle_pause(self):
        self.paused = not self.paused
        if self.paused:
            self.pause_button.config(text="▶️ Continuar")
        else:
            self.pause_button.config(text="⏸ Pausar")


    def build(self):
        self.base_size = (self.width, self.height)
        shape = f"{self.width}x{self.height}"
        self.app.geometry(shape)

    def run(self):
        self.app.mainloop()

    # métodos para activar la animación sobre canvas
    def create_frame(self)->Punto:
        punto = {
            "x": self.current_frame%600,
            "y": 200 + 50*math.sin(math.radians((self.current_frame%600)*10))
        }
        return Punto(**punto)

    def calculate_frames(self):
        while self.running:
            if not self.paused:
                frame = self.create_frame()
                self.frames.append(frame)
            time.sleep(0.1)

    def update_canvas(self):
        if self.frames and self.running and not self.paused:
            punto = self.frames.pop(0)
            self.canvas.delete("all")
            self.canvas.create_oval(
                punto.x,
                punto.y,
                punto.x + 20,
                punto.y + 20,
                fill="blue"
            )
            self.current_frame += 1
        if self.running:
            self.app.after(50, self.update_canvas)



