from tkinter import Tk,Canvas, ttk
import tkinter as tk
from typing import Any
from dataclasses import dataclass
from rich import print
import threading
import time
import math

@dataclass
class Punto:
    x:float 
    y:float 
    
    def par(self):
        return self.x, self.y


@dataclass
class Cardioide:
    x0:float 
    y0:float

    size:int=50
    amplitude:int = 10
    frequency:float = 0.1
    color:str = "red"

    def figura(self, current_frame:int)->list[Punto]:
        t = current_frame * self.frequency
        size = self.size  + self.amplitude*math.sin(t)
        points = []
        for angulo in range(0,360,5):
            radianes = math.radians(angulo)
            x = self.x0 + size*(1-math.sin(radianes))*math.cos(radianes)

            y = self.y0 - size*(1-math.sin(radianes))*math.sin(radianes)

            points.append(Punto(x,y))

        return points




class GUICardioide:
    frames:list[Any]
    width:int=600
    height:int=600
    paused:bool=False
    running:bool=True
    current_frame:int = 0

    def __init__(self):
        self.cardioide = Cardioide(self.width/2, self.height/2)
        self.app = Tk()
        self.build()
        self.montar_widgets()
        self.frames = []
        self.current_frame = 0
        # activar el hilo para procesar la funcion
        self.calculate_thread = threading.Thread(
            target=self.calculate_frames
        )
        self.calculate_thread.start()
        self.update_canvas()
        self.app.protocol("WM_DELETE_WINDOW", self.close)

    def build(self):
        self.base_size = (self.width, self.height)
        shape = f"{self.width}x{self.height}"
        self.app.geometry(shape)

    def montar_widgets(self):
        self.montar_canvas()
        self.montar_botones()

    def montar_canvas(self):
        self.canvas = Canvas(self.app, bg="white")
        self.canvas.pack(
            anchor="nw",
            fill="both",
            expand=True
        )

    def montar_botones(self):
        style = ttk.Style()
        style.configure(
            "TButton",
            font=("Helvetica",12),
            padding=10
        )
        style.map(
            "TButton",
            background=[
                ("active","#FFDDC1"),
                ("!active","#FFA07A")
            ]
        )
        # boton de pause
        self.pause_button = ttk.Button(
            self.app,
            text="⏸ Pausar",
            command=self.toggle_pause,
            style="TButton"
        )
        self.pause_button.pack(
            side=tk.LEFT,
            padx=10,
            pady=10
        )

        self.stop_button = ttk.Button(
            self.app, 
            text="⏹ Detener", 
            command=self.stop_animation, 
            style='TButton')
        self.stop_button.pack(
            side=tk.LEFT, 
            padx=10, 
            pady=10)
        
        self.close_button = ttk.Button(
            self.app, 
            text="✖️ Cerrar", 
            command=self.close, 
            style='TButton')
        self.close_button.pack(
            side=tk.RIGHT, 
            padx=10, 
            pady=10)
    
    def toggle_pause(self):
        self.paused = not self.paused 
        if self.paused:
            self.pause_button.config(text="▶️ Continuar")
        else:
            self.pause_button.config(text="⏸ Pausar")

    def stop_animation(self):
        self.running = False

    def close(self):
        self.stop_animation()
        self.app.destroy()

    def run(self):
        self.app.mainloop()

    def update_canvas(self):
        if self.frames and self.running and not self.paused:
            puntos = self.frames.pop(0)
            self.canvas.delete("all")
            for i, punto in enumerate(puntos):
                x1,y1 = punto.par()
                punto_prox = puntos[(i+1)%len(puntos)]
                x2, y2 = punto_prox.par()
                self.canvas.create_line(
                    x1,y1,x2,y2, fill="red", width=5
                )
            self.current_frame += 1
        if self.running:
            self.app.after(50, self.update_canvas)


    def create_frame(self)->list[Punto]:
        points = self.cardioide.figura(self.current_frame)
        self.current_frame += 1
        return points

    
    def calculate_frames(self):
        while self.running:
            if not self.paused:
                frame = self.create_frame()
                self.frames.append(frame)
            time.sleep(0.1)
