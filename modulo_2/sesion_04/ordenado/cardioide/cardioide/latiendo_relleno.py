from tkinter import Tk, Canvas, PhotoImage, NW
import tkinter as tk
from tkinter import ttk
from typing import Any
from dataclasses import dataclass
from rich import print
import threading
import time
import math

@dataclass
class Point:
    x:float 
    y:float 

    def par(self):
        return self.x, self.y

@dataclass
class Cardioide:
    x0:float 
    y0:float
    size:int = 50
    amplitude:int = 10
    frequency:float = 0.1
    color:str = "red"

    def figure(self, current_frame:int)->list[Point]:
        t = current_frame * self.frequency
        size = self.size + self.amplitude * math.sin(t)
        points = []
        for angle in range(0, 360, 5):
            radians = math.radians(angle)
            x = self.x0 + size * (1 - math.sin(radians)) * math.cos(radians)
            y = self.y0 - size * (1 - math.sin(radians)) * math.sin(radians)
            points.append(Point(x, y))
        return points


class GUICardioideRelleno:
    frames:list[Any]
    width:int=600
    height:int=600
    paused:bool=False
    running:bool=True
    
    def __init__(self):
        self.app = Tk()
        self.build()
        self.montar_widgets()
        self.estilos()
        self.cardioide = Cardioide(self.width/2,self.height/2)
        self.frames = []
        self.current_frame = 0
        # Iniciar el hilo para calcular los frames
        self.calculate_thread = threading.Thread(
            target=self.calculate_frames)
        self.calculate_thread.start()

        # iniciar animación al abrir la ventana
        self.update_canvas()
        # safe close and END
        self.app.protocol(
            "WM_DELETE_WINDOW", 
            self.close)

    def estilos(self):
        # Definir estilos para los botones
        style = ttk.Style()
        style.configure('TButton', font=('Helvetica', 12), padding=10)
        style.map('TButton', background=[('active', '#FFDDC1'), ('!active', '#FFA07A')])
        self.estilo = style

    def build(self):
        self.base_size = (self.width,self.height)
        shape = f"{self.width}x{self.height}"
        self.app.geometry(shape)

    def montar_widgets(self):
        self.montar_canvas()
        self.montar_botones()

    def montar_canvas(self):
        self.canvas = Canvas(self.app, bg="black")
        self.canvas.pack(
            anchor="nw",
            fill="both",
            expand=True
        )

    def montar_botones(self):
        style = ttk.Style()
        style.configure(
            "TButton",
            font=("Helvetica", 12),
            padding=10
        )
        style.map(
            "TButton",
            background=[
                ("active","#FFDDC1"),
                ("!active", "#FFA07A")
            ])
        self.pause_button = ttk.Button(
            self.app,
            text="⏸ Pausar",
            command=self.toggle_pause,
            style="TButton"
        )
        self.pause_button.pack(
            side=tk.LEFT, 
            padx=10, 
            pady=10)

        self.stop_button = ttk.Button(
            self.app, 
            text="⏹ Detener", 
            command=self.stop_animation, 
            style='TButton')
        self.stop_button.pack(
            side=tk.LEFT, 
            padx=10, 
            pady=10)
        
        self.close_button = ttk.Button(
            self.app, 
            text="✖️ Cerrar", 
            command=self.close, 
            style='TButton')
        self.close_button.pack(
            side=tk.RIGHT, 
            padx=10, 
            pady=10)

    def toggle_pause(self):
        self.paused = not self.paused
        if self.paused:
            self.pause_button.config(
                text="▶️ Continuar")
        else:
            self.pause_button.config(
                text="⏸ Pausar")

    def stop_animation(self):
        self.running = False

    def close(self):
        self.stop_animation()
        self.app.destroy()

    def run(self):
        self.app.mainloop()


    # manejar frame 
    def create_frame(self)->list[Point]:
        """
        lo que se dibuja en el canvas
        parametrizado

        """
        points = self.cardioide.figure(self.current_frame)
        self.current_frame += 1
        return points

    def calculate_frames(self):
        while self.running:
            if not self.paused:
                frame = self.create_frame()
                self.frames.append(frame)
            time.sleep(0.1)


    def update_canvas(self):
        if self.frames and self.running and not self.paused:
            points = self.frames.pop(0)
            self.canvas.delete("all")
            self.fill_heart(points)
            # dibujar un circulo
            # for i, point in enumerate(points):
            #     x1, y1 = point.par()
            #     next_point = points[(i+1) % len(points)]
            #     x2, y2 = next_point.par()
            #     self.canvas.create_line(
            #         x1,
            #         y1,
            #         x2, 
            #         y2, 
            #         fill="red", 
            #         width=5)
        if self.running:
            # https://recursospython.com/guias-y-manuales/la-funcion-after-en-tkinter/
            # millisegundos
            self.app.after(
                50, 
                self.update_canvas)


    def fill_heart(self, points):
         min_y = min(p.y for p in points)
         max_y = max(p.y for p in points)
         for y in range(int(min_y), int(max_y)):
             intersection_points = []
             for i,point in enumerate(points):
                 x1, y1 = point.par()
                 next_point = points[(i + 1) % len(points)]
                 x2, y2 = next_point.par()
                 if y1 <= y < y2 or y2 <= y < y1:
                     if y2 != y1:
                         x = x1 + (y - y1) * (x2 - x1) / (y2 - y1)
                         intersection_points.append(x)
             intersection_points.sort()
             for i in range(0, len(intersection_points), 2):
                 x_start = intersection_points[i]
                 x_end = intersection_points[i + 1] if i + 1 < len(intersection_points) else x_start
                 self.canvas.create_line(x_start, y, x_end, y, fill='red')
