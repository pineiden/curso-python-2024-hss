import tkinter as tk
from tkinter import ttk
import threading
import time

class AnimationApp:
    def __init__(self, root):
        self.root = root
        self.canvas = tk.Canvas(root, width=400, height=400)
        self.canvas.pack()
        
        self.frames = []
        self.current_frame = 0
        self.running = True
        self.paused = False
        
        # Definir estilos para los botones
        style = ttk.Style()
        style.configure('TButton', font=('Helvetica', 12), padding=10)
        style.map('TButton', background=[('active', '#FFDDC1'), ('!active', '#FFA07A')])

        # Botones para detener, pausar/continuar la animación y cerrar la ventana
        self.pause_button = ttk.Button(root, text="⏸ Pausar", command=self.toggle_pause, style='TButton')
        self.pause_button.pack(side=tk.LEFT, padx=10, pady=10)

        self.stop_button = ttk.Button(root, text="⏹ Detener", command=self.stop_animation, style='TButton')
        self.stop_button.pack(side=tk.LEFT, padx=10, pady=10)
        
        self.close_button = ttk.Button(root, text="✖️ Cerrar", command=self.close, style='TButton')
        self.close_button.pack(side=tk.RIGHT, padx=10, pady=10)
        
        # Iniciar el hilo para calcular los frames
        self.calculate_thread = threading.Thread(target=self.calculate_frames)
        self.calculate_thread.start()
        
        # Iniciar la animación
        self.update_canvas()
    
    def calculate_frames(self):
        while self.running:
            if not self.paused:
                frame = self.create_frame()
                self.frames.append(frame)
            time.sleep(0.1)  # Simula el tiempo de cálculo
    
    def create_frame(self):
        # Aquí puedes poner tu lógica para crear un frame
        frame = {"x": self.current_frame % 400, "y": 200}
        return frame
    
    def update_canvas(self):
        if self.frames and self.running and not self.paused:
            frame = self.frames.pop(0)
            self.canvas.delete("all")
            self.canvas.create_oval(frame["x"], frame["y"], frame["x"] + 20, frame["y"] + 20, fill="blue")
            self.current_frame += 1
        
        if self.running:
            self.root.after(50, self.update_canvas)

    def toggle_pause(self):
        self.paused = not self.paused
        if self.paused:
            self.pause_button.config(text="▶️ Continuar")
        else:
            self.pause_button.config(text="⏸ Pausar")

    def stop_animation(self):
        self.running = False
    
    def close(self):
        self.stop_animation()
        self.root.destroy()

if __name__ == "__main__":
    root = tk.Tk()
    app = AnimationApp(root)
    root.protocol("WM_DELETE_WINDOW", app.close)
    root.mainloop()
