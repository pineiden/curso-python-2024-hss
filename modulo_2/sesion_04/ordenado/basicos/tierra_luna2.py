import tkinter as tk
from PIL import Image, ImageTk
import math

class ObjetoCeleste:
    def __init__(self, canvas, image_path, x, y, scale):
        self.canvas = canvas
        self.image = Image.open(image_path)
        self.scale = scale
        self.image = self.image.resize((int(self.image.width * self.scale), int(self.image.height * self.scale)), Image.LANCZOS)
        self.image = ImageTk.PhotoImage(self.image)
        self.id = self.canvas.create_image(x, y, image=self.image)
        self.x = x
        self.y = y

    def move_to(self, x, y):
        self.canvas.coords(self.id, x, y)
        self.x = x
        self.y = y

class Tierra(ObjetoCeleste):
    def __init__(self, canvas, x, y, scale=1):
        super().__init__(canvas, "earth.png", x, y, scale)

class Luna(ObjetoCeleste):
    def __init__(self, canvas, x, y, scale=0.273):
        super().__init__(canvas, "moon.png", x, y, scale)

    def move_around(self, center_x, center_y, radius, angle):
        x = center_x + radius * math.cos(math.radians(angle))
        y = center_y + radius * math.sin(math.radians(angle))
        self.move_to(x, y)

class AnimationApp:
    def __init__(self, root):
        self.canvas_width = 800
        self.canvas_height = 600
        self.canvas = tk.Canvas(root, width=self.canvas_width, height=self.canvas_height, bg="black")
        self.canvas.pack()

        # Posicionar Tierra y Luna en el centro del canvas
        self.center_x = self.canvas_width // 2
        self.center_y = self.canvas_height // 2
        
        # Escala para los tamaños de las imágenes
        self.earth_scale = 0.2  # Escala para la imagen de la Tierra
        self.moon_scale = 0.05  # Escala para la imagen de la Luna
        
        # Crear Tierra y Luna
        self.tierra = Tierra(self.canvas, self.center_x, self.center_y, self.earth_scale)
        self.luna = Luna(self.canvas, self.center_x + 200, self.center_y, self.moon_scale)  # Ajustar la posición inicial de la Luna

        self.angle = 0
        self.animate()

    def animate(self):
        self.angle = (self.angle + 1) % 360
        self.luna.move_around(self.center_x, self.center_y, 200, self.angle)
        self.canvas.after(50, self.animate)

if __name__ == "__main__":
    root = tk.Tk()
    app = AnimationApp(root)
    root.mainloop()
