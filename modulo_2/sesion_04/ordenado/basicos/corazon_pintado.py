import tkinter as tk
from tkinter import ttk
import threading
import time
import math

class AnimationApp:
    def __init__(self, root):
        self.root = root
        self.canvas = tk.Canvas(root, width=400, height=400)
        self.canvas.pack()
        
        self.frames = []
        self.current_frame = 0
        self.running = True
        self.paused = False
        self.size = 50
        self.amplitude = 10
        self.frequency = 0.1
        
        # Definir estilos para los botones
        style = ttk.Style()
        style.configure('TButton', font=('Helvetica', 12), padding=10)
        style.map('TButton', background=[('active', '#FFDDC1'), ('!active', '#FFA07A')])

        # Botones para detener, pausar/continuar la animación y cerrar la ventana
        self.pause_button = ttk.Button(root, text="⏸ Pausar", command=self.toggle_pause, style='TButton')
        self.pause_button.pack(side=tk.LEFT, padx=10, pady=10)

        self.stop_button = ttk.Button(root, text="⏹ Detener", command=self.stop_animation, style='TButton')
        self.stop_button.pack(side=tk.LEFT, padx=10, pady=10)
        
        self.close_button = ttk.Button(root, text="✖️ Cerrar", command=self.close, style='TButton')
        self.close_button.pack(side=tk.RIGHT, padx=10, pady=10)
        
        # Iniciar el hilo para calcular los frames
        self.calculate_thread = threading.Thread(target=self.calculate_frames)
        self.calculate_thread.start()
        
        # Iniciar la animación
        self.update_canvas()
    
    def calculate_frames(self):
        while self.running:
            if not self.paused:
                frame = self.create_frame()
                self.frames.append(frame)
            time.sleep(0.1)  # Simula el tiempo de cálculo
    
    def create_frame(self):
        t = self.current_frame * self.frequency
        size = self.size + self.amplitude * math.sin(t)
        points = []
        for angle in range(0, 360, 5):
            radians = math.radians(angle)
            x = 200 + size * (1 - math.sin(radians)) * math.cos(radians)
            y = 200 - size * (1 - math.sin(radians)) * math.sin(radians)
            points.append((x, y))
        self.current_frame += 1
        return points
    
    def update_canvas(self):
        if self.frames and self.running and not self.paused:
            frame = self.frames.pop(0)
            self.canvas.delete("all")
            self.fill_heart_with_gradient(frame)
        
        if self.running:
            self.root.after(50, self.update_canvas)

    def fill_heart_with_gradient(self, points):
        min_y = min(y for x, y in points)
        max_y = max(y for x, y in points)
        height = max_y - min_y
        for y in range(int(min_y), int(max_y)):
            intersection_points = []
            for i in range(len(points)):
                x1, y1 = points[i]
                x2, y2 = points[(i + 1) % len(points)]
                if y1 <= y < y2 or y2 <= y < y1:
                    if y2 != y1:
                        x = x1 + (y - y1) * (x2 - x1) / (y2 - y1)
                        intersection_points.append(x)
            intersection_points.sort()
            for i in range(0, len(intersection_points), 2):
                x_start = intersection_points[i]
                x_end = intersection_points[i + 1] if i + 1 < len(intersection_points) else x_start
                color = self.get_gradient_color(y - min_y, height)
                self.canvas.create_line(x_start, y, x_end, y, fill=color)

    def get_gradient_color(self, position, total):
        r = 255
        g = int(255 * (position / total))
        b = int(255 * (position / total))
        return f'#{r:02x}{g:02x}{b:02x}'

    def toggle_pause(self):
        self.paused = not self.paused
        if self.paused:
            self.pause_button.config(text="▶️ Continuar")
        else:
            self.pause_button.config(text="⏸ Pausar")

    def stop_animation(self):
        self.running = False
    
    def close(self):
        self.stop_animation()
        self.root.destroy()

if __name__ == "__main__":
    root = tk.Tk()
    app = AnimationApp(root)
    root.protocol("WM_DELETE_WINDOW", app.close)
    root.mainloop()
