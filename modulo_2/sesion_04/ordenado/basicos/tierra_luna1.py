import tkinter as tk
from PIL import Image, ImageTk
import math

class ObjetoCeleste:
    def __init__(self, canvas, image_path, x, y):
        self.canvas = canvas
        self.image = Image.open(image_path)
        self.image = ImageTk.PhotoImage(self.image)
        self.id = self.canvas.create_image(x, y, image=self.image)
        self.x = x
        self.y = y

    def move_to(self, x, y):
        self.canvas.coords(self.id, x, y)
        self.x = x
        self.y = y

class Tierra(ObjetoCeleste):
    def __init__(self, canvas, x, y):
        super().__init__(canvas, "earth.png", x, y)

class Luna(ObjetoCeleste):
    def __init__(self, canvas, x, y):
        super().__init__(canvas, "moon.png", x, y)

    def move_around(self, center_x, center_y, radius, angle):
        x = center_x + radius * math.cos(math.radians(angle))
        y = center_y + radius * math.sin(math.radians(angle))
        self.move_to(x, y)

class AnimationApp:
    def __init__(self, root):
        self.canvas = tk.Canvas(root, width=800, height=600, bg="black")
        self.canvas.pack()

        self.tierra = Tierra(self.canvas, 400, 300)
        self.luna = Luna(self.canvas, 500, 300)

        self.angle = 0
        self.animate()

    def animate(self):
        self.angle = (self.angle + 1) % 360
        self.luna.move_around(400, 300, 100, self.angle)
        self.canvas.after(50, self.animate)

if __name__ == "__main__":
    root = tk.Tk()
    app = AnimationApp(root)
    root.mainloop()
