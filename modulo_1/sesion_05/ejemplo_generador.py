from ejemplo_medir_tiempo import time_esto

"""
Generador que entrega n numeros
"""
def numeros(n:int)->int:
    for i in range(n+1):
        yield i

"""
Esto es una función clásica
"""
def altura(n:int)->None:
    for i in range(n):
        for j in numeros(i):
            print(j,end="")
            if j==i:
                print("")

import sys

if __name__=="__main__":
    with time_esto("triangulo"):
        valor = int(sys.argv[1])
        assert valor>0, f"Valor debe ser mayor a 0, no {valor}"
        altura(valor)


