from itertools import pairwise
import math
import cmath
def punto_medio(valores:list[float])->float:
    """
    Representa la creación de un proceso complejo, no podemos procesar
    todo de una vez
    """
    for a,b in pairwise(valores):
        yield math.fabs((a-b)/2)


def exponencial(valores:list[float])->float:
    """
    Representa la creación de un proceso complejo, no podemos procesar
    todo de una vez
    """
    for a,b in pairwise(valores):
        yield math.pow(a,b), cmath.exp(b * cmath.log(a))


import sys 
if __name__=="__main__":

    
    valores_str = sys.argv[1]
    valores = [float(valor.strip()) 
               for valor in valores_str.split(",")]
    #[-100, 4, -60, 33.2, 45, 1000]
    for resultado in punto_medio(valores):
        print(resultado)

    print("Exponenciales a^b")

    for resultado, resultado_c in exponencial(valores):
        print(resultado, resultado_c)
