from collections import Counter
from rich import print

texto = """
Esta era una clase de programación
en que aprendíamos un montón
"""

# pide un iterable, cada elemento lo considera una llave, y va contando
contar_letras = Counter(texto)

print(contar_letras)

otro_texto = "El Cristobal llegó tarde"
contar2= Counter(otro_texto)

contar_letras.update(contar2)


print(contar_letras)
