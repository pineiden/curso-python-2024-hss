from ejemplo_medir_tiempo import time_esto

archivo = "./notas.org"

def cat(filepath:str):
    with open(filepath, "r") as f:
        reader = f.readlines()
        for line in reader:
            print(line, end="")


import sys

if __name__=="__main__":
    filepath = sys.argv[1]
    with time_esto("leer-archivo"):
        cat(filepath)
