from collections import Counter
from rich import print

texto = """
Este es un ejemplo de contar las letras que contiene un texto
utilizando Counter, que es un tipo de mapping especial 
para el conteo de elementos
"""

conteo_letras = Counter(texto)

print(texto)

print(conteo_letras)

texto2 = """
Escribir historias es, en esencia, jugar. Quizá el trabajo se infiltre cuando el
escritor decide ponerse en serio, pero el proceso empieza casi siempre con un 
simple fingimiento. El principio de toda historia es un <<¿Qué pasaría si...?>>...
-Stephen King-
"""

conteo_letras2 = Counter(texto2)

print(texto2)

print(conteo_letras2)

print("TOTAL")

conteo_letras.update(conteo_letras2)

print(conteo_letras)
