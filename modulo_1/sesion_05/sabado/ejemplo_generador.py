from ejemplo_contexto import medir_duracion

"""
Se declara fn generador al usar 'yield'
"""
def numeros(n:int)-> int:
	for i in range(n+1):
		yield i

"""
Esto es poco óptimo en N grande
"""		
def numeros_clasica(n:int)-> int:
	nros = []
	
	for i in range(n+1):
		nros.append(i)
		
	return nros
		
		
		
"""
Una fn clásica que recibe el número
e imprime en la línea
"""

def altura(n:int)->None:
	for i in range(n):
		for j in numeros(i):
			print(j, end="")
			if j==i:
				print("")

import sys

if __name__=="__main__":
	with medir_duracion("triangulo"):
		valor = int(sys.argv[1])
		assert valor>0, f"Valor debe ser mayor a 0, no {valor}"
		altura(valor)
