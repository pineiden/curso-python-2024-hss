import time
from contextlib import contextmanager

@contextmanager
def medir_duracion(label:str):
	start = time.time()
	try:
		yield
	finally:
		end = time.time()
		print(f"El proceso {label} ha durado {end-start}")
		
		

if __name__=="__main__":
	with medir_duracion("ejemplo-while"):
		n = 100_000_000
		while n>0:
			n -= 1
