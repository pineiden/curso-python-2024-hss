from itertools import cycle
import time

numeros = [1,2,3,4,5]

"""
¿En qué numero caerá cuando haga la iteración 73? 
"""
counter = 1
for numero in cycle(numeros):
	print(numero)
	time.sleep(.1)
	
	if counter==73:
		break
		
	counter +=1
		
print(f"Para counter={counter}, cae en={numero}")
