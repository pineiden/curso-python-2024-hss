from ejemplo_contexto import medir_duracion

archivo = "../notas.org"

def cat(filepath:str):
	with open(filepath, "r") as f:
		reader = f.readlines()
		for linea in reader:
			print(linea,end="")


import sys

if __name__=="__main__":
	filepath = sys.argv[1]
	with medir_duracion("leer-archivo"):
		cat(filepath)
