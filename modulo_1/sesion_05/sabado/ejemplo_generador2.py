from itertools import pairwise
import math
import cmath
import time

def punto_medio(valores:list[float])->float:
	"""
	Representar la creación de un proceso complejo
	no podemos procesar todo a la vez
	"""
	for a,b in pairwise(valores):
		yield a,b,math.fabs((b-a)/2)
		
		
		
def exponencial(valores:list[float])->float:
	"""
	Representar la creación de un proceso complejo
	no podemos procesar todo a la vez	
	"""		
	for a,b in pairwise(valores):
		yield a, b, cmath.exp(b * cmath.log(a))
	
		
import sys

if __name__=="__main__":
	"""
	input: 1,2,-100, 33.4,56.1
	"""
	valores_str = sys.argv[1]
	valores = [float(v.strip()) for v in valores_str.split(",")]

	for resultado in punto_medio(valores):
		print(resultado)

	time.sleep(2)
	print("Exponenciales  a^b")
	
	for resultado in exponencial(valores):
		print(resultado)
