from itertools import groupby
from rich import print


personas = [
	{"nombre":"Juan", "mascota":"perro"},
	{"nombre":"Daniela", "mascota":"gato"},
	{"nombre":"Mario", "mascota":"perro"},
	{"nombre":"Sebastian", "mascota":"loro"},
	{"nombre":"Valeria", "mascota":"gato"},
	{"nombre":"Raj", "mascota":"perro"},
	{"nombre":"Felipe", "mascota":"oveja"},
	{"nombre":"Emilio", "mascota":"perro"},
	{"nombre":"Marta", "mascota":"gallina"},
]


ordernadas = sorted(
	personas,
	key=lambda item:item["mascota"]
)

agrupacion = {}

"""
Equivalente a definir lambda:
lambda item:item["mascota"]
"""
def criterio(item):
	return item["mascota"]

for mascota, grupo in groupby(
	ordernadas, 
	key=criterio):
	agrupacion[mascota] = list(grupo)
	
print(agrupacion)
