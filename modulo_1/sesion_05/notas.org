#+TITLE: Sesión 5, curso de programación.

* collections

Este módulo contiene objetos especializados en manejar colecciones de
datos basadas en las estructuras básicas de python:

- list
- tuple 
- dict 
- set

Objetos especiales:

- deque :: una lista inversa que puede operar como cola
- Chainmap :: permite unir un conjunto de maps (diccionarios)
- Counter :: uno de los más usados, permite contar elementos en un
  iterable. Hacer operaciones entre Counters tb.

¿Cómo se podría hacer el caso de contar caracteres de un texto usando Counter?

Con *deque* se puede trabajar como una lista bidireaccional. Poner
elementos al inicio o al final. 

- defauldict :: permite construir un diccionario en que por defecto
  cada valor sea creado por una función, como {llave:lista[valores]}
  Sirve mucho para incluir valores que corresponden a una misma
  llave. Por ejemplo, contar por separado las letras de dos textos
  distintos y acumular de manera ordenada cada cantidad.

- namedtuple :: sirve para definir de manera compacta una clase y sus
  atributos principales.

* itertools

Este módulo provee herramientas funcionales para operar con
iteradores.

Cómo:

- count() :: para contar indefinidamente cada p pasos.
- cycle([1,2,3]) :: para tomar valores de manera circular en una
  secuencia.
{1,2,3,1,2,3.....,1,2,3}

- repeat(N,K) :: repetir un elemento N K veces

De uso muy útil *groupby*.

Para agrupar según criterios.

- batches :: Para separar una secuencia de largo N en grupos de K valores:

[1,2,3,4,5,6]-(k=2)-> {1,2} {3,4} {5,6}

- dropwhile(fn, iterable) :: para eliminar de una secuencia dada una condición.

- takewhile(fn, iterable) :: para tomar de una secuencia dada una condición.
 
- paiwise(iterable) :: aparear de manera encadenada

- tee(iterable, n) :: generara n iteradores en base a iterable

** Iteradores para Combinatoria

- product :: es equivalente a la operación anidada de for
- permutations(p, r) :: entrega tuplas de largo 'r', en todos los
  ordenes posibles, SIN REPETIR elementos 
- combinations(p,r) ::  entrega tuplas de largo 'r', ordenadas, sin elementos repetidos
- combinations_with_replacement(p,r) :: entre tuplas de largo r, en
  orden, CON elementos repetidos.

Tarea :: 

Estudiar la documentación y ejecutar los ejemplos:
https://docs.python.org/3/library/itertools.html

* generadores 

http://www.dabeaz.com/
https://dabeaz-course.github.io/practical-python/
https://github.com/dabeaz-course/python-mastery

Es un tipo especial de *funciones* que permiten construir una máquina
que genera valores y los entrega a medida que son necesarios,
permitiendo ahorrar memoria y mejorar el rendimiento de los programas.


Ver esto:
https://www.youtube.com/watch?v=5-qadlG7tWo

http://www.dabeaz.com/coroutines/

Uso de *yield*  para crear funciones generadores, en vez de usar *return*

- ejemplo: cuenta regresiva

Permite crear *pipelines*, operaciones encadenadas.

Permite *enviar* datos al objeto generador para recibir valors.

Esto habilita la construcción de un sistema de mensajería.

Recibir:

#+begin_src python
item = yield
#+end_src

Entregar:

#+begin_src python
yield item
#+end_src

Es un objeto que se puede cerrar.

Uso de *yield from* permite llamar dentro del generador y recibir
desde otro generador datos (Delegar).

#+begin_src python
def func():
    result =  yield from generador()
#+end_src


https://es.stackoverflow.com/questions/313174/funcionamiento-de-yield-from


* contextos

https://docs.python.org/3/library/contextlib.html

Creamos un context manager para contar el tiempo.

#+begin_src python :results output
import time
from contextlib import contextmanager

@contextmanager
def timethis(label):
    start = time.time()
    try:
        yield
    finally:
        end = time.time()
        print(f"{label}: {end-start}")


with timethis('counting'):
    n = 10000
    while n>0:
        n -= 1
#+end_src

#+RESULTS:
: 
: counting: 0.00038361549377441406
: 

* lectura y escritura de archivos

El trabajo con archivos nos permite comprender la información como
elementos que pueden almacenarse y ocuparse a medida que uno los
necesite.

Para dominar este tema es necesario conocer sobre:

- permisos de lectura/escritura
- rutas de archivos
- contextos
- generadores

1) Todo lo que se abre debe cerrarse:

- lectura/escritura de archivos
- conexiones a sockets
- conexiones a bases de datos
- etc

Iniciando con:

https://docs.python.org/es/3.9/library/io.html


Al *abrir* un archivo se crea un objeto de tipo *stream* o flujo de
bytes.

#+begin_src python
f = open("archivo.txt", "r", encoding="utf-8")
#+end_src

O bien, directamente un texto en memoria convertido  *stream*

#+begin_src python
import io

f = io.StringIO("Algún texto especial en memoria")
#+end_src

Si lo que tratamos son *bytes* o forma binaria de información, la
expresión cambia a uso de *b*

#+begin_src python
f = open("imagen.jpg", "rb", encoding="utf-8")
#+end_src

O bien, directamente un texto en memoria convertido  *stream*

#+begin_src python
import io

f = io.StringIO(b"Algún texto especial en memoria")
#+end_src

Acciones sobre archivos:

- w :: escribir 
- wa :: añadir y escribir
- r :: leer
- wb :: escribir bytes
- rb :: leer bytes

Para acceder a un archivo de manera segura, sea lectura o escritura,
se abre un contexto:

#+begin_src python
with open('spam.txt', 'w') as file:
    file.write('Spam and eggs!')
#+end_src

De otra manera, será necesario *cerrar* la operación.

#+begin_src python
f.close()
#+end_src

Operaciones sobre el archivo:

- readlines :: crea un generador para leer linea a línea
- seek :: define un byte específico para ubicar el punto de acción (r/w)
- seekable :: booleano True si el stream puede accederse de manera aleatoria
- tell :: entrega posicion actual del stream
- writable :: si el stream soporta escritura
- writelines :: escribe lineas 

* ¿Cómo leer/escribir un csv?

- leyendo un archivo directamente y convirtiendo
- usando *csv*
- usando *csv.DictWriter/DictReader*


https://docs.python.org/3/library/csv.html

* Ejercicio final de clase.

** Planteamiento

Desde la oficina de agricultura se ha decidido subsidiar en maquinaria a aquellos agricultores
que tienen un registro de crecimiento anual de hasta un 15%, se tiene la lista
de los agricultores y la lista de su crecimiento promedio.

¿A quienes se subsidiaría?

** Tabla de crecimiento anual
 
|----+-----------+------------------+----------|
| id | Nombre    | Tasa Crecimiento | Subsidia |
|----+-----------+------------------+----------|
|  1 | Martinez  |                8 |        1 |
|  2 | Soto      |              6.5 |        1 |
|  3 | Osorio    |              9.3 |        1 |
|  4 | Pineda    |               33 |        0 |
|  5 | Jara      |               42 |        0 |
|  6 | Hernandez |                5 |        1 |
|  7 | Acuña     |               12 |        1 |
|  8 | Oña       |               54 |        0 |
|  9 | Perez     |                3 |        1 |
| 10 | Nuñez     |               60 |        0 |
|----+-----------+------------------+----------|
#+tblfm: $1=@#-1::$4=$3<=15

** Explicación


Los primeros 10 agricultores tienen una tasa descrita en la tabla, pero hay 1000
en la lista. Nuestro operador se demoró un día en evaluar estos primeros diez y
tiene hasta este mismo viernes para entregar los resultados de todos.

¿Lo podremos ayudar?

** Diseño


1.- Identificar con un número a cada agricultor (entero, secuencial)

2.- Ordenar el valor de cada agricultor con su tasa de crecimiento (en una
tupla)

3.- Asociar todo en una lista 

5.- Aplicar en un filtro

** Ordenar en lista de tuplas


#+BEGIN_SRC python
agricultores=[
  ("Martinez",8 ),
  ("Soto",6.5 ),
  ("Osorio",9.3),
  ("Pineda",33),
  ("Jara",42),
  ("Hernandez",5),
  ("Acuña",12),
  ("Oña",54),
  ("Perez",3),
  ("Nuñez",60)
]
#+END_SRC

** Imprimir la lista de tuplas


#+BEGIN_SRC python
for agricultor, tasa in agricultores:
  print(f"Para {agricultor} la tasa es {tasa}")
#+END_SRC

** Definir el filtro


En caso que la tasa sea inferior al 15% recibirá el subsidio.

#+BEGIN_SRC python
tasa_limite = 15
for agricultor, tasa in agricultores:
  print(f"Para {agricultor} la tasa es {tasa}")
  if tasa<=tasa_limite:
    print(f"Agricultor {agricultor} recibe el subsidio")
#+END_SRC

** Problema, subsidio inversamente proporcional


Es decir, para un 0% de crecimiento un 90% de subsidio, y para un 15% de
crecimiento un 20% de subsidio.

[[file:./img/subsidio.png]]

** Implementación subsidio IP


#+BEGIN_SRC python
if tasa<=tasa_limite:
  print(f"Agricultor {agricultor} recibe el subsidio")
  subsidio = -(70/15)*tasa+90
  print(f"Recibirá un subsidio del {subsidio} %")
#+END_SRC

** Aproximar decimales


Necesitamos, para mayor claridad de los agricultores, asignar solo valores
enteros para los subsidios, por lo que aplicaremos una función que transforma el
valor al entero superior inmediato, utilizando *math.ceil*

#+BEGIN_SRC python
import math
.
.
if tasa<=tasa_limite:
  print(f"Agricultor {agricultor} recibe el subsidio")
  subsidio = math.ceil(-(70/15)*tasa+90)
  print(f"Recibirá un subsidio del {subsidio} %")
#+END_SRC

** Implementando la solución


Se hace una revisión de la tasa de crecimiento de cada agricultor y se agrupan
los que si podrían recibir subsidio.

#+BEGIN_SRC python
for agricultor, tasa in agricultores:
    print(f"Para {agricultor} la tasa es {tasa}")        

tasa_limite = 15
nombres = []
subsidios = []
for agricultor, tasa in agricultores:
    print(f"Para {agricultor} la tasa es {tasa}")
    if tasa<=tasa_limite:
        nombres.append(agricultor)
        print(f"Agricultor {agricultor} recibe el subsidio")
        subsidio = math.ceil(-(70/15)*tasa+90)
        print(f"Recibirá un subsidio del {subsidio} %")
        subsidios.append(subsidio)

agricultores_seleccion = list(zip(nombres, subsidios))
#+END_SRC

** Incentivo a la asociatividad


Es posible que hasta tres agricultores se asocien en la inversión de maquinaria,
el subsidio asociada otorgado será la mayor de las tres sumando el 50% de las
otras dos. 

Es decir, 

- $subsidio_{asoc} = subsidio_a+(subsidio_b+subsidio_c)*.5$

¿Qué podemos hacer?

- Una opción es esperar que se asocien naturalmente
- Otra opción es calcular las mejores combinaciones y recomendarlas

** El uso de itertools


Una vez que ya sabemos utilizar listas, tuplas y strings; podemos pasar a
utilizar algunas herramientas muy útiles para la gestión de estas estructuras de
datos.

- itertools :: https://docs.python.org/3.8/library/itertools.html

Nos permite tomar nuestro *iterable* y combinar, permutar o agrupar, entre otras
cosas. Resultado un objeto *iterador* que nos permite operar secuencialmente.

** Permutar con itertools


Necesitamos conocer todas las combinaciones posible de una lista de nombres, en
listas de 2 y de 3 posibilidades.

#+BEGIN_SRC python
from itertools import permutations
nombres = ["David", "Raquel", "Juanin"]
comb_2 = list(permutations(nombres, 2))
print("Permutaciones de 2", comb_2)
comb_3 = list(permutations(nombres, 3))
print("Permutaciones de 3", comb_3)
#+END_SRC

** Operar elementos en el iterador


Cómo el objeto que retorna es un *iterador*, que entrega en cada iteración un
nuevo elemento, necesitamos mostrar el primer nombre en mayúsculas en
combinaciones de a 3.

#+BEGIN_SRC python :results output
from itertools import permutations


nombres = ["David", "Luisa", "Juanin"]
comb_3 = [(a.upper(), b, c) for a, b, c in permutations(nombres, 3)]
print("Permutaciones de 3", comb_3)
#+END_SRC

#+RESULTS:
: Permutaciones de 3 [('DAVID', 'Luisa', 'Juanin'), ('DAVID', 'Juanin', 'Luisa'), ('LUISA', 'David', 'Juanin'), ('LUISA', 'Juanin', 'David'), ('JUANIN', 'David', 'Luisa'), ('JUANIN', 'Luisa', 'David')]
: 

** Combinar con itertools


Las *combinaciones* consiste en la lista de todos los conjuntos diferentes que
se pueden crear al combinar una serie de elementos.

#+BEGIN_SRC python :results output
from itertools import combinations
nombres = ["David", "Analí", "Juanin"]
comb_2 = list(combinations(nombres, 2))
print("Combinaciones de 2", comb_2)
comb_3 = list(combinations(nombres, 3))
print("Combinaciones de 3", comb_3)
#+END_SRC

#+RESULTS:
: Combinaciones de 2 [('David', 'Analí'), ('David', 'Juanin'), ('Analí', 'Juanin')]
: Combinaciones de 3 [('David', 'Analí', 'Juanin')]
: 

** Solución ¿Cómo mostrar las mejores combinaciones para los agricultores?


Una solución podría ser:

- Realizar las combinaciones de los agricultores
- Por cada grupo de combinación que contenga como líder al mismo agricultor,
  entregar la tasa más alta, y los dos siguientes.

** Implementando la solución.


Realizando las permutaciones y agrupando por líder (de mayor subsidio)

#+BEGIN_SRC python
from itertools import permutations
from itertools import groupby

.
.

print(len(nombres), len(subsidios))
[print(n,t) for n,t in zip(nombres,subsidios)]
# obtener permutaciones de seleccion con subsidio
permutaciones = list(permutations(nombres, 3))

# se define funcion para agrupar según líder
def getnombre(tupla):
    return tupla[0]

def getsubsidio(tupla):
    return tupla[1]


# se inicializan las listas
grupos=[]
llaves_grupos=[]

# se agrupa por agricultor lider
for agr_name, group_iter in groupby(permutaciones, key=getnombre):
    grupos.append(list(group_iter))
    llaves_grupos.append(agr_name)

print("Agrupaciones")
[print(n,g) for n,g in zip(llaves_grupos,grupos) ]
#+END_SRC

** Definimos funciones que filtran y seleccionan


Se crean funciones que filtran y seleccionan elementos desde una lista.

#+BEGIN_SRC python    
def subsidio(nombre, agricultores_seleccion):
    for agr, tasa in agricultores_seleccion:
        if nombre==agr:
            return agr, tasa

def subsidio_grupo(grupo, agricultores_seleccion):
    subsidio_group = [subsidio(nombre, agricultores_seleccion) 
                              for nombre in grupo]
    return grupo, 
          subsidio_group[0][1]+sum(
                  [agr_sbs[1] for agr_sbs in subsidio_group[1:]])*.5

def mejores_select(subsidios, n=3):
    sbs_ordenadas=sorted(subsidios, key=getsubsidio, reverse=True)
    count = 0
    past_sbs = 0 
    tos = []
    for group, sbs in sbs_ordenadas:
        if (sbs > past_sbs or sbs < past_sbs) and count>n:
            break
        elif sbs==past_sbs or count>0:
            tos.append((group, sbs))
        past_sbs = sbs            
        count += 1            
    return tos
#+END_SRC

El uso de *break* permite detener la operación de la iteración y continuar con
el código que sigue.

** Aplicamos las funciones


Se utilizan las funciones y se muestra la selección de los mayores subsidios por agricultor.

#+BEGIN_SRC python        
subsidios_agrupados = []
for pos, nombre in enumerate(llaves_grupos):
    subsidios_agrupados.append(
        mejores_select(
            [subsidio_grupo(grupo, agricultores_seleccion) for grupo in grupos[pos]]
            )
        )

print(len(nombres), len(subsidios))
[print(n,t) for n,t in zip(nombres,subsidios)]
# Resultadoas
for pos, nombre in enumerate(llaves_grupos):
    print("Agricultor ", nombre)
    for group, val in subsidios_agrupados[pos]:
        print("Tasa subsidio:", val)
        print("Grupo: ", group)
#+END_SRC


