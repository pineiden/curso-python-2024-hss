import time
from contextlib import contextmanager


@contextmanager
def time_esto(label:str):
    start = time.time()
    try:
        yield
    finally:
        end  = time.time()
        print(f"Proceso: {label}, demoró: {end-start}")


if __name__=="__main__":
    with time_esto("contar-op"):
        n = 100000000
        while n>0:
            n -= 1
