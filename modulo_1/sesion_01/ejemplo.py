import sys

persona = {
    "nombre":"Danilo",
    "acciones":[]
}


botella = {
    "volumen":1000
}

def vaciar(persona, botella, cuanto:int=0):
    accion = ("vaciar", botella, cuanto)
    botella["volumen"] -= cuanto


if __name__=="__main__":
    cuanto = int(sys.argv[1])
    print("Antes", botella)
    vaciar(persona, botella, cuanto)
    print("Despues", botella)



