import sys
import copy
from rich import print


def sumar_todo(*args):
	print(args)
	result = 0
	for elem in args:
		result += elem
	return result
	
	
def opciones(nombre:str="Juan", apellido:str="Soto", **kwargs):
	item = copy.deepcopy(kwargs)
	item["nombre"] = nombre
	item["apellido"] = apellido
	return item

if __name__=="__main__":
	args = [float(s.strip()) for s in sys.argv[1].split(",")]
	resultado = sumar_todo(*args)
	
	res2 = 0
	for elem in args:
		res2 += sumar_todo(elem)
	
	print(resultado, res2)

	datos = {
		"mascota":"perro",
		"ciudad":"Valparíso",
		"hermano":["Daniela", "Pericles"]
	}
	
	item = opciones(apellido="Gonzalez",**datos)
	print(item)
	
	item = opciones(nombre="David", **datos)
	
	print(item)
