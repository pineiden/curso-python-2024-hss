"""
Vamos a crear un buscador de texto en un directorio
"""


from pathlib import Path
from rich import print
from collections import namedtuple

Grep = namedtuple(
	'Grep', 
	['linea', 'texto', "posicion", "archivo"]
	)

base = Path(__file__).parent
ruta = base / "clase" / "archivos"

#print(ruta, ruta.exists())

def listar(ruta, patron:str="*.csv")->Path:
	for item in ruta.glob(patron):
		yield item

def busqueda(ruta:Path, texto:str, patron:str="*.csv")->Grep:
	for item in listar(ruta, patron):
		with item.open("r") as f:
			for i, linea in enumerate(f.readlines()):
				if texto in linea:
					yield Grep(	
							i+1, 
							linea, 
							linea.find(texto),
							item)

if __name__=="__main__":					
	texto = input("Entrega texto a buscar ")
					
	for resultado in busqueda(ruta, texto):
		print(resultado)




