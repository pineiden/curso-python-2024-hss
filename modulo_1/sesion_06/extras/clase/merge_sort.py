import math

inf = float('inf')

def merge(A, p, q, r):
    # slice left
    # slice hasta posición q
    L = A[p:q+1]
    L.append(inf)
    # slice right
    # slice desde posicion q+1 hasta r
    R = A[q+1:r+1]
    R.append(inf)
    # counters
    i=0
    j=0
    for k in range(p,r+1):
        if L[i] <= R[j]:
            A[k] = L[i]
            i += 1
        else:
            A[k] = R[j]
            j += 1

def merge_sort(A, p, r):
    """
    A :: list to order
    p :: initial index
    r :: final index
    """
    if p < r:
        q = int(math.floor((p+r)/2))
        merge_sort(A, p, q)
        merge_sort(A, q+1, r)
        merge(A, p, q, r)

def gen_args(A):
    return A, 0, len(A)-1

if __name__ == "__main__":
    lista = [3,-5, 8, 9, 50, 33,1000,-34532]
    print("lista original", lista)
    merge_sort(*gen_args(lista))
    print("lista ordenada", lista)
