# algoritmo para 
# lectura y caĺculo 
# de parámetros estadísticos

"""
Objetivo:

Calcular promedio y varianza de temperatura

"""

# Leemos el archivo y mostramos que tiene

archivo_ruta = "./mes_temp.csv"
with open(archivo_ruta, 'r') as archivo:
    for line in archivo.readlines():
        print(line)

"""
Observamos cómo está hecho,
sus columnas son:

nr_dia,dia_semana,temperatura
"""

temperaturas = []
import csv
with open(archivo_ruta, 'r') as archivo:
    lector =  csv.DictReader(archivo, delimiter=',')
    for fila in lector:
        temperaturas.append(fila)

"""
Creamos las funciones que calculan el promedio
y varianza, dado que:
Se entrega una lista de diccionarios,
cada diccionario tienee los campos: número, día, temperatura°[C]
"""
import math

campo_valor="temperatura°[C]"
get_valor = lambda e: float(e.get(campo_valor,0))

def promedio(temperaturas):
    valores = list(map(get_valor, temperaturas))
    promedio = sum(valores)/len(temperaturas)
    return promedio


def varianza(promedio, temperaturas):
    valores = list(map(get_valor, temperaturas))
    diferencias_elev2 = [(valor-promedio)**2 for valor in valores]
    std  = math.sqrt(sum(diferencias_elev2)/len(temperaturas))
    return std

"""
Utilizamos las funciones con los valores 
que obtuvimos del archivo

"""

mu =  promedio(temperaturas)
dev_std =  varianza(mu, temperaturas)


print("La temperatura promedio es:" , mu, "°[C]")
print("La variabilidad de la temperarura es: ", dev_std)

