def fibonnaci(n):
    if n==1:
        return 1
    elif n==0:
        return 0
    return fibonnaci(n-1)+fibonnaci(n-2)

print(fibonnaci(10))
[print(fibonnaci(n)) for n in range(15)]

print("="*30)
print("Fibonacci generador")


def fibonnaci_gen(n):
    if n==1:
        yield 1
    elif n==0:
        yield 0
    yield next(fibonnaci_gen(n-1))+next(fibonnaci_gen(n-2))

for i in range(1,120):
    for f in fibonnaci_gen(i):
        print(f)

