

url="http://www.historicas.unam.mx/publicaciones/publicadigital/libros/apariciones/celestiales.html"
url_list= url.split("/")
base_url = "/".join(url_list[:-2])

import urllib.request
"""
Leer desde la web y descargar el texto html
"""
def read_web(url):
    with urllib.request.urlopen(url) as f:
        web=f.read().decode('utf-8')

        """
        Guardamos a un archivo
        """
        with open("libro.html","w") as libro:
            libro.write(web)

"""
Transformar a una estructura de objetos
"""
from bs4 import BeautifulSoup
# activamos expresiones regulares
import re
pdf = re.compile("pdf$")
print("Regex", pdf)
# con esto buscaremos coincidencias de los archivos pdf

with open("libro.html","r") as libro:
    web=libro.read()
    soup  = BeautifulSoup(web,features="html.parser")
    main = soup.html.body.main 
    main_table = main.table

    for row in main_table.findAll("tr"):
        cells = row.findAll("td")
        row_file = cells[-1]
        link =  row_file.find("a")
        if link:
            """
            Si es enlace <a>--> obtener atributo href
            """
            href = link.attrs.get("href").strip()
            if re.search(pdf, href):
                print("href", href)
                url_file = "%s/%s" %(base_url, href)
                print("URL:", url_file)
                # guardar archivo
                print("Descargando...")
                urllib.request.urlretrieve(url_file, './libro/%s' % href)
