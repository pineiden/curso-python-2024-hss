def insertion_sort(lista):
    """
    Este algoritmo opera sobre la misma lista
    no crea otra lista
    """
    largo =  len(lista)
    for j in range(1,largo):
        # se obtiene valor en la posición
        valor =  lista[j]
        # indice anterior a j
        i = j - 1
        # se itera desde j-1 hasta 0, comparando
        # con el valor reservado en posición j
        while i >= 0 and lista[i] > valor:
            # si ocurre, el valor en la lista
            # del índice superior, pasa a ser el de este valor
            lista[i+1] = lista[i]
            # se resta uno al índice i, hasta llegar a 0
            # o cuando lista[i]<=valor
            i = i - 1
        # se vuelve a poner en la posición superior el 
        # valor reservador
        lista[i+1] = valor
    
def insertion_sort_decreciente(lista):
    """
    Este algoritmo opera sobre la misma lista
    no crea otra lista
    """
    largo =  len(lista)
    for j in range(largo-1,-1,-1):
        valor =  lista[j]
        #indice anterior
        i = j + 1
        while i < largo and lista[i] < valor:
            lista[i - 1] = lista[i]
            i = i + 1
        lista[i-1] = valor
     
    
if __name__ == "__main__":
    lista = [3,-5, 8, 9, 50, 33,1000,-34532]
    print("lista original", lista)
    insertion_sort(lista)
    print("lista ordenada", lista)

