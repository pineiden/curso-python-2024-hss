from datetime import datetime


# tiempo de ahora:
now = datetime.now()

dt1 = datetime(year=2015, month=1, day=30)


params = {
	"year": 2015,
	"month": 1,
	"day": 30
}

dt2 = datetime(**params)

print(dt1, dt2)
