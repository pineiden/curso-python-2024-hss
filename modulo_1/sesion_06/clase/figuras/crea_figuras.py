import random
import time
import queue
import math


"""
Creación de figuras
"""

# funcion lambda genera valores aleatorios
lado = lambda x: random.uniform(0.1,100)
# tupla de nombres de lados
names = ('a','b', 'c', 'd', 'e')

def triangulo():
    """
    Triángulo de lados diferentes
    """
    a = lado(0)
    b = lado(0)
    c = random.uniform(abs((a-b)), (a+b))
    lados = [a,b,c]
    return dict(zip(names, lados))
    
def cuadrado():
    """
    Cuadrado: todos los lados iguales
    """
    lado_c = lado(0)
    lados = [lado_c]*2
    return dict(zip(names,lados))
    
def pentagono():
    """
    Pentagono de lados iguales
    """
    lado_c = lado(0)
    lados = [lado_c]*5
    return dict(zip(names,lados))


"""
Función con un selector que crea figura en base al nombre 
de la figura
"""    
def crea_parametros(figura):
    figuras = dict(
        triangulo=triangulo, 
        cuadrado=cuadrado, 
        pentagono=pentagono)
    seleccion =  figuras.get(figura, triangulo)
    return seleccion()
    
    
"""
Función que crea una figura aleatoreamente
"""
def genera_figura():
    nombre =  random.choice((
                "triangulo", 
                "cuadrado", 
                "pentagono"))
    figura = {}
    figura["lados"] = crea_figura(nombre)
    figura["nombre"] = nombre
    return figura
    
"""
Función generadora para leer mensajes de una cola
"""
def read_queue(queue):
    if not queue.empty():
        for i in range(queue.qsize()):
            msg = queue.get()
            yield msg


"""
Calculos de área y perímetros
"""
def ap_triangulo(**figura):
    a = figura["lados"].get('a')
    b = figura["lados"].get('b')
    c = figura["lados"].get("c")
    perimetro = a+b+c
    s = perimetro / 2
    area =  math.sqrt(s*(s-a)*(s-b)*(s-c))
    return {'area':area, 'perimetro':perimetro}

def ap_cuadrado(**figura):
    a = figura["lados"].get('a')
    b = figura["lados"].get('b')
    return {'area':a*b, 'perimetro':2*(a+b)}
    
def ap_pentagono(**figura):
    a = figura["lados"].get('a')
    b = figura["lados"].get('b')
    c = figura["lados"].get("c")
    d = figura["lados"].get("d")
    e = figura["lados"].get("e")
    perimetro = a+b+c+d+e
    apotema = a / (2*math.tan(math.radians(36)))
    area = 5*a*apotema/2
    return {'area':area, 'perimetro':perimetro}


"""
Función que calcula área y perímetro dados
los datos de la figura
"""
def calcula_area_perimetro(figura):
    nombre = figura.get('nombre')
    ap_funciones = {
        'triangulo':ap_triangulo,
        'cuadrado':ap_cuadrado,
        'pentagono':ap_pentagono    
    }
    ap = ap_funciones.get(nombre)(**figura)
    ap["nombre"]=nombre
    return ap


"""
Función que toma la cola y lee la figura
Calcula área y perímetro, retorna resultado 
a la otra cola
"""
def area_perimetro(q_fc, q_cf):
    for figura in read_queue(q_fc):
        ap = calcula_area_perimetro(figura)
        q_cf.put(ap)

"""
Es una función generadora que opera como
fuente de figuras, las envía a la cola, 
lee la cola de resultados
"""
def crea_figuras_pre(q_cf):
    while True:
        figura = genera_figura()
        print("Figura", figura)
        yield figura
        time.sleep(3)
        for ap in read_queue(q_cf):
            print("Resultado", ap)

import csv

"""
Es una función generadora que opera como
fuente de figuras, las envía a la cola, 
lee la cola de resultados y guarda en
un archivo csv
"""

def crea_figuras(q_cf):
    with open("figuras.csv", "w") as figuras_file:
        writer =  csv.DictWriter(figuras_file, 
                        fieldnames=["nombre","lados","area","perimetro"])
        writer.writeheader()        
        while True:
            figura = genera_figura()
            print("Figura", figura)
            yield figura
            time.sleep(3)
            for ap in read_queue(q_cf):
                print("Resultado", ap)
                if figura.get("nombre") == ap.get("nombre"):
                    figura.update(ap)
                    print("Guardando...", figura)
                    writer.writerow(figura)

"""
Script de ejecución de la figura
"""
if __name__ == '__main__':
    q_fc = queue.Queue()
    q_cf = queue.Queue()
    
    for figura in crea_figuras(q_cf):
        q_fc.put(figura)
        area_perimetro(q_fc, q_cf)
