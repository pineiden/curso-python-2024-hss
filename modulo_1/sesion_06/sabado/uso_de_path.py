from pathlib import Path
from rich import print
from typing import Tuple, Any
from collections import namedtuple
import sys

Grep = namedtuple("Grep",["path", "fila", "columna", "linea"])

# ruta base
base = Path(__file__).parent


def listar(
        ruta:Path, 
        patron:str="*.csv")->Path:
    #print(ruta.parent, ":")
    for item in ruta.glob(patron):
        #print(item.name)
        yield item

def busqueda(
        ruta:Path,
        texto:str,
        patron:str="*.csv"
)->Grep:
    for csv_path in listar(ruta, patron):
        with csv_path.open("r") as f:
            for i, linea in enumerate(f.readlines()):
                if texto in linea:
                    yield Grep(
                        csv_path, 
                        i, 
                        linea.find(texto), 
                        linea)

if __name__=="__main__":
    ruta =  base / "clase" / "archivos"
    patron = "*.csv"
    texto = sys.argv[1]
    for resultado in busqueda(ruta, texto):
        print(resultado)
