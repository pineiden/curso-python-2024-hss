import random
from typing import Dict, List
import time
import queue
import math
from rich import print

lado = lambda: random.uniform(.1, 100)

names = ("a","b","c","d","e")

Figura = Dict[str, float]

def triangulo()->Figura:
    a = lado()
    b = lado()
    c = random.uniform(abs(a-b), a+b)
    lados = (a,b,c)
    return dict(zip(names, lados))


def cuadrado()->Figura:
    a = lado()
    lados = (a,a,a,a)
    return dict(zip(names, lados))

def pentagono()->Figura:
    a = lado()
    lados = tuple([a for _ in range(5)])
    return dict(zip(names, lados))


def crea_paremetros(figura:str)->Figura:
    operaciones = {
        "triangulo": triangulo,
        "cuadrado": cuadrado,
        "pentagono": pentagono
    }
    try:
        funcion = operaciones[figura]
        return funcion()
    except Exception as e:
        print("Exception ",e)
        raise e


def genera_figura()->Figura:
    nombre = random.choice((
        "triangulo", 
        "cuadrado", 
        "pentagono"))
    figura = {"nombre":nombre}
    figura["lados"] = crea_paremetros(nombre)
    return figura


def crea_figuras(cola_cf:queue.Queue):
    while True:
        yield genera_figura()
        time.sleep(3)
        for msg in read_queue(cola_cf):
            print("Resultado", msg)


def read_queue(cola):
    if not cola.empty():
        for _ in range(cola.qsize()):
            msg = cola.get()
            yield msg


APFigura = Dict[str, float]
def ap_triangulo(**figura)->APFigura:
    a = figura["lados"].get("a")
    b = figura["lados"].get("b")
    c = figura["lados"].get("c")
    perimetro = a+b+c
    s = perimetro / 2
    area = math.sqrt(s*(s-a)*(s-b)*(s-c))
    return {"area":area, "perimetro":perimetro}


def ap_cuadrado(**figura)->APFigura:
    a = figura["lados"].get("a")
    return {"area":a**2, "perimetro":4*a}

def ap_pentagono(**figura)->APFigura:
    a = figura["lados"].get("a")
    perimetro = 5*a
    radianes = math.radians(36)
    a_triangulo = (a**2) / (4*math.tan(radianes))
    area = 5 * a_triangulo 
    return  {"area":area, "perimetro": perimetro}


def calcula_area_perimetro(
        figura:Figura)->APFigura:
    nombre = figura.get("nombre")
    ap_funciones = {
        "triangulo":ap_triangulo,
        "cuadrado":ap_cuadrado,
        "pentagono":ap_pentagono
    }
    ap = ap_funciones.get(nombre)(**figura)
    ap["nombre"] = nombre
    return ap


def area_perimetro(cola_fc, cola_cf):
    for figura in read_queue(cola_fc):
        ap = calcula_area_perimetro(figura)
        cola_cf.put(ap)


def crea_figuras(cola_cf):
    while True:
        figura = genera_figura()
        yield figura 
        time.sleep(3)
        for msg in read_queue(cola_cf):
            print("Resultado", msg)


if __name__=="__main__":
    cola_fc = queue.Queue()
    cola_cf = queue.Queue()
    
    for figura in crea_figuras(cola_cf):
        cola_fc.put(figura)
        area_perimetro(cola_fc, cola_cf)
        
