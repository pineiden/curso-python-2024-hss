from pathlib import Path
import time

buscar="precipitacion"
ruta =  Path('./archivos')
print("String de ruta_>",str(ruta))
#windows
#ruta =  Path('.\archivos')

# listar los csv
if ruta.exists():
    archivos = []
    generador = ruta.glob("*.csv")
    print(generador)
    seleccion = set()
    seleccion_archivos = set()
    for archivo in generador:
        print(archivo)
        archivo_str = str(archivo)
        if archivo_str.find(buscar) >= 0:
            seleccion_archivos.add(archivo)
        if archivo.is_file():
            archivos.append(archivo)
            with archivo.open() as f:
                nro = 1
                for linea in f.readlines():
                    coincidencia = linea.find(buscar)
                    seleccion.add((nro, archivo))
                    nro += 1
        elif archivo.is_dir():
            print("La ruta a %s es un directorio" %archivo)
    print("Resultados de la búsqueda")
    if seleccion_archivos:
        print("En nombres de archivos, buscar %s" %buscar)
        [print(archivo) for archivo in seleccion_archivos]
    if seleccion:
        print("En contenidos de archivos, buscar %s" %buscar)
        for nro_archivo in seleccion:
            time.sleep(1)            
            print("Linea %d, archivo %s" %nro_archivo)             
else:
    print("No existe esa ruta")
