import sys
from typing import Dict, Any
import copy
from rich import print

def sumar_todo(*args):
    print(args)
    result = 0
    for elem in args:
        result += elem
    return result


def opciones(
        nombre:str="Juan",
        apellido:str="Soto",
        **kwargs
)->Dict[str, Any]:
    item = copy.deepcopy(kwargs)
    item["nombre"] = nombre
    item["apellido"] = apellido
    return item


if __name__=="__main__":
    
    args = [
        float(nro.strip()) 
        for nro in sys.argv[1].split(",")
    ]
    

    resultado = sumar_todo(*args)

    print("Resultado",resultado)

    equivalencia = sum(args)
    print("Equivalencia", equivalencia)
    resultado2 = 0
    for elem in args:
        resultado2 += sumar_todo(elem)
        
    print("Resultado2", resultado2)


    datos = {
        "mascota":"perro",
        "ciudad":"Valparíso",
    }

    item = opciones(**datos)

    print(item)

    item2 = opciones(nombre="David", **datos)
    print(item2)

    print(id(datos),id(item), id(item2))
