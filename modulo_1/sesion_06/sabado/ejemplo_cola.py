import queue
from datetime import datetime

cola = queue.Queue()

lista = [1,2,3]

cola.put(lista)

while (palabra:=input("Palabra: "))!="END":
    now = datetime.now()
    cola.put((now, palabra))


print("A: Estado de la cola, vacía?", cola.empty())

if not cola.empty():
    for _ in range(cola.qsize()):
        recibido = cola.get()
        print("Recibido", recibido)
        cola.task_done()


print("B: Estado de la cola, vacía?", cola.empty())
