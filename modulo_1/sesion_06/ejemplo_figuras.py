import random
from typing import Dict, List
import time
import queue
import math

lado = lambda: random.uniform(0.1, 100)

names = ("a","b","c","d","e")

def triangulo(*args, **kwargs)->Dict[str, float]:
	a = lado()
	b = lado()
	c = random.uniform(abs(a-b), a+b)
	lados = (a,b,c)
	return dict(zip(names, lados))

def cuadrado(*args, **kwargs)->Dict[str, float]:
	a = lado()
	lados = (a,a,a,a)
	return dict(zip(names, lados))

def pentagono(*args, **kwargs)->Dict[str,float]:
	a = lado()
	lados = tuple([a for _ in range(5)])
	return dict(zip(names, lados))

def crea_parametros(figura):
	operaciones = {
		"triangulo": triangulo,
		"cuadrado": cuadrado,
		"pentagono": pentagono
	}
	#funcion = operaciones.get(
	#	figura, triangulo)
	try:
		funcion = operaciones[figura]
		args = []
		kwargs = {}
		return funcion(*args, **kwargs)
	except Exception as e:
		print("Excepcion ", e)
		raise e
		
		
def genera_figura():
	nombre = random.choice((
		"triangulo", "cuadrado", "pentagono"
	))
	figura = {"nombre": nombre}
	figura["lados"] = crea_parametros(nombre)
	return figura
	
	

def crea_figuras(cola_cf:queue.Queue):
	while True:
		yield genera_figura()
		time.sleep(3)
		for msg in read_queue(cola_cf):
			print("Resultado", msg)

def read_queue(cola):
	if not cola.empty():
		for _ in range(cola.qsize()):
			msg  = cola.get()
			yield msg

def ap_triangulo(**figura):
	a = figura["lados"].get("a")
	b = figura["lados"].get("b")
	c = figura["lados"].get("c")
	perimetro = a+b+c
	s = perimetro  / 2
	area = math.sqrt(s*(s-a)*(s-b)*(s-c))
	return {"area":area, "perimetro": perimetro}


def ap_cuadrado(**figura):
	a = figura["lados"].get("a")
	return {"area":a**2, "perimetro":4*a}

def ap_pentagono(**figura):
	a = figura["lados"].get("a")
	perimetro = 5*a
	apotema = a / (2*math.tan(math.radians(36)))
	area = 5*a*apotema/2
	return {"area":area, "perimetro": perimetro}



def calcula_area_perimetro(figura):
	nombre = figura.get("nombre")
	ap_funciones = {
		"triangulo": ap_triangulo,
		"cuadrado": ap_cuadrado,
		"pentagono": ap_pentagono
	}
	ap = ap_funciones.get(nombre)(**figura)
	ap["nombre"] = nombre 
	return ap
	
	
	
def area_perimetro(cola_fc, cola_cf):
	for figura in read_queue(cola_fc):
		ap = calcula_area_perimetro(figura)
		cola_cf.put(ap)


def crea_figuras(cola_cf):
	while True:
		figura = genera_figura()
		yield figura
		time.sleep(3)
		for msg in read_queue(cola_cf):
			print("Resultado", msg)
			
			
if __name__=="__main__":
	cola_fc = queue.Queue()
	cola_cf = queue.Queue()
	for figura in crea_figuras(cola_cf):
		cola_fc.put(figura)
		area_perimetro(cola_fc, cola_cf)
