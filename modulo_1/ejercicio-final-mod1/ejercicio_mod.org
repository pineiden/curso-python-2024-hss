#+TITLE: Ejercicio final módulo 1

* Programar una versión sencilla de awk.

El comando *awk* es una herramienta de la línea de comandos para el
análisis y control de la información en formato de texto. Es también
un lenguaje, provee diversas características para la visualización y
selección de información.

Usando el lenguaje  *python* y todo el conocimiento de programación
del primer módulo se pide hacer una versión simplificada de *awk* que
vaya mostrando las coincidencias según lo que se explica a continuación..

Primero debe hacer lo siguiente :

- leer un archivo dada una ruta
- esta ruta debe entregarse en la línea de comandos

#+begin_src bash
programa.py ruta-archivo.csv
#+end_src

- seleccionar un string separador, este separador define campos.

#+begin_src bash
Héctor|Melenao|Odiseo|Helena|Paris
#+end_src

Por ejemplo, si el separador es "|", y se quieren los campos 1 y 5,
de lo siguiente se tendrá:

#+begin_src bash
Héctor|Paris
#+end_src

¿De qué estamos hablando acá?

Ahora, para mostrar esto, el programa debería tener por input:

#+begin_src bash
programa.py ruta-archivo.csv "|" "[1,5]"
#+end_src

Por último, agregar una serie de filtros en que se exprese de la
siguiente manera:

- 0:texto :: coincidencia toda la linea 
- 1:texto :: coincidencia texto en campo 1
- N:texto :: coincidencia texto en campo N

¿Cómo podríamos entregar esta información como input? Dado que es un
/mapping/ se sugiere algo así:

#+begin_src bash
programa.py ruta-archivo.csv "|" "[1,5]" "0:texto,1:Héctor,2:Paulino"
#+end_src

Para probar este programa buscar archivos de texto como libros o csv
en distintas plataformas y guardar estos archivos en un directorio
*textos*.

El programa debe entregarse en el siguiente formato:

#+begin_example
ejercicio1_mod1_NOMBRE.tar.gz
#+end_example

Entregar antes del 30 de mayo.


Sobre *awk* 

https://es.wikipedia.org/wiki/AWK
https://www.gnu.org/software/gawk/manual/html_node/Very-Simple.html
https://www.um.es/innova/OCW/informatica-para-universitarios/ipu_docs/la_shell/awk.pdf
https://vergaracarmona.es/guia-del-comando-awk/

Y, en la terminal:

#+begin_src bash
man awk
#+end_src
