texto = """
«Y la acción y el ejemplo arrastra mucho más que la palabra
Libertad, Justicia y Fraternidad
Hacer el bien a todos los que sufren
No hacerle el mal a nadie
Sólo atacar a quiénes atacan a los trabajadores
¡No se desalienten nunca!
Tendrán que tener a veces pequeñas derrotas, no, si
¿cuántas derrotas no hemos sufrido nosotros?
Ahora ya llevo más de diez años en la misma tarea
sin resultado alguno, porque aun la pudedumbre es mucho más grande que antes
No se desalienten jamás, sigan adelante en esta lucha
especialmente en las poblaciones, donde está más, hay más pobreza…»

El último año del siglo XIX, un 17 de noviembre de 1899, nacía quien se convertiría en una de las figuras más emblemáticas del movimiento sindical chileno, Clotario Blest Riffo. 

De un pensamiento profundamente cristiano y admirador de Luis Emilio Recabarren, Blest fundaría en 1943 la Asociación Nacional de Empleados Fiscales. Diez años después, con independencia de los partidos políticos, participaría en la fundación de la Central Unitaria de Trabajadores. 

Desde la presidencia de la Central es que Clotario Blest se posicionó como un líder sindical de renombre, encabezando huelgas y movilizaciones sociales. 18 veces fue encarcelado, por los gobiernos de Carlos Ibáñez del Campo, Jorge Alessandri y Eduardo Frei Montalva.

Siempre fue promotor de la unidad de los trabajadores, desde su pensamiento social cristiano. «Se ha desfigurado a Cristo ante las masas hasta el extremo de hacerlo odioso. Silencio alrededor del obrero que es Cristo: mucha palabrería alrededor del Dios que es rey. Se ha desfigurado a Jesús, mirándole sólo como Dios, y no como hombre y obrero, verdadero hermano nuestro según la carne, donde Él quiere y desea y pide ser imitado y amado», dijo una vez.

En los 60 participó en la creación del Movimiento de Izquierda Revolucionaria. Tras larga lucha de resistencia en dictadura, Clotario Blest, fallecería tras largos años viviendo con distintas enfermedades, en la madrugada del 31 de mayo de 1990.

El mismo día en que dos de las organizaciones que Clotario Blest fundó -la CUT y la ANEF- sufrieron una dura derrota en la negociación del reajuste del sector público, coincide con un aniversario más del nacimiento del barbudo dirigente. Es por eso que en El Desconcierto recordamos una parte de uno de sus recordados discursos, que hoy mismo, parece tener más actualidad que nunca.

«Y la acción y el ejemplo arrastra mucho más que la palabra
Libertad, Justicia y Fraternidad
Hacer el bien a todos los que sufren
No hacerle el mal a nadie
Sólo atacar a quiénes atacan a los trabajadores
¡No se desalienten nunca!
Tendrán que tener a veces pequeñas derrotas, no, si
¿cuántas derrotas no hemos sufrido nosotros?
Ahora ya llevo más de diez años en la misma tarea
sin resultado alguno, porque aun la pudedumbre es mucho más grande que antes
No se desalienten jamás, sigan adelante en esta lucha
especialmente en las poblaciones, donde está más, hay más pobreza…»
"""
"""
Preervar: 

- abecedario: a-z, 
- ' '
"""
import unicodedata


preservar = [chr(i) for i in range(65, 91)]
preservar.extend([chr(i) for i in range(97,123)])
preservar.append(" ")


texto = unicodedata.normalize("NFD", texto)
lineas = texto.split("\n")


# creo el diccionario inicializado con el conteo en 0
item = {letra:0 for letra in preservar}

for linea in lineas:
	linea_valida = [letra for letra in linea 
		if letra in preservar]
	# no sirve: linea_valida = set(linea) & preservar
	for letra in linea_valida:
		item[letra] += 1
		
from pprint import pprint

pprint(item, indent=4)

ordenado = sorted(item.items(), key=lambda x: -x[1])

pprint(ordenado, indent=4) 

item2  =item
print("Posicion original", id(item))
print("Posicion item2", id(item2))

print("Posicion ordenado", id(ordenado))
import copy

item3 = copy.deepcopy(item)
print("Posicion item3", id(item3))

for llave, valor in dict(ordenado).items():
	print(llave,"->", valor)



print("Cantidad de letras con 'a'", item["a"])
try:
	print("Cantidad de letras con '?'", item["?"])
except Exception as e:
	print("Hubo una excepcion",e)
	#raise e


