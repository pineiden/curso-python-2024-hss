"""
Obtener los primeros N primos
"""

def primeros_primos(n:int)->list[int]:
    """
    Una cantidad de N primos
    """
    coleccion = []
    numero = 2
    while len(coleccion)<n:
        bandera = True

        for nro in coleccion:
            if numero%nro==0:
                bandera = False
                # detengo la operación
                break

        if bandera:
            coleccion.append(numero)
        
        numero += 1

    return coleccion

def primos_hasta(n:int)->list[int]:
    """
    Una cantidad de N primos
    """
    coleccion = []
    for numero in range(2, n+1):
        bandera = True

        for nro in coleccion:
            if numero%nro==0:
                bandera = False
                # detengo la operación
                break

        if bandera:
            coleccion.append(numero)
        
        numero += 1

    return coleccion



if __name__=="__main__":
    N = 100
    resultado = primeros_primos(N)
    print(resultado)
    resultado2 = primos_hasta(N)
    print(resultado2)
