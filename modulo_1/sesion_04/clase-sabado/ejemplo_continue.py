nombres = [
    "Juana",
    "Mario",
    "Pericles",
    "Karina",
    "Zoila",
    "Carolina",
    "Kevin",
    "Eustaquio"
]

excluir = {"k", "z"}

for nombre in nombres:
    # conjunto de letras del nombre intersectado con excluir = vacio
    condicion = set(nombre.lower()) & excluir
    if condicion:
        continue
    # si se cumple condicion 'continue' provoca que nose pase a mayúscula
    print(nombre.upper())
