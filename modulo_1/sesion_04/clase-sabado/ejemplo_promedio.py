from functools import reduce
import math

def promedio_tradicional(numeros:list[float])->float:
    assert len(numeros)>0, "La lista debe contener al menos un valor"

    valor = 0
    contador = 0
    
    for numero in numeros:
        valor += numero
        contador +=1

    return valor/contador


def promedio_reduce(numeros:list[float])->float:
    assert len(numeros)>0, "La lista debe contener al menos un valor"
    sumar = lambda a,b: a+b
    total = len(numeros)
    return reduce(sumar, numeros)/total


def promedio_builtin(numeros:list[float])->float:
    assert len(numeros)>0, "La lista debe contener al menos un valor"
    return sum(numeros)/len(numeros)

"""
Desviación estandar
"""

def desv_estandar_tradicional(numeros:list[float])->float:
    assert len(numeros)>0, "La lista debe contener al menos un valor"

    promedio = promedio_tradicional(numeros)

    valor = 0
    contador = 0
    
    for numero in numeros:
        valor += (numero-promedio)**2
        contador +=1
        
    return math.sqrt(valor/contador)



def desv_estandar_builtin(numeros:list[float])->float:
    assert len(numeros)>0, "La lista debe contener al menos un valor"
    promedio = promedio_builtin(numeros)
    datos = sum([(n-promedio)**2 for n in numeros])
    return math.sqrt(datos/len(numeros))



def desv_estandar_reduce(numeros:list[float])->float:
    assert len(numeros)>0, "La lista debe contener al menos un valor"
    promedio = promedio_reduce(numeros)
    sumar = lambda a,b: a+b
    datos = reduce(sumar, [(n-promedio)**2 for n in numeros])        
    return math.sqrt(datos/len(numeros))


def get_max_tradicional(numeros:list[float])->float:
    assert len(numeros)>0, "La lista debe contener al menos un valor"
    max_valor = numeros[0]
    for numero in numeros[1::]:
        if numero>max_valor:
            max_valor=numero
    return max_valor


def get_max_builtin(numeros:list[float])->float:
    assert len(numeros)>0, "La lista debe contener al menos un valor"
    return max(numeros)


def get_max_reduce(numeros:list[float])->float:
    assert len(numeros)>0, "La lista debe contener al menos un valor"
    return reduce(lambda a,b: a if a>b else b, numeros)


if __name__=="__main__":
    numeros = [-10,4,5,22.3,12,-1100, 33500]
    r1 = promedio_tradicional(numeros)
    r2 = promedio_reduce(numeros)
    r3 = promedio_builtin(numeros)
    print("tradicional ", r1)
    print("reduce ", r2)
    print("builtin ", r3)
    r4 = desv_estandar_tradicional(numeros)
    r5 = desv_estandar_reduce(numeros)
    r6 = desv_estandar_builtin(numeros)
    print("desv estandar tradicional ", r4)
    print("desv estandar reduce ", r5)
    print("desv estandar builtin ", r6)

    r7 = get_max_tradicional(numeros)
    r8 = get_max_reduce(numeros)
    r9 = get_max_builtin(numeros)
    print("Max tradicional ", r7)
    print("Max builtin ", r8)
    print("Max reduce ", r9)
