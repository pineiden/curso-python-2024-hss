from ejemplo1 import perimetro, area


def check_anillo(R:float,r:float)->bool:
    assert R>=r, """Radio externo debe ser mayor o igual al interno"""

def perimetro_anillo(
        radio_externo:float, 
        radio_interno:float)->float:
    check_anillo(radio_externo, radio_interno)
    p1 = perimetro(radio_interno)
    p2 = perimetro(radio_externo)
    return p1 + p2

def area_anillo(
        radio_externo:float, 
        radio_interno:float)->float:
    check_anillo(radio_externo, radio_interno)
    a1 = area(radio_interno)
    a2 = area(radio_externo)
    return a2 + a1


if __name__=="__main__":
    R = 10
    r = 15
    p_anillo = perimetro_anillo(R,r)
    a_anillo = area_anillo(R,r)
    print("Perímetro del anillo", p_anillo)
    print("Área del anillo", a_anillo)

