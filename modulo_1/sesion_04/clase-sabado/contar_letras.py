texto = """
El último año del siglo XIX, un 17 de noviembre de 1899, nacía quien se convertiría en una de las figuras más emblemáticas del movimiento sindical chileno, Clotario Blest Riffo.

De un pensamiento profundamente cristiano y admirador de Luis Emilio Recabarren, Blest fundaría en 1943 la Asociación Nacional de Empleados Fiscales. Diez años después, con independencia de los partidos políticos, participaría en la fundación de la Central Unitaria de Trabajadores.

Desde la presidencia de la Central es que Clotario Blest se posicionó como un líder sindical de renombre, encabezando huelgas y movilizaciones sociales. 18 veces fue encarcelado, por los gobiernos de Carlos Ibáñez del Campo, Jorge Alessandri y Eduardo Frei Montalva.

Siempre fue promotor de la unidad de los trabajadores, desde su pensamiento social cristiano. «Se ha desfigurado a Cristo ante las masas hasta el extremo de hacerlo odioso. Silencio alrededor del obrero que es Cristo: mucha palabrería alrededor del Dios que es rey. Se ha desfigurado a Jesús, mirándole sólo como Dios, y no como hombre y obrero, verdadero hermano nuestro según la carne, donde Él quiere y desea y pide ser imitado y amado», dijo una vez.

En los 60 participó en la creación del Movimiento de Izquierda Revolucionaria. Tras larga lucha de resistencia en dictadura, Clotario Blest, fallecería tras largos años viviendo con distintas enfermedades, en la madrugada del 31 de mayo de 1990.

El mismo día en que dos de las organizaciones que Clotario Blest fundó -la CUT y la ANEF- sufrieron una dura derrota en la negociación del reajuste del sector público, coincide con un aniversario más del nacimiento del barbudo dirigente. Es por eso que en El Desconcierto recordamos una parte de uno de sus recordados discursos, que hoy mismo, parece tener más actualidad que nunca.

«Y la acción y el ejemplo arrastra mucho más que la palabra
Libertad, Justicia y Fraternidad
Hacer el bien a todos los que sufren
No hacerle el mal a nadie
Sólo atacar a quiénes atacan a los trabajadores
¡No se desalienten nunca!
Tendrán que tener a veces pequeñas derrotas, no, si
¿cuántas derrotas no hemos sufrido nosotros?
Ahora ya llevo más de diez años en la misma tarea
sin resultado alguno, porque aun la pudedumbre es mucho más grande que antes
No se desalienten jamás, sigan adelante en esta lucha
especialmente en las poblaciones, donde está más, hay más pobreza…»
"""
import unicodedata
from pprint import pprint
import rich

#rich.print(dict(ranking))
#pprint(ranking, indent=2)
#texto = "Este en texto de prueba en que el ñandú se come a un dragón"
letras = [chr(i) for i in range(97,123)]
mayusculas = [chr(i) for i in range(65,91)]
letras.extend(mayusculas)

def normalizar(linea:str)->str:
    return unicodedata.normalize("NFD", linea)


#print(letras)

def contar_letras(txt:str)->dict[str, int]:
    lineas = texto.split("\n")
    contador = {letra:0 for letra in letras}
    contador["ñ"] = 0
    contador["Ñ"] = 0
    alias_especiales = {"ñ":"n", "Ñ":"N"}   

    for linea in lineas:
        # reseteamos el contador para casos especiales
        casos_especiales = {"ñ":0, "Ñ":0}   

        # verificamos que la línea tenga o no ñ
        if set(casos_especiales) & set(linea): 
            for letra in linea:
                # ve que letra sea ñ o Ñ
                if letra in casos_especiales:
                    contador[letra] += 1
                    casos_especiales[letra] += 1

        # normalizamos la línea y contamos cada letra
        linea_nfd = normalizar(linea)
        for letra in linea_nfd:
            if letra in letras:
                contador[letra] += 1

        # decartar las n que pasan por ñ
        for llave, valor in casos_especiales.items():
            llave_alias = alias_especiales[llave]
            contador[llave_alias] -= valor

    #print(contador)


    # ordenamos contador en base al criterio mayor a menor de cantidad de letras
    ranking = sorted(
        contador.items(), 
        key=lambda item: -item[1]
    )

    return dict(ranking)




if __name__=="__main__":
    texto = input("Ingresa un texto y te retorno el conteo de letras:\n")
    resultado = contar_letras(texto)

    rich.print(resultado)

    print("Mostrar solo los resultados con valor>0")

    # for llave in resultado.keys():
    #     valor = resultado[llave]
    #     if valor==0:
    #         continue
    #     print(llave, "->", valor)

    for llave, valor in resultado.items():
        if valor==0:
            continue
        print(llave, "->", valor)
