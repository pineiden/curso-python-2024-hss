import math


def division(a:float, b:float)->float:
    try:
        return a/b
    except ZeroDivisionError as ze:
        print("No se puede dividir por 0, cambia el valor de b")
        raise ze


if __name__=="__main__":
    a = float(input("Dame un valor: "))
    b = float(input("Dame el divisor: "))

    resultado = division(a,b)

    print("Resultado", resultado)
