"""
Definir área y perímetro de círculo
"""
import math

def perimetro(radio:float)->float:
    assert radio>=0, "El valor debe ser positivo o 0"
    return 2*math.pi*radio

def area(radio:float)->float:
    assert radio>=0, "El valor debe ser positivo o 0"
    return math.pi * radio**2


if __name__=="__main__":
    radio = 1
    p = perimetro(radio)
    print(f"Perímetro para radio: {radio} = {p} [m]")
    a = area(radio)
    print(f"Área para radio: {radio} = {a} [m^2]")

    radio = 22.25
    p = perimetro(radio)
    print(f"Perímetro para radio: {radio} = {p} [m]")
    a = area(radio)
    print(f"Área para radio: {radio} = {a} [m^2]")

