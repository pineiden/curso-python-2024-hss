import math
from typing import Union
from functools import reduce
# unión de tipos
# Numero = float | int
Numero = Union[float , int]


def promedio(valores:list[Numero])->float:
	#assert len(valores)>0, "Debe ser una lista de valores con una cantidad mayor o igual a 1"
	try:
		suma = sum(valores)
		N = len(valores)
		return suma/N
	except ZeroDivisionError as ze:
		print(ze)
		raise ze
		
def desv_estandar(valores):
	try:
		prom = promedio(valores)
		suma_q = sum([(x-prom)**2 for x in valores])
		N = len(valores)
		return math.sqrt(suma_q/N)
	except ZeroDivisionError as ze:
		print(ze)
		raise ze

def get_max(valores:list[Numero])->float:
	# max(valores)
	return reduce(lambda a,b: a if a>b else b,valores)

if __name__=="__main__":
	valores = [1,-10,4,5,6.3,-12,0.3,8]
	prom = promedio(valores)
	print(prom)
	dev_std = desv_estandar(valores)
	print(dev_std)
	maximo = get_max(valores)
	print("Maximo entre los valores", maximo)
