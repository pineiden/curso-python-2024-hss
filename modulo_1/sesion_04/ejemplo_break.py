valores = [2,3,4,6,-10,5,11,6,7,100]

"""
Si el valor está entre 10 excluyéndolo y 20, detener la operación

if x in (10,20]

Retornar la posición en que se detiene.
"""

for i, item in enumerate(valores):
	if 10 < item <= 20:
	#if 10 < item and item <=20:		
		break


print(f"Valor de detención {item}")
print(f"Posición de detención {i}")


print("La posición y valor es: (%d, %d)" %(i, item))

print("El resto de los valores son", valores[i::])

