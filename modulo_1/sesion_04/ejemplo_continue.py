nombres = [
	"Sebastian",
	"Virginia",
	"Diego",
	"Karina",
	"Omar",
	"Zoila",
	"Estebam",	
	"Karlali",
	"Zulema",
	"Aura María"
]

excluir = {"k","z"}
"""
Procesar a mayúsculas los nombres que no tengan ciertos caracterez:

excluir : {k, z}
"""


def check_nombre(nombre, excluir):
	lista = [
			letra in excluir 
			for letra in nombre.lower() ]
			
	return any(lista)


for nombre in nombres:
	condicion = check_nombre(nombre, excluir)
	print(f"Nombre :{nombre}",condicion)
	if condicion:
		continue
	print(nombre.upper())  

print("[]"*10)		
print("="*10)	
print("[]"*10)	

print("Hola" + "Mundo")
	
for nombre in nombres:
	condicion = set(nombre.lower()) & excluir
	#condicion = set(nombre.lower()).intersection(excluir)
	print(f"Nombre :{nombre}",condicion)
	if condicion:
		continue
	print(nombre.upper())  

