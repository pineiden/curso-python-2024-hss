"""
Cosas con números

"""
import math


def hipotenusa(a:float,b:float)->float:
    """
    Según el teorema de pitágoras, 
    la hipotenusa es el resultado de la raiz cuadrada de la suma
    cuadrada de los catetos de un triangulo
    """
    return math.sqrt(a**2 + b**2)


if __name__=="__main__":
    lados = [
        (3,4),
        (1,1),
        (5,12),
    ]

    catetos = (2,2)

    hip0 = hipotenusa(*catetos)
    print(hip0)

    resultados = []

    for catetos in lados:
        hip = hipotenusa(*catetos)
        result = (*catetos, hip)
        resultados.append(result)

    print(resultados)

    # equivalente a lambda:
    # def operacion_hip(x):
    #     return hipotenusa(*x)

    operacion_hip = lambda x: (*x, hipotenusa(*x))    
    # map -> (funcion, iterable)
    resultados2 = list(map(operacion_hip, lados))
    print(resultados2)
