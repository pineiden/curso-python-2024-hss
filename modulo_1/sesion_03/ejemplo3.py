from ejemplo2 import seleccion
from declaracion import txt
import inspect

if __name__=="__main__":
    print(seleccion)
    print(inspect.isfunction(seleccion))
    print(inspect.signature(seleccion))
    print(inspect.getdoc(seleccion))
    # mientras no ocurra lac condición de detención

    condicion = ""
    while condicion!="END":
        #print(condicion)
        condicion = input("Dame una palabra para buscar: ")
        result, nolines = seleccion(txt, condicion)
        # #breakpoint()
        if result!="":
            #condicion="END"
            break
    for linea in result.split("|"):
        print(linea)
