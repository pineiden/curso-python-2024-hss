ParTexto = tuple[str, list[str]]

def seleccion(
        txt:str, 
        palabra:str)->ParTexto:
    """
    Es un filtro al que se le entrega un texto y una palabra para
    filtrar.
    Retorna la selección encontrada y la lista de lineas que no
    cumplen la condición

    """
    lineas =  txt.split("\n")

    lineas_con_aprender = []
    otras_lineas = []

    for linea in lineas:
        if palabra.lower() in linea.lower():
            lineas_con_aprender.append(linea)
        else:
            linea = linea.strip()
            if linea != "":
                otras_lineas.append(linea)

    volvemos_al_txt = "|".join(lineas_con_aprender)

    # tupla (a,b,c)
    return volvemos_al_txt, otras_lineas


if __name__=="__main__":
    txt = """
Acá estamos aprendiendo 
a programar en el hackerspace
vamos en la tercera clase 
y aprenderemos a hacer funciones
además de ocupar los condicionales
    """
    palabra = "aprend"
    resultado, otro_resultado = seleccion(txt, palabra)


    print(resultado)
    print(otro_resultado)
