txt = """
Declaración de Independencia del Ciberespacio
(Por John Perry Barlow)
Gobiernos del Mundo Industrial, vosotros, cansados gigantes de carne y acero, vengo del
Ciberespacio, el nuevo hogar de la Mente. En nombre del futuro, os pido en el pasado que nos
dejéis en paz. No sois bienvenidos entre nosotros. No ejercéis ninguna soberanía sobre el
lugar donde nos reunimos.
No hemos elegido ningún gobierno, ni pretendemos tenerlo, así que me dirijo a vosotros sin
más autoridad que aquélla con la que la libertad siempre habla. Declaro el espacio social global
que estamos construyendo independiente por naturaleza de las tiranías que estáis buscando
imponernos. No tenéis ningún derecho moral a gobernarnos ni poseéis métodos para hacernos
cumplir vuestra ley que debamos temer verdaderamente.
Los gobiernos derivan sus justos poderes del consentimiento de los que son gobernados. No
habéis pedido ni recibido el nuestro. No os hemos invitado. No nos conocéis, ni conocéis
nuestro mundo. El Ciberespacio no se halla dentro de vuestras fronteras. No penséis que
podéis construirlo, como si fuera un proyecto público de construcción. No podéis. Es un acto
natural que crece de nuestras acciones colectivas.
No os habéis unido a nuestra gran conversación colectiva, ni creasteis la riqueza de nuestros
mercados. No conocéis nuestra cultura, nuestra ética, o los códigos no escritos que ya
proporcionan a nuestra sociedad más orden que el que podría obtenerse por cualquiera de
vuestras imposiciones.
Proclamáis que hay problemas entre nosotros que necesitáis resolver. Usáis esto como una
excusa para invadir nuestros límites. Muchos de estos problemas no existen. Donde haya
verdaderos conflictos, donde haya errores, los identificaremos y resolvereremos por nuestros
propios medios. Estamos creando nuestro propio Contrato Social. Esta autoridad se creará
según las condiciones de nuestro mundo, no del vuestro. Nuestro mundo es diferente.
El Ciberespacio está formado por transacciones, relaciones, y pensamiento en sí mismo, que
se extiende como una quieta ola en la telaraña de nuestras comunicaciones. Nuestro mundo
está a la vez en todas partes y en ninguna parte, pero no está donde viven los cuerpos.
Estamos creando un mundo en el que todos pueden entrar, sin privilegios o prejuicios debidos
a la raza, el poder económico, la fuerza militar, o el lugar de nacimiento.
Estamos creando un mundo donde cualquiera, en cualquier sitio, puede expresar sus
creencias, sin importar lo singulares que sean, sin miedo a ser coaccionado al silencio o el
conformismo.
Vuestros conceptos legales sobre propiedad, expresión, identidad, movimiento y contexto no se
aplican a nosotros. Se basan en la materia. Aquí no hay materia.
Nuestras identidades no tienen cuerpo, así que, a diferencia de vosotros, no podemos obtener
orden por coacción física. Creemos que nuestra autoridad emanará de la moral, de un
progresista interés propio, y del bien común. Nuestras identidades pueden distribuirse a través
de muchas jurisdicciones. La única ley que todas nuestras culturas reconocerían es la Regla
Dorada. Esperamos poder construir nuestras soluciones particulares sobre esa base. Pero no
podemos aceptar las soluciones que estáis tratando de imponer.
En Estados Unidos hoy habéis creado una ley, el Acta de Reforma de las Telecomunicaciones,
que repudia vuestra propia Constitución e insulta los sueños de Jefferson, Washington, Mill,
Madison, DeToqueville y Brandeis. Estos sueños deben renacer ahora en nosotros.
Os atemorizan vuestros propios hijos, ya que ellos son nativos en un mundo donde vosotros
siempre seréis inmigrantes. Como les teméis, encomendáis a vuestra burocracia las
responsabilidades paternas a las que cobardemente no podéis enfrentaros. En nuestro mundo,
todos los sentimientos y expresiones de humanidad, de las más viles a las más angelicales,
son parte de un todo único, la conversación global de bits. No podemos separar el aire que
asfixia de aquél sobre el que las alas baten.
En China, Alemania, Francia, Rusia, Singapur, Italia y los Estados Unidos estáis intentando
rechazar el virus de la libertad erigiendo puestos de guardia en las fronteras del Ciberespacio.
Puede que impidan el contagio durante un pequeño tiempo, pero no funcionarán en un mundo
que pronto será cubierto por los medios que transmiten bits.
Vuestras cada vez más obsoletas industrias de la información se perpetuarían a sí mismas
proponiendo leyes, en América y en cualquier parte, que reclamen su posesión de la palabra
por todo el mundo. Estas leyes declararían que las ideas son otro producto industrial, menos
noble que el hierro oxidado. En nuestro mundo, sea lo que sea lo que la mente humana pueda
crear puede ser reproducido y distribuido infinitamente sin ningún coste. El trasvase global de
pensamiento ya no necesita ser realizado por vuestras fábricas.
Estas medidas cada vez más hostiles y colonialistas nos colocan en la misma situación en la
que estuvieron aquellos amantes de la libertad y la autodeterminación que tuvieron que luchar
contra la autoridad de un poder lejano e ignorante. Debemos declarar nuestros "yos" virtuales
inmunes a vuestra soberanía, aunque continuemos consintiendo vuestro poder sobre nuestros
cuerpos. Nos extenderemos a través del planeta para que nadie pueda encarcelar nuestros
pensamientos.
Crearemos una civilización de la Mente en el Ciberespacio. Que sea más humana y hermosa
que el mundo que vuestros gobiernos han creado antes.
Davos, Suiza. 8 de febrero de 199
"""
