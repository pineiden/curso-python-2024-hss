import math
import random

def generar_numeros(
    inicio:int,
    final:int,
    cantidad:int=1
)->list[int]:
    """
    En base a un valor inicial y final
    entregar una lista aleatoria de valores
    la cantidad está definida por el keyword 'cantidad'
    """
    assert inicio<final, f"""El valor inicial {inicio} debe ser menor
    que el final {final}"""
    assert cantidad>=1, f"""El valor de cantidad debe ser entero
    positivo, entregaste {cantidad}"""
    
    numero = lambda: random.randint(inicio, final)
    # forma funcional
    # return list(map(numero, range(cantidad)))
    # forma comprehensiva
    return [numero() for x in range(cantidad)]

