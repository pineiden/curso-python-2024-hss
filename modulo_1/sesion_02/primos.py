def primos(n):
    set_primos = [2]
    for i in range(2,n+1):
        bandera = True 
        for j in set_primos: 
            if i%j == 0:
                bandera = False
                break
        if bandera:
            set_primos.append(i)
    return set_primos

if __name__ == "__main__":
    lista = primos(100)

    print(lista)
